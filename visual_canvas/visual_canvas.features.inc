<?php
/**
 * @file
 * visual_canvas.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function visual_canvas_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function visual_canvas_node_info() {
  $items = array(
    'visual_canvas' => array(
      'name' => t('Visual Canvas'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
