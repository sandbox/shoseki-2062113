<?php

/**
 * Displays the content type admin overview page.
 */
function sgnodeobject_overview_types() {
  $types = sgnodeobject_type_get_types();
  $names = sgnodeobject_type_get_names();
  $field_ui = module_exists('field_ui');
  $header = array(t('Name'), array('data' => t('Operations'), 'colspan' => $field_ui ? '4' : '2'));
  $rows = array();

  foreach ($names as $key => $name) {
    $type = $types[$key];
    if (sgnodeobject_hook($type->type, 'form')) {
      $type_url_str = str_replace('_', '-', $type->type);
      $row = array(theme('sgnodeobject_admin_overview', array('name' => $name, 'type' => $type)));
      // Set the edit column.
      $row[] = array('data' => l(t('edit'), 'admin/structure/sgnodeobjecttypes/manage/' . $type_url_str));

      if ($field_ui) {
        // Manage fields.
        $row[] = array('data' => l(t('manage fields'), 'admin/structure/sgnodeobjecttypes/manage/' . $type_url_str . '/fields'));

        // Display fields.
        $row[] = array('data' => l(t('manage display'), 'admin/structure/sgnodeobjecttypes/manage/' . $type_url_str . '/display'));
      }

      // Set the delete column.
      if ($type->custom) {
        $row[] = array('data' => l(t('delete'), 'admin/structure/sgnodeobjecttypes/manage/' . $type_url_str . '/delete'));
      }
      else {
        $row[] = array('data' => '');
      }

      $rows[] = $row;
    }
  }

  $build['sgnodeobject_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No sgnodeobject types available. <a href="@link">Add sgnodeobject type</a>.', array('@link' => url('admin/structure/sgnodeobjecttypes/add'))),
  );

  return $build;
}

/**
 * Returns HTML for a node type description for the content type admin overview page.
 *
 * @param $variables
 *   An associative array containing:
 *   - name: The human-readable name of the content type.
 *   - type: An object containing the 'type' (machine name) and 'description' of
 *     the content type.
 *
 * @ingroup themeable
 */
function theme_sgnodeobject_admin_overview($variables) {
  $name = $variables['name'];
  $bundle_type = $variables['bundle_type'];

  $output = check_plain($name);
  $output .= ' <small>' . t('(Machine name: @bundle_type)', array('@bundle_type' => $bundle_type->bundle_type)) . '</small>';
  $output .= '<div class="description">' . filter_xss_admin($bundle_type->description) . '</div>';
  return $output;
}