<?php

function _cam3d_three_jstree_sgnode_core_encode($sgnodecore) {
    
  $jstree_object = new stdClass();

  $sgnodecore_metadata_wrapper = entity_metadata_wrapper('file', $sgnodecore);
  
  if (!empty($sgnodecore_metadata_wrapper->field_file_image_title_text)) {
      $jstree_object->text = $sgnodecore_metadata_wrapper->field_file_image_title_text->value();
  }
  else {
      $jstree_object->text = $sgnodecore->uri;
  }
  
  $jstree_object->children = false;
  $jstree_object->icon = 'sgnodecore';
  $jstree_object->state = 'closed';
  $jstree_object->id = intval('sgnodecore-' . $sgnodecore->fid);
  return $jstree_object;
}

function cam3d_three_sgnode_json($sgnode_id, $is_root = false) {
    
    $sgnode = sgnodeobject_load(intval($sgnode_id));

    $class_prefix = 'entity-example-basic-';

    if ($is_root) {
        $jstree_object = _cam3d_three_jstree_sgnode_encode($sgnode, $is_root);
        $list_wrapper = array($jstree_object);
    }
    else {
         $list_wrapper = array();
        
        if ($sgnode->core != -1) {
            // Core has been defined
            $file_entity = file_load($sgnode->core);
            $list_wrapper[] = _cam3d_three_jstree_sgnode_core_encode($file_entity);
        }
         
        if (is_array($sgnode->children) && count($sgnode->children) > 0) {
            $child_sgnodes = sgnodeobject_load_multiple($sgnode->children);
            foreach($child_sgnodes as $child_node) $list_wrapper[] = _cam3d_three_jstree_sgnode_encode($child_node, $is_root);
        }
        else {
          // Do nothing
        }
    }
    
    $sgnode_json = json_encode($list_wrapper);
    
    drupal_add_http_header('Content-Type', 'application/json');

    print $sgnode_json;
    
    drupal_exit();
}

function _cam3d_three_jstree_sgnode_encode($sgnode, $is_root) {
    
  $jstree_object = new stdClass();
  $jstree_object->text = $sgnode->title;
    
  if (is_array($sgnode->variables) && isset($sgnode->variables['children']) && is_array($sgnode->variables['children']) && count($sgnode->variables['children']) > 0) {
    $jstree_object->children = true;
  }
  else $jstree_object->children = false;
    
  $jstree_object->id = 'sgnode-' . intval($sgnode->basic_id);
   
  if ($is_root) {
    $jstree_object->icon = 'root';
    $jstree_object->state = 'open';
  }
  else {
    $jstree_object->icon = 'sgnode';
    $jstree_object->state = 'closed';
  }
    
  return $jstree_object;
}