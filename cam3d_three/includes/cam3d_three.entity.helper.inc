<?php

/**
 * Fetch a basic object.
 *
 * This function ends up being a shim between the menu system and
 * sgnodeobject_load_multiple().
 *
 * This function gets its name from the menu system's wildcard
 * naming conventions. For example, /path/%wildcard would end
 * up calling wildcard_load(%wildcard value). In our case defining
 * the path: examples/entity_example/basic/%sgnodeobject in
 * hook_menu() tells Drupal to call sgnodeobject_load().
 *
 * @param int $basic_id
 *   Integer specifying the basic entity id.
 * @param bool $reset
 *   A boolean indicating that the internal cache should be reset.
 *
 * @return object
 *   A fully-loaded $basic object or FALSE if it cannot be loaded.
 *
 * @see sgnodeobject_load_multiple()
 * @see entity_example_menu()
 */
function sgnodeobject_load($basic_id = NULL, $reset = FALSE) {
  $basic_ids = (isset($basic_id) ? array($basic_id) : array());
  $basic = sgnodeobject_load_multiple($basic_ids, array(), $reset);
  return $basic ? reset($basic) : FALSE;
}

function cam3d_three_sgnodeobject_label_callback($entity, $entity_type) {
    return $entity->title;
}

/**
 * Loads multiple basic entities.
 *
 * We only need to pass this request along to entity_load(), which
 * will in turn call the load() method of our entity controller class.
 */
function sgnodeobject_load_multiple($basic_ids = FALSE, $conditions = array(), $reset = FALSE) {
  return entity_load('sgnodeobject', $basic_ids, $conditions, $reset);
}

/**
 * Implements the uri callback.
 */
function sgnodeobject_uri($basic) {
  return array(
    'path' => 'sgnodeobject/' . $basic->basic_id,
  );
}

/**
 * Basic information for the page.
 */
function entity_example_info_page() {
  $content['preface'] = array(
    '#type' => 'item',
    '#markup' => t('The entity example provides a simple example entity.'),
  );
  if (user_access('administer sgnodeobject entities')) {
    $content['preface']['#markup'] = t('You can administer these and add fields and change the view !link.',
      array('!link' => l(t('here'), 'admin/structure/sgnodeobject/manage'))
    );
  }
  $content['table'] = sgnodeobject_list_entities();

  return $content;
}

/**
 * Returns a render array with all sgnodeobject entities.
 *
 * In this basic example we know that there won't be many entities,
 * so we'll just load them all for display. See pager_example.module
 * to implement a pager. Most implementations would probably do this
 * with the contrib Entity API module, or a view using views module,
 * but we avoid using non-core features in the Examples project.
 *
 * @see pager_example.module
 */
function sgnodeobject_list_entities() {
  $content = array();
  // Load all of our entities.
  $entities = sgnodeobject_load_multiple();
  
  if (!empty($entities)) {
    foreach ($entities as $entity) {
      // Create tabular rows for our entities.
      $rows[] = array(
        'data' => array(
          'id' => $entity->basic_id,
          'title' => $entity->title,
          'item_description' => l($entity->item_description, 'examples/entity_example/basic/' . $entity->basic_id),
          //'bundle' => $entity->bundle_type,
          'actions' =>  implode(array(
                            l('view', 'examples/entity_example/basic/' . $entity->basic_id),
                            l('edit', 'examples/entity_example/basic/' . $entity->basic_id . '/edit'),
                        ), ' | '),
        ),
      );
    }
    // Put our entities into a themed table. See theme_table() for details.
    $content['entity_table'] = array(
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => array(t('ID'), t('Title'), t('Item Description'), t('Actions')),
    );
  }
  else {
    // There were no entities. Tell the user.
    $content[] = array(
      '#type' => 'item',
      '#markup' => t('No sgnodeobject entities currently exist.'),
    );
  }
  return $content;
}

/**
 * Callback for a page title when this entity is displayed.
 */
function sgnodeobject_title($entity) {
  return t('Entity Example Basic (item_description=@item_description)', array('@item_description' => $entity->title));
}

/**
 * Menu callback to display an entity.
 *
 * As we load the entity for display, we're responsible for invoking a number
 * of hooks in their proper order.
 *
 * @see hook_entity_prepare_view()
 * @see hook_entity_view()
 * @see hook_entity_view_alter()
 */
function sgnodeobject_view($entity, $view_mode = 'tweaky') {
  // Our entity type, for convenience.
  $entity_type = 'sgnodeobject';
  // Start setting up the content.
  $entity->content = array(
    '#view_mode' => $view_mode,
  );
  // Build fields content - this is where the Field API really comes in to play.
  // The task has very little code here because it all gets taken care of by
  // field module.
  // field_attach_prepare_view() lets the fields load any data they need
  // before viewing.
  field_attach_prepare_view($entity_type, array($entity->basic_id => $entity),
    $view_mode);
  // We call entity_prepare_view() so it can invoke hook_entity_prepare_view()
  // for us.
  entity_prepare_view($entity_type, array($entity->basic_id => $entity));
  // Now field_attach_view() generates the content for the fields.
  $entity->content += field_attach_view($entity_type, $entity, $view_mode);

  // OK, Field API done, now we can set up some of our own data.
  $entity->content['title'] = array(
    '#type' => 'item',
    '#title' => t('Title'),
    '#markup' => $entity->title,
  );
  // OK, Field API done, now we can set up some of our own data.
  $entity->content['created'] = array(
    '#type' => 'item',
    '#title' => t('Created date'),
    '#markup' => format_date($entity->created),
  );

  $entity->content['item_description'] = array(
    '#type' => 'item',
    '#title' => t('Item Description'),
    '#markup' => $entity->item_description,
  );

  // Now to invoke some hooks. We need the language code for
  // hook_entity_view(), so let's get that.
  global $language;
  $langcode = $language->language;
  // And now invoke hook_entity_view().
  module_invoke_all('entity_view', $entity, $entity_type, $view_mode,
    $langcode);
  
  // Now invoke hook_entity_view_alter().
  drupal_alter(array('sgnodeobject_view', 'entity_view'),
    $entity->content, $entity_type);

  // And finally return the content.
  return $entity->content;
}

/**
 * Provides a wrapper on the edit form to add a new entity.
 */
function sgnodeobject_add() {
  // Create a basic entity structure to be used and passed to the validation
  // and submission functions.
  $entity = entity_get_controller('sgnodeobject')->create();
  return drupal_get_form('sgnodeobject_form', $entity);
}

/**
 * Form function to create an sgnodeobject entity.
 *
 * The pattern is:
 * - Set up the form for the data that is specific to your
 *   entity: the columns of your base table.
 * - Call on the Field API to pull in the form elements
 *   for fields attached to the entity.
 */
function sgnodeobject_form($form, &$form_state, $entity) {

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#required' => TRUE,
    '#default_value' => $entity->title,
  );

  $form['object_type'] = array(
    '#type' => 'textfield',
    '#title' => t('SGNode Object Type'),
    '#required' => TRUE,
    '#default_value' => $entity->object_type,
  );
  
  $form['item_description'] = array(
    '#type' => 'textfield',
    '#title' => t('Item Description'),
    '#required' => TRUE,
    '#default_value' => $entity->item_description,
  );
  
  $form['children'] = array(
    '#type' => 'textfield',
    '#title' => t('Child SGNode IDs'),
    '#required' => FALSE,
    '#default_value' => !empty($entity->variables) && !empty($entity->variables['children']) && is_array($entity->variables['children']) ? implode(' ', $entity->variables['children']) : '',
  );
  
  $form['basic_entity'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );

  field_attach_form('sgnodeobject', $entity, $form, $form_state);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );
  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#submit' => array('sgnodeobject_edit_delete'),
    '#weight' => 200,
  );

  return $form;
}


/**
 * Validation handler for sgnodeobject_add_form form.
 *
 * We pass things straight through to the Field API to handle validation
 * of the attached fields.
 */
function sgnodeobject_form_validate($form, &$form_state) {
  field_attach_form_validate('sgnodeobject', $form_state['values']['basic_entity'], $form, $form_state);
}


/**
 * Form submit handler: Submits basic_add_form information.
 */
function sgnodeobject_form_submit($form, &$form_state) {

  $entity = $form_state['values']['basic_entity'];
  $entity->title = $form_state['values']['title'];  
  $entity->item_description = $form_state['values']['item_description'];
  
  if (empty($entity->variables)) $entity->variables = array();
  
  $entity->variables['children'] = explode(' ', $form_state['values']['children']);
  foreach ($entity->variables['children'] as $child_id_key => $child_id) {
      if (!is_numeric($child_id)) unset($entity->variables['children'][$child_id_key]);
      else $entity->variables['children'][$child_id_key] = intval($child_id);
  }
  
  field_attach_submit('sgnodeobject', $entity, $form, $form_state);
  $entity = sgnodeobject_save($entity);
  $form_state['redirect'] = 'examples/entity_example/basic/' . $entity->basic_id;
}

/**
 * Form deletion handler.
 *
 * @todo: 'Are you sure?' message.
 */
function sgnodeobject_edit_delete($form, &$form_state) {
  $entity = $form_state['values']['basic_entity'];
  sgnodeobject_delete($entity);
  drupal_set_message(t('The entity %item_description (ID %id) has been deleted',
    array('%item_description' => $entity->item_description, '%id' => $entity->basic_id))
  );
  $form_state['redirect'] = 'examples/entity_example';
}

/**
 * We save the entity by calling the controller.
 */
function sgnodeobject_save(&$entity) {
  return entity_get_controller('sgnodeobject')->save($entity);
}

/**
 * Use the controller to delete the entity.
 */
function sgnodeobject_delete($entity) {
  entity_get_controller('sgnodeobject')->delete($entity);
}

class SGNodeEntity extends Entity {
  
}

/**
 * SGNodeObjectControllerInterface definition.
 *
 * We create an interface here because anyone could come along and
 * use hook_entity_info_alter() to change our controller class.
 * We want to let them know what methods our class needs in order
 * to function with the rest of the module, so here's a handy list.
 *
 * @see hook_entity_info_alter()
 */
interface OldSGNodeObjectControllerInterface
  extends DrupalEntityControllerInterface {

  /**
   * Create an entity.
   */
  public function create();

  /**
   * Save an entity.
   *
   * @param object $entity
   *   The entity to save.
   */
  public function save($entity);

  /**
   * Delete an entity.
   *
   * @param object $entity
   *   The entity to delete.
   */
  public function delete($entity);

}

/**
 * SGNodeObjectController extends DrupalDefaultEntityController.
 *
 * Our subclass of DrupalDefaultEntityController lets us add a few
 * important create, update, and delete methods.
 */
class OldSGNodeObjectController
  extends DrupalDefaultEntityController
  implements SGNodeObjectControllerInterface {

  /**
   * Create and return a new sgnodeobject entity.
   */
  public function create() {
    $entity = new stdClass();
    $entity->type = 'sgnodeobject';
    $entity->basic_id = 0;
    $entity->title = '';
    $entity->object_type = -1;
    $entity->item_description = '';
    $entity->variables = array(
        'children' => array(),
    );
    $entity->orphan = 1;
    return $entity;
  }

  /**
   * Saves the custom fields using drupal_write_record().
   */
  public function save($entity) {
      
    // If our entity has no basic_id, then we need to give it a
    // time of creation.
    if (empty($entity->basic_id)) {
      $entity->created = time();
    }
    
    if (is_array($entity->variables)) {
        $entity->variables = serialize($entity->variables);
    }
    
    // Invoke hook_entity_presave().
    module_invoke_all('entity_presave', $entity, 'sgnodeobject');
    // The 'primary_keys' argument determines whether this will be an insert
    // or an update. So if the entity already has an ID, we'll specify
    // basic_id as the key.
    $primary_keys = $entity->basic_id ? 'basic_id' : array();
    // Write out the entity record.
    drupal_write_record('sgnodeobject', $entity, $primary_keys);
    // We're going to invoke either hook_entity_update() or
    // hook_entity_insert(), depending on whether or not this is a
    // new entity. We'll just store the name of hook_entity_insert()
    // and change it if we need to.
    $invocation = 'entity_insert';
    // Now we need to either insert or update the fields which are
    // attached to this entity. We use the same primary_keys logic
    // to determine whether to update or insert, and which hook we
    // need to invoke.
    if (empty($primary_keys)) {
      field_attach_insert('sgnodeobject', $entity);
    }
    else {
      field_attach_update('sgnodeobject', $entity);
      $invocation = 'entity_update';
    }
    // Invoke either hook_entity_update() or hook_entity_insert().
    module_invoke_all($invocation, $entity, 'sgnodeobject');
    return $entity;
  }

  /**
   * Delete a single entity.
   *
   * Really a convenience function for deleteMultiple().
   */
  public function delete($entity) {
    $this->deleteMultiple(array($entity));
  }

  /**
   * Delete one or more sgnodeobject entities.
   *
   * Deletion is unfortunately not supported in the base
   * DrupalDefaultEntityController class.
   *
   * @param array $entities
   *   An array of entity IDs or a single numeric ID.
   */
  public function deleteMultiple($entities) {
    $basic_ids = array();
    if (!empty($entities)) {
      $transaction = db_transaction();
      try {
        foreach ($entities as $entity) {
          // Invoke hook_entity_delete().
          module_invoke_all('entity_delete', $entity, 'sgnodeobject');
          field_attach_delete('sgnodeobject', $entity);
          $basic_ids[] = $entity->basic_id;
        }
        db_delete('sgnodeobject')
          ->condition('basic_id', $basic_ids, 'IN')
          ->execute();
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('entity_example', $e);
        throw $e;
      }
    }
  }
  
  public function load($ids = array(), $conditions = array()) {
      $entities = parent::load($ids, $conditions);
      foreach($entities as $entity_id => $entity) {
        if (is_string($entities[$entity_id]->variables)) {
            $entities[$entity_id]->variables = unserialize ($entities[$entity_id]->variables);
        }
        if ($entities[$entity_id]->variables === false) {
            $entities[$entity_id]->variables = array(
                'children' => array(),
            );
        }
      }
      return $entities;
  }
}

/**
 * @} End of "defgroup entity_example".
 */
