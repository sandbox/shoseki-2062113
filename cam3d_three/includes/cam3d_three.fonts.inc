<?php

// Function to call when retrieving typefaces, in the form $typeface_fonts['font']['weight]['style] = 'font_file_name.js';
function cam3d_three_retrieve_typeface_fonts() {
    $data = array();
    $hook_name = 'typeface_font_info';
    $data = module_invoke_all($hook_name);
    drupal_alter($hook_name, $data);
    return $data;
}

/**
 * Implements hook_typeface_font_info().
 */
function cam3d_three_typeface_font_info() {
    $typeface_fonts = array();

    // Convert the font into a typeface javascript file using http://typeface.neocracy.org/
    // $typeface_fonts['font']['weight]['style] = 'font_file_name.js';

    global $base_root;
    $three_path = libraries_get_path('three.js') . '/examples/fonts/';

    $typeface_fonts['gentilis']['bold']['normal'] = $base_root . '/' . $three_path . 'gentilis_bold.typeface.js';
    $typeface_fonts['gentilis']['normal']['normal'] = $base_root . '/' . $three_path . 'gentilis_regular.typeface.js';
    $typeface_fonts['helvetiker']['bold']['normal'] = $base_root . '/' . $three_path . 'helvetiker_bold.typeface.js';
    $typeface_fonts['helvetiker']['normal']['normal'] = $base_root . '/' . $three_path . 'helvetiker_regular.typeface.js';
    $typeface_fonts['optimer']['bold']['normal'] = $base_root . '/' . $three_path . 'optimer_bold.typeface.js';
    $typeface_fonts['optimer']['normal']['normal'] = $base_root . '/' . $three_path . 'optimer_regular.typeface.js';

    $typeface_fonts['droid sans']['bold']['normal'] = $base_root . '/' . $three_path . 'droid/droid_sans_bold.typeface.js';
    $typeface_fonts['droid sans mono']['normal']['normal'] = $base_root . '/' . $three_path . 'droid/droid_sans_mono_regular.typeface.js';
    $typeface_fonts['droid sans']['normal']['normal'] = $base_root . '/' . $three_path . 'droid/droid_sans_regular.typeface.js';
    $typeface_fonts['droid serif']['bold']['normal'] = $base_root . '/' . $three_path . 'droid/droid_serif_bold.typeface.js';
    $typeface_fonts['droid serif']['normal']['normal'] = $base_root . '/' . $three_path . 'droid/droid_serif_regular.typeface.js';

    return $typeface_fonts;
}