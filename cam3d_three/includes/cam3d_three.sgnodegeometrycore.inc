<?php

include_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'media_cam3d_three') . '/includes/SGNodeGeometryCoreStreamWrapper.inc';
include_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'cam3d_three') . '/includes/cam3d_three.fonts.inc';

function cam3d_three_retrieve_sgnodegeometrycorecontrollers() {
    $data = array();
    $hook_name = 'sgnodegeometrycorecontrollers_info';
    $data = module_invoke_all($hook_name);
    drupal_alter($hook_name, $data);
    return $data;
}

/**
 * Implements hook_sgnodegeometrycorecontrollers_info().
 */
function cam3d_three_sgnodegeometrycorecontrollers_info() {

    $controllers = array(
        'plane' => array(
            'label' => 'Plane',
            'data_controller' => 'PlaneGeometryCoreCAM3DController',
            'variables' => array(
                'width' => array('default_value' => 200.0, 'type' => 'double'),
                'height' => array('default_value' => 200.0, 'type' => 'double'),
                'widthSegments' => array('default_value' => 1, 'type' => 'integer'),
                'heightSegments' => array('default_value' => 1, 'type' => 'integer'),
                'geometry_type' => array('default_value' => 'plane', 'type' => 'string'),
            ),
        ),
        'box' => array(
            'label' => 'Box',
            'data_controller' => 'BoxGeometryCoreCAM3DController',
            'variables' => array(
                'width' => array('default_value' => 100.0, 'type' => 'double'),
                'height' => array('default_value' => 100.0, 'type' => 'double'),
                'depth' => array('default_value' => 100.0, 'type' => 'double'),
                'widthSegments' => array('default_value' => 1, 'type' => 'integer'),
                'heightSegments' => array('default_value' => 1, 'type' => 'integer'),
                'depthSegments' => array('default_value' => 1, 'type' => 'integer'),
                'geometry_type' => array('default_value' => 'box', 'type' => 'string'),
            ),
        ),
        'circle' => array(
            'label' => 'Circle',
            'data_controller' => 'CircleGeometryCAM3DControllerCAM3DController',
            'variables' => array(
                'radius' => array('default_value' => 50.0, 'type' => 'double'),
                'segments' => array('default_value' => 8, 'type' => 'integer'),
                'thetaStart' => array('default_value' => 0, 'type' => 'double'),
                'thetaLength' => array('default_value' => pi() * 2, 'type' => 'double'),
                'geometry_type' => array('default_value' => 'circle', 'type' => 'string'),
            ),
        ),
        'cylinder' => array(
            'label' => 'Cylinder',
            'data_controller' => 'CylinderGeometryCoreCAM3DController',
            'variables' => array(
                'radiusTop' => array('default_value' => 20.0, 'type' => 'double'),
                'radiusBottom' => array('default_value' => 20.0, 'type' => 'double'),
                'height' => array('default_value' => 100.0, 'type' => 'double'),
                'radialSegments' => array('default_value' => 8, 'type' => 'integer'),
                'heightSegments' => array('default_value' => 1, 'type' => 'integer'),
                'openEnded' => array('default_value' => false, 'type' => 'boolean'),
                'geometry_type' => array('default_value' => 'cylinder', 'type' => 'string'),
            ),
        ),
        'sphere' => array(
            'label' => 'Sphere',
            'data_controller' => 'SphereGeometryCoreCAM3DController',
            'variables' => array(
                'radius' => array('default_value' => 50.0, 'type' => 'double'),
                'widthSegments' => array('default_value' => 8, 'type' => 'integer'),
                'heightSegments' => array('default_value' => 6, 'type' => 'integer'),
                'phiStart' => array('default_value' => 0.0, 'type' => 'double'),
                'phiLength' => array('default_value' => pi() * 2, 'type' => 'double'),
                'thetaStart' => array('default_value' => 0.0, 'type' => 'double'),
                'thetaLength' => array('default_value' => pi(), 'type' => 'double'),
                'geometry_type' => array('default_value' => 'sphere', 'type' => 'string'),
            ),
        ),
        'icosahedron' => array(
            'label' => 'Icosahedron',
            'data_controller' => 'IcosahedronGeometryCoreCAM3DController',
            'variables' => array(
                'radius' => array('default_value' => 75.0, 'type' => 'double'),
                'detail' => array('default_value' => 2, 'type' => 'integer'),
                'geometry_type' => array('default_value' => 'icosahedron', 'type' => 'string'),
            ),
        ),
        'torus' => array(
            'label' => 'Torus',
            'data_controller' => 'TorusGeometryCoreCAM3DController',
            'variables' => array(
                'radius' => array('default_value' => 100.0, 'type' => 'double'),
                'tube' => array('default_value' => 40.0, 'type' => 'double'),
                'radialSegments' => array('default_value' => 8, 'type' => 'integer'),
                'tubularSegments' => array('default_value' => 6, 'type' => 'integer'),
                'arc' => array('default_value' => pi() * 2, 'type' => 'double'),
                'geometry_type' => array('default_value' => 'torus', 'type' => 'string'),
            ),
        ),
        'torusknot' => array(
            'label' => 'Torus Knot',
            'data_controller' => 'TorusKnotGeometryCoreCAM3DController',
            'variables' => array(
                'radius' => array('default_value' => 100.0, 'type' => 'double'),
                'tube' => array('default_value' => 40.0, 'type' => 'double'),
                'radialSegments' => array('default_value' => 64, 'type' => 'integer'),
                'tubularSegments' => array('default_value' => 8, 'type' => 'integer'),
                'p' => array('default_value' => 2.0, 'type' => 'double'),
                'q' => array('default_value' => 3.0, 'type' => 'double'),
                'heightScale' => array('default_value' => 1.0, 'type' => 'double'),
                'geometry_type' => array('default_value' => 'torusknot', 'type' => 'string'),
            ),
        ),
        'text' => array(
            'label' => 'Text',
            'data_controller' => 'TextGeometryCoreCAM3DController',
            'variables' => array(
                'text' => array('default_value' => 'Text', 'type' => 'string'),
                'size' => array('default_value' => 150.0, 'type' => 'double'),
                'height' => array('default_value' => 50.0, 'type' => 'double'),
                'curveSegments' => array('default_value' => 100, 'type' => 'integer'),
                'font' => array('default_value' => 'helvetiker', 'type' => 'string'),
                'weight' => array('default_value' => 'normal', 'type' => 'string'),
                'style' => array('default_value' => 'normal', 'type' => 'string'),
                'bevelEnabled' => array('default_value' => false, 'type' => 'boolean'),
                'bevelThickness' => array('default_value' => 10, 'type' => 'double'),
                'bevelSize' => array('default_value' => 8, 'type' => 'double'), 
                'geometry_type' => array('default_value' => 'text', 'type' => 'string'),
            ),
        ),
    );

    foreach ($controllers as $controllers_key => $controller) {
        $controllers[$controllers_key]['variables']['geometry_id'] = array('default_value' => -1, 'type' => 'integer');
    }

    return $controllers;
}
class GenericGeometryCAM3DController extends GenericFileEntityCAM3DController {

    public function front_end_controllers($variables) {
        
        $files = array();
        
        // Base controller doesn't care about the $data_object type
        global $base_root;
        $files[] = $base_root . '/' . drupal_get_path('module', 'cam3d_three') . '/js/sgnodegeometrycorecontrollers.core.js';
        
        // Check to see if $variables contains the editor value, in which case add core.ui controllers
        if (!empty($variables['ui'])) {
            $files[] = $base_root . '/' . drupal_get_path('module', 'cam3d_three') . '/js/sgnodegeometrycorecontrollers.core.ui.js';
        }
        return $files;
    }
    
    public function create($default_variables, $variables, $type = 'generic') {

      // Create an entity and save variables in it      
      $variables['geometry_type'] = $type;
      if (!empty($variables['geometry_id'])) unset($variables['geometry_id']);

      $uri = SGNodeGeometryCoreStreamWrapper::generate_uri($variables);
      $file = file_uri_to_object($uri);
      
      $file_entity = file_save($file);

      // Save entity converts the variables array into a string
      return $this->read($default_variables, $file_entity);
    }

    public function read($default_variables, $entity) {
        
        // $id by default is the id of our entity
        // Parameters includes name => default_value and type
        $file_stream_wrapper = file_stream_wrapper_get_instance_by_uri($entity->uri);

        $variables = $file_stream_wrapper->get_parameters();
        
        $variables = $this->deserialize_parameters($default_variables, $variables);
        
        $variables['geometry_id'] = $entity->fid;
        // Here geometry_id is set

        $json = $this->generate_json($default_variables, $variables); 

        // Here it isn't
        return $json;
    }

    public function update($default_variables, $variables) {
        
        $geometry_id = $variables['geometry_id'];
        
        // Load the entity from the db, update it and save
        unset($variables['geometry_id']);
        
        $file_entity = file_load($geometry_id);
        
        // Process variables
        // Consolidate the variables together
        // Then save them
        
        $file_stream_wrapper = file_stream_wrapper_get_instance_by_uri($file_entity->uri);

        $previous_variables = $file_stream_wrapper->get_parameters();
        
        $previous_variables_deserialised = $this->deserialize_parameters($default_variables, $previous_variables);
        
        $updated_variables = array_merge($previous_variables_deserialised, $variables);
        
        $serialised_updated_variables = $this->serialize_parameters($default_variables, $updated_variables);
        
        $file_entity->uri = SGNodeGeometryCoreStreamWrapper::generate_uri($serialised_updated_variables);
        file_save($file_entity);

        // Need to set it again after saving
        $updated_variables['geometry_id'] = $geometry_id;

        $json = $this->generate_json($default_variables, $updated_variables); 
        // Here it isn't
        return $json;
    }

    public function delete($id) {
        // Delete the entity from the db
    }
}
class PlaneGeometryCoreCAM3DController extends GenericGeometryCAM3DController {
    public function create($default_variables, $variables, $type = 'plane') {
        return parent::create($default_variables, $variables, $type);
    }
}
class BoxGeometryCoreCAM3DController extends GenericGeometryCAM3DController {
    public function create($default_variables, $variables, $type = 'box') {
        return parent::create($default_variables, $variables, $type);
    }
}
class CircleGeometryCAM3DControllerCAM3DController extends GenericGeometryCAM3DController {
    public function create($default_variables, $variables, $type = 'circle') {
        return parent::create($default_variables, $variables, $type);
    }
}
class CylinderGeometryCoreCAM3DController extends GenericGeometryCAM3DController {
    public function create($default_variables, $variables, $type = 'cylinder') {
        return parent::create($default_variables, $variables, $type);
    }
}
class SphereGeometryCoreCAM3DController extends GenericGeometryCAM3DController {
    public function create($default_variables, $variables, $type = 'sphere') {
        return parent::create($default_variables, $variables, $type);
    }
}
class IcosahedronGeometryCoreCAM3DController extends GenericGeometryCAM3DController {
    public function create($default_variables, $variables, $type = 'icosahedron') {
        return parent::create($default_variables, $variables, $type);
    }
}
class TorusGeometryCoreCAM3DController extends GenericGeometryCAM3DController {
    public function create($default_variables, $variables, $type = 'torus') {
        return parent::create($default_variables, $variables, $type);
    }
}
class TorusKnotGeometryCoreCAM3DController extends GenericGeometryCAM3DController {
    public function create($default_variables, $variables, $type = 'torusknot') {
        return parent::create($default_variables, $variables, $type);
    }
}
class TextGeometryCoreCAM3DController extends GenericGeometryCAM3DController {
    public function create($default_variables, $variables, $type = 'text') {
        return parent::create($default_variables, $variables, $type);
    }
    public function front_end_controllers($variables) {
        $files = parent::front_end_controllers($variables);
        
        $typeface_fonts = cam3d_three_retrieve_typeface_fonts();

        $font_selected = (!empty($variables['font']) && strlen($variables['font']) > 0) ? $variables['font'] : 'helvetiker';
        $weight_selected = (!empty($variables['weight']) && strlen($variables['weight']) > 0) ? $variables['weight'] : 'normal';
        $style_selected = (!empty($variables['style']) && strlen($variables['style']) > 0) ? $variables['style'] : 'normal';
        
        if (!empty($typeface_fonts[$font_selected][$weight_selected][$style_selected])) {
            $files[] = $typeface_fonts[$font_selected][$weight_selected][$style_selected];
        }
        
        if (!in_array($typeface_fonts['helvetiker']['normal']['normal'], $files)) {
            $files[] = $typeface_fonts['helvetiker']['normal']['normal'];
        }
        
        return $files;
    }
}