<?php

// This tells Drupal what fields it applies to and what settings are available.
function _cam3d_three_is_entity_reference($field_info, $field_mapping_key) {
  return isset($field_info[$settings[$field_mapping_key]]) &&
    $field_info[$settings[$field_mapping_key]]['type'] == 'entityreference';
}

function _cam3d_three_startsWith($haystack, $needle) {
  // search backwards starting from haystack length characters from the end
  return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
}

/**
 * Check if the given operation is allowed.
 */
function sgnodeobject_type_access($op, $type = NULL, $account = NULL) {
  return user_access('administer cam3d_three types', $account);
}

