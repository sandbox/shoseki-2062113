<?php

include_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'media_cam3d_three') . '/includes/SGNodeMaterialCoreStreamWrapper.inc';

define('THREEJS_SrcAlphaFactor', 204);
define('THREEJS_OneMinusSrcAlphaFactor', 205);
define('THREEJS_NormalBlending', 1);
define('THREEJS_AddEquation', 100);
define('THREEJS_FrontSide', 0);
define('THREEJS_MultiplyOperation', 0);

function cam3d_three_retrieve_sgnodematerialcorecontrollers() {
    $data = array();
    $hook_name = 'sgnodematerialcorecontrollers_info';
    $data = module_invoke_all($hook_name);
    drupal_alter($hook_name, $data);
    return $data;
}

/**
 * Implements hook_sgnodematerialcorecontrollers_info().
 */
function cam3d_three_sgnodematerialcorecontrollers_info () {

    $controllers = array(
        'linebasic' => array(
            'data_controller' => 'LineBasicMaterialCoreCAM3DController',
            'variables' => array(
                'color' => array('default_value' => 0xffffff, 'type' => 'integer'),
                'linewidth' => array('default_value' => 1.0, 'type' => 'double'),
                'linecap' => array('default_value' => 'round', 'type' => 'string'),
                'linejoin' => array('default_value' => 'round', 'type' => 'string'),
                'vertexColors' => array('default_value' => 0, 'type' => 'integer'),
                'fog' => array('default_value' => true, 'type' => 'boolean'),
                'material_type' => array('default_value' => 'linebasic', 'type' => 'string'),
            ),
        ),
        'linedashed' => array(
            'data_controller' => 'LineDashedMaterialCoreCAM3DController',
            'variables' => array(
                'color' => array('default_value' => 0xffffff, 'type' => 'integer'),
                'linewidth' => array('default_value' => 1.0, 'type' => 'double'),
                'scale' => array('default_value' => 1.0, 'type' => 'double'),
                'dashSize' => array('default_value' => 3.0, 'type' => 'double'),
                'gapSize' => array('default_value' => 1.0, 'type' => 'double'),
                'vertexColors' => array('default_value' => 0, 'type' => 'integer'),
                'fog' => array('default_value' => true, 'type' => 'boolean'),
                'material_type' => array('default_value' => 'linedashed', 'type' => 'string'),
            ),
        ),
        'meshbasic' => array(
            'data_controller' => 'MeshBasicMaterialCoreCAM3DController',
            'variables' => array(
                'color' => array('default_value' => 0xffffff, 'type' => 'integer'),
                'map' => array('default_value' => CAM3D_THREE_DEFAULT_IMAGE, 'type' => 'image'),
                'lightMap' => array('default_value' => CAM3D_THREE_DEFAULT_IMAGE, 'type' => 'image'),
                'specularMap' => array('default_value' => CAM3D_THREE_DEFAULT_IMAGE, 'type' => 'image'),
                'envMap' => array('default_value' => null, 'type' => 'array'),
                'combine' => array('default_value' => THREEJS_MultiplyOperation, 'type' => 'integer'),
                'reflectivity' => array('default_value' => 1.0, 'type' => 'double'),
                'refractionRatio' => array('default_value' => 0.98, 'type' => 'double'),
                'shading' => array('default_value' => 2, 'type' => 'integer'),
                'wireframe' => array('default_value' => false, 'type' => 'boolean'),
                'wireframeLinewidth' => array('default_value' => 1.0, 'type' => 'double'),
                'vertexColors' => array('default_value' => 0, 'type' => 'integer'),
                'skinning' => array('default_value' => false, 'type' => 'boolean'),
                'morphTargets' => array('default_value' => false, 'type' => 'boolean'),
                'fog' => array('default_value' => true, 'type' => 'boolean'),
                'material_type' => array('default_value' => 'meshbasic', 'type' => 'string'),
            ),            
        ),
        'meshdepth' => array(
            'data_controller' => 'MeshDepthMaterialCoreCAM3DController',
            'variables' => array(
                'wireframe' => array('default_value' => false, 'type' => 'boolean'),
                'wireframeLinewidth' => array('default_value' => 1, 'type' => 'double'),
                'material_type' => array('default_value' => 'meshdepth', 'type' => 'string'),
            ),
        ),
        'meshface' => array(
            'data_controller' => 'MeshFaceMaterialCoreCAM3DController',
            'variables' => array(
                'material_type' => array('default_value' => 'meshface', 'type' => 'string'),
            ),            
        ),
        'meshlambert' => array(
            'data_controller' => 'MeshLambertMaterialCoreCAM3DController',
            'variables' => array(
                'color' => array('default_value' => 0xffffff, 'type' => 'integer'),
                'ambient' => array('default_value' => 0xffffff, 'type' => 'integer'),
                'emissive' => array('default_value' => 0x000000, 'type' => 'integer'),
                'map' => array('default_value' => CAM3D_THREE_DEFAULT_IMAGE, 'type' => 'image'),
                'lightMap' => array('default_value' => CAM3D_THREE_DEFAULT_IMAGE, 'type' => 'image'),
                'specularMap' => array('default_value' => CAM3D_THREE_DEFAULT_IMAGE, 'type' => 'image'),
                'envMap' => array('default_value' => null, 'type' => 'array'),
                'combine' => array('default_value' => 0, 'type' => 'integer'),
                'reflectivity' => array('default_value' => 1.0, 'type' => 'double'),
                'refractionRatio' => array('default_value' => 0.98, 'type' => 'double'),
                'shading' => array('default_value' => 2, 'type' => 'integer'),
                'wireframe' => array('default_value' => false, 'type' => 'boolean'),
                'wireframeLinewidth' => array('default_value' => 1.0, 'type' => 'double'),
                'vertexColors' => array('default_value' => 0, 'type' => 'integer'),
                'skinning' => array('default_value' => false, 'type' => 'boolean'),
                'morphTargets' => array('default_value' => false, 'type' => 'boolean'),
                'morphNormals' => array('default_value' => false, 'type' => 'boolean'),
                'fog' => array('default_value' => true, 'type' => 'boolean'),
                'material_type' => array('default_value' => 'meshlambert', 'type' => 'string'),
            ),
        ),
        'meshnormal' => array(
            'data_controller' => 'MeshNormalMaterialCoreCAM3DController',
            'variables' => array(
                'shading' => array('default_value' => 2, 'type' => 'integer'),
                'wireframe' => array('default_value' => false, 'type' => 'boolean'),
                'wireframeLinewidth' => array('default_value' => 1.0, 'type' => 'double'),
                'material_type' => array('default_value' => 'meshnormal', 'type' => 'string'),
            ),            
        ),
        'meshphong' => array(
            'data_controller' => 'MeshPhongMaterialCoreCAM3DController',
            'variables' => array(
                'color' => array('default_value' => 0xffffff, 'type' => 'integer'),
                'ambient' => array('default_value' => 0xffffff, 'type' => 'integer'),
                'emissive' => array('default_value' => 0x000000, 'type' => 'integer'),
                'specular' => array('default_value' => 0x111111, 'type' => 'integer'),
                'shininess' => array('default_value' => 30, 'type' => 'double'),
                'map' => array('default_value' => CAM3D_THREE_DEFAULT_IMAGE, 'type' => 'image'),
                'lightMap' => array('default_value' => CAM3D_THREE_NULL_IMAGE, 'type' => 'image'),
                'bumpMap' => array('default_value' => CAM3D_THREE_NULL_IMAGE, 'type' => 'image'),
                'bumpScale' => array('default_value' => 1.0, 'type' => 'double'),
                'normalMap' => array('default_value' => CAM3D_THREE_NULL_IMAGE, 'type' => 'image'),
                'normalScale' => array('default_value' => array(1.0, 1.0), 'type' => 'vec2f'),
                'specularMap' => array('default_value' => CAM3D_THREE_NULL_IMAGE, 'type' => 'image'),
                'envMap' => array('default_value' => null, 'type' => 'array'),
                'combine' => array('default_value' => 0, 'type' => 'integer'),
                'reflectivity' => array('default_value' => 1.0, 'type' => 'double'),
                'refractionRatio' => array('default_value' => 0.98, 'type' => 'double'),
                'shading' => array('default_value' => 2, 'type' => 'integer'),
                'wireframe' => array('default_value' => false, 'type' => 'boolean'),
                'wireframeLinewidth' => array('default_value' => 1.0, 'type' => 'double'),
                'vertexColors' => array('default_value' => 0, 'type' => 'integer'),
                'skinning' => array('default_value' => false, 'type' => 'boolean'),
                'morphTargets' => array('default_value' => false, 'type' => 'boolean'),
                'morphNormals' => array('default_value' => false, 'type' => 'boolean'),
                'fog' => array('default_value' => true, 'type' => 'boolean'),
                'material_type' => array('default_value' => 'meshphong', 'type' => 'string'),
            ),
        ),
        'particlesystem' => array(
            'data_controller' => 'ParticleSystemMaterialCoreCAM3DController',
            'variables' => array(
                'color' => array('default_value' => 0xffffff, 'type' => 'integer'),
                'map' => array('default_value' => CAM3D_THREE_NULL_IMAGE, 'type' => 'image'),
                'size' => array('default_value' => 1.0, 'type' => 'double'),
                'vertexColors' => array('default_value' => 0, 'type' => 'integer'),
                'fog' => array('default_value' => true, 'type' => 'boolean'),
                'material_type' => array('default_value' => 'particlesystem', 'type' => 'string'),
            ),
        ),
        'shader' => array(
            'data_controller' => 'ShaderMaterialCoreCAM3DController',
            'variables' => array(
                'fragmentShader' => array('default_value' => 'void main() {}', 'type' => 'string'),
                'vertexShader' => array('default_value' => 'void main() {}', 'type' => 'string'),
                'uniforms' => array('default_value' => new stdClass(), 'type' => 'object'),
                'defines' => array('default_value' => new stdClass(), 'type' => 'object'),
                'attributes' => array('default_value' => null, 'type' => 'object'),
                'shading' => array('default_value' => 2, 'type' => 'integer'),
                'wireframe' => array('default_value' => false, 'type' => 'boolean'),
                'wireframeLinewidth' => array('default_value' => 1.0, 'type' => 'double'),
                'lights' => array('default_value' => false, 'type' => 'boolean'),
                'vertexColors' => array('default_value' => 0, 'type' => 'integer'),
                'skinning' => array('default_value' => false, 'type' => 'boolean'),
                'morphTargets' => array('default_value' => false, 'type' => 'boolean'),
                'morphNormals' => array('default_value' => false, 'type' => 'boolean'),
                'fog' => array('default_value' => true, 'type' => 'boolean'),
                'material_type' => array('default_value' => 'shader', 'type' => 'string'),
            ),
        ),
        'spritecanvas' => array(
            'data_controller' => 'SpriteCanvasMaterialCoreCAM3DController',
            'variables' => array(
                'color' => array('default_value' => 0xffffff, 'type' => 'integer'),
                'program' => array('default_value' => 'function ( context, color ) {};', 'type' => 'string'),
                'material_type' => array('default_value' => 'spritecanvas', 'type' => 'string'),
            ),
        ),
        'sprite' => array(
            'data_controller' => 'SpriteMaterialCoreCAM3DController',
            'variables' => array(
                'color' => array('default_value' => 0xffffff, 'type' => 'integer'),
                'map' => array('default_value' => CAM3D_THREE_DEFAULT_IMAGE, 'type' => 'image'),
                'uvOffset' => array('default_value' => array(0.0, 0.0), 'type' => 'vec2f'),
                'uvScale' => array('default_value' => array(1.0, 1.0), 'type' => 'vec2f'),
                'fog' => array('default_value' => true, 'type' => 'boolean'),
                'material_type' => array('default_value' => 'sprite', 'type' => 'string'),
            ),
        ),
    );

    // Default parameters for each material
    foreach ($controllers as $controllers_key => $controller) {
        $controllers[$controllers_key]['variables']['material_id'] = array('default_value' => -1, 'type' => 'integer');
        $controllers[$controllers_key]['variables']['opacity'] = array('default_value' => 1.0, 'type' => 'double');
        $controllers[$controllers_key]['variables']['transparent'] = array('default_value' => false, 'type' => 'boolean');
        $controllers[$controllers_key]['variables']['blending'] = array('default_value' => THREEJS_NormalBlending, 'type' => 'integer');
        $controllers[$controllers_key]['variables']['blendSrc'] = array('default_value' => THREEJS_SrcAlphaFactor, 'type' => 'integer');
        $controllers[$controllers_key]['variables']['blendDst'] = array('default_value' => THREEJS_OneMinusSrcAlphaFactor, 'type' => 'integer');
        $controllers[$controllers_key]['variables']['blendEquation'] = array('default_value' => THREEJS_AddEquation, 'type' => 'integer');
        $controllers[$controllers_key]['variables']['depthTest'] = array('default_value' => true, 'type' => 'boolean');
        $controllers[$controllers_key]['variables']['depthWrite'] = array('default_value' => true, 'type' => 'boolean');
        $controllers[$controllers_key]['variables']['polygonOffset'] = array('default_value' => false, 'type' => 'boolean');
        $controllers[$controllers_key]['variables']['polygonOffsetFactor'] = array('default_value' => 0, 'type' => 'integer');
        $controllers[$controllers_key]['variables']['polygonOffsetUnits'] = array('default_value' => 0, 'type' => 'integer');
        $controllers[$controllers_key]['variables']['alphaTest'] = array('default_value' => 0, 'type' => 'double');
        $controllers[$controllers_key]['variables']['overdraw'] = array('default_value' => false, 'type' => 'boolean');
        $controllers[$controllers_key]['variables']['visible'] = array('default_value' => true, 'type' => 'boolean');
        $controllers[$controllers_key]['variables']['side'] = array('default_value' => THREEJS_FrontSide, 'type' => 'integer');
    }

    return $controllers;
}

class GenericMaterialCoreCAM3DController extends GenericFileEntityCAM3DController {

    public function front_end_controllers($variables) {
        
        // Base controller doesn't care about the $data_object.material type
        global $base_root;
        $files[] = $base_root . '/' . drupal_get_path('module', 'cam3d_three') . '/js/sgnodematerialcorecontrollers.core.js';
        
        // Check to see if $variables contains the editor value, in which case add core.ui controllers
        if (!empty($variables['ui'])) {
            $files[] = $base_root . '/' . drupal_get_path('module', 'cam3d_three') . '/js/sgnodematerialcorecontrollers.core.ui.js';
        }

        return $files;
    }
    public function create($default_variables, $variables, $type = 'generic') {

      // Create an entity and save variables in it      
      // $consolidated_variables = $this->consolidate_variables($default_variables, $variables);
      $variables['material_type'] = $type;
      if (!empty($consolidated_variables['material_id'])) unset($variables['material_id']);

      $serialised_variables = $this->serialize_parameters($default_variables, $variables);
      
      $uri = SGNodeMaterialCoreStreamWrapper::generate_uri($serialised_variables);
      $file = file_uri_to_object($uri);
      
      $file_entity = file_save($file);

      // Save entity converts the variables array into a string
      return $this->read($default_variables, $file_entity);
    }
    public function read($default_variables, $entity) {
        // $id by default is the id of our entity
        // Parameters includes name => default_value and type
        $file_stream_wrapper = file_stream_wrapper_get_instance_by_uri($entity->uri);

        $variables = $file_stream_wrapper->get_parameters();
        
        $deserialised_variables = $this->deserialize_parameters($default_variables, $variables);
        
        $deserialised_variables['material_id'] = $entity->fid;
        if (is_string($deserialised_variables['material_id'])) $deserialised_variables['material_id'] = intval($deserialised_variables['material_id']);

        return $this->generate_json($default_variables, $deserialised_variables);
    }
    public function update($default_variables, $variables) {
        $material_id = $variables['material_id'];
        
        // Load the entity from the db, update it and save
        unset($variables['material_id']);
        
        $file_entity = file_load($material_id);
        
        // Process variables
        // Consolidate the variables together
        // Then save them
        
        $file_stream_wrapper = file_stream_wrapper_get_instance_by_uri($file_entity->uri);

        $previous_variables = $file_stream_wrapper->get_parameters();

        $previous_variables_deserialised = $this->deserialize_parameters($default_variables, $previous_variables);

        $updated_variables = array_merge($previous_variables_deserialised, $variables);
        
        $serialised_updated_variables = $this->serialize_parameters($default_variables, $updated_variables);
        
        $file_entity->uri = SGNodeMaterialCoreStreamWrapper::generate_uri($serialised_updated_variables);
        
        file_save($file_entity);

        // Need to set it again after saving
        $updated_variables['material_id'] = $material_id;
        
        $json = $this->generate_json($default_variables, $updated_variables); 

        return $json;
    }
    public function delete($id) {
        // Delete the entity from the db
        $file = file_load($variables['material_id']);
        file_delete($file);
    }
    public function generate_json($default_variables, $variables) {
        $wrapper = $this->generate_json_wrapper($default_variables, $variables);
        return $wrapper;
    }    
}
class LineBasicMaterialCoreCAM3DController extends GenericMaterialCoreCAM3DController {
    public function create($default_variables, $variables, $type = 'linebasic') {
        return parent::create($default_variables, $variables, $type);
    }
}
class LineDashedMaterialCoreCAM3DController extends GenericMaterialCoreCAM3DController {
    public function create($default_variables, $variables, $type = 'linedashed') {
        return parent::create($default_variables, $variables, $type);
    }
}
class MeshBasicMaterialCoreCAM3DController extends GenericMaterialCoreCAM3DController {
    public function create($default_variables, $variables, $type = 'meshbasic') {
        return parent::create($default_variables, $variables, $type);
    }
}
class MeshDepthMaterialCoreCAM3DController extends GenericMaterialCoreCAM3DController {
    public function create($default_variables, $variables, $type = 'meshdepth') {
        return parent::create($default_variables, $variables, $type);
    }
}
class MeshFaceMaterialCoreCAM3DController extends GenericMaterialCoreCAM3DController {
    public function create($default_variables, $variables, $type = 'meshface') {
        return parent::create($default_variables, $variables, $type);
    }
}
class MeshLambertMaterialCoreCAM3DController extends GenericMaterialCoreCAM3DController {
    public function create($default_variables, $variables, $type = 'meshlambert') {
        return parent::create($default_variables, $variables, $type);
    }
}
class MeshNormalMaterialCoreCAM3DController extends GenericMaterialCoreCAM3DController {
    public function create($default_variables, $variables, $type = 'meshnormal') {
        return parent::create($default_variables, $variables, $type);
    }
}
class MeshPhongMaterialCoreCAM3DController extends GenericMaterialCoreCAM3DController {
    public function create($default_variables, $variables, $type = 'meshphong') {
        return parent::create($default_variables, $variables, $type);
    }
}
class ParticleSystemMaterialCoreCAM3DController extends GenericMaterialCoreCAM3DController {
    public function create($default_variables, $variables, $type = 'particlesystem') {
        return parent::create($default_variables, $variables, $type);
    }
}
class ShaderMaterialCoreCAM3DController extends GenericMaterialCoreCAM3DController {
    public function create($default_variables, $variables, $type = 'shadermaterial') {
        return parent::create($default_variables, $variables, $type);
    }
}
class SpriteMaterialCoreCAM3DController extends GenericMaterialCoreCAM3DController {
    public function create($default_variables, $variables, $type = 'spritematerial') {
        return parent::create($default_variables, $variables, $type);
    }
}
class SpriteCanvasMaterialCoreCAM3DController extends GenericMaterialCoreCAM3DController {
    public function create($default_variables, $variables, $type = 'spritecanvas') {
        return parent::create($default_variables, $variables, $type);
    }
}