<?php

function _cam3d_three_js_controllers() {
  $sgnodeobjectcontrollers = cam3d_three_retrieve_sgnodeobjectcontrollers();
  $json = $_POST['data'];
  $data = json_decode($json);
  $variables = get_object_vars($data);

  $object_type = $variables['object_type'];

  $controller = new $sgnodeobjectcontrollers[$object_type]['data_controller']();

  $json_wrapper = new stdClass();       
  $json_wrapper->files = $controller->front_end_controllers($variables);

  drupal_json_output($json_wrapper);
  drupal_exit();
}

function _cam3d_three_read() {

  // Two versions of this controller
  // First loads the entity and then applies the controller as defined by object_type
  // However, if an object type is specified, then the controller is selected first and the entire object is passed
  // Enabling sub objects to be loaded etc
  $json = $_POST['data'];
  $data = json_decode($json);

  $variables = get_object_vars($data);

  $sgnodeobjectcontrollers = cam3d_three_retrieve_sgnodeobjectcontrollers();

  if (!empty($variables['object_type'])) {
    $object_type = $variables['object_type'];
    $controller = new $sgnodeobjectcontrollers[$object_type]['data_controller']();
    $json_object = $controller->read_sub_controller($sgnodeobjectcontrollers[$object_type]['variables'], $variables);
  }
  else {
    $id = $variables['object_id'];

    $entity = sgnodeobject_load(intval($id));
    
    if (!empty($entity)) {
      $object_type = $entity->object_type;

      $controller = new $sgnodeobjectcontrollers[$object_type]['data_controller']();

      $json_object = $controller->read($sgnodeobjectcontrollers[$object_type]['variables'], $entity);
    }
    else {
        drupal_not_found();
        drupal_exit();
    }
  }

  drupal_json_output($json_object);
  drupal_exit();
}