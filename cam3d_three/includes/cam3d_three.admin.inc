<?php

function sgnodeobject_form_wrapper($sgnodeobject, $op = 'edit') {
  return drupal_get_form('sgnodeobject_form', $sgnodeobject, $op);
}
function sgnodeobject_form($form, &$form_state, $sgnodeobject = NULL, $op = 'edit') {

  $form_state['entity_type'] = 'sgnodeobject';
  $form_state['sgnodeobject'] = $sgnodeobject;
  
  $types = sgnodeobject_types();
  $sgnodeobject_type = $types[$sgnodeobject->type_name];

  dpm($sgnodeobject);
  dpm($sgnodeobject_type);

  module_load_include('inc', 'cam3d_three', 'includes/cam3d_three.sgnodeobject');

  // Retrieve controllers
  $sgnodeobject_controllers = cam3d_three_retrieve_sgnodeobjectcontrollers();
  $sgnodeobject_controller_types = array_keys($sgnodeobject_controllers);

  $sgnodeobject_types = array();
  foreach ($sgnodeobject_controllers as $sgnodeobject_controller_type => $sgnodeobject_controller_details) {
    $sgnodeobject_types[$sgnodeobject_controller_type] = $sgnodeobject_controller_details['label'];
  }
  $default_sgnodeobject_type = $sgnodeobject_type['default_three_type'];

  if (!empty($sgnodeobject->sgnoid)) {
    $form['sgnoid'] = array(
      '#title' => t('Object ID'),
      '#type' => 'textfield',
      '#default_value' => $sgnodeobject->sgnoid,
      '#weight' => -60,
      '#attributes' => array(
        'disabled' => 'disabled'
      ),
    );
  }

  if (!empty($sgnodeobject->created_at)) {
    $form['created_at'] = array(
      '#title' => t('Created At'),
      '#type' => 'textfield',
      '#default_value' => format_date($sgnodeobject->created_at),
      '#weight' => -50,
      '#attributes' => array(
        'disabled' => 'disabled'
      ),
    );
  }
  
  $form['object_type'] = array(
    '#title' => t('Object Type'),
    '#type' => 'select',
    '#options' => $sgnodeobject_types,
    '#required' => TRUE,
    '#default_value' => isset($sgnodeobject->object_type) ? $sgnodeobject->object_type : $default_sgnodeobject_type,
    '#weight' => -40,
  );

  $form['name'] = array(
    '#title' => t('Name'),
    '#type' => 'textfield',
    '#default_value' => isset($sgnodeobject->name) ? $sgnodeobject->name : '',
    '#required' => TRUE,
    '#weight' => -30,
  );
  
  $form['orphan'] = array(
    '#title' => t('Is An Orphan'),
    '#description' => t('Whether this item has been detected to not currently have any parent objects.'),
    '#type' => 'textfield',
    '#default_value' => isset($sgnodeobject->orphan) ? $sgnodeobject->orphan : true,
    '#weight' => -20,
    '#attributes' => array(
      'disabled' => 'disabled'
    ),
  );
  
  field_attach_form('sgnodeobject', $sgnodeobject, $form, $form_state);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 10,
  );

  return $form;
}
function sgnodeobject_form_submit($form, &$form_state) {

  unset($form_state['values']['sgnoid']);
  unset($form_state['values']['created_at']);
  unset($form_state['values']['orphan']);  

  $sgnodeobject = entity_ui_form_submit_build_entity($form, $form_state);

  $sgnodeobject->save();

  drupal_set_message(t('@name sgnodeobject has been saved.', array('@name' => $sgnodeobject->name)));
  $form_state['redirect'] = CAM3D_THREE_ENTITY_PATH;
}

function sgnodeobject_type_form_wrapper($sgnodeobject_type, $op = 'edit') {
  // Include the forms file from the Product module.
  return drupal_get_form('sgnodeobject_type_form', $sgnodeobject_type, 'edit');
}
function sgnodeobject_type_form($form, &$form_state, $sgnodeobject_type = NULL, $op = 'edit') {
  
  module_load_include('inc', 'cam3d_three', 'includes/cam3d_three.sgnodeobject');

  // Retrieve controllers
  $sgnodeobject_controllers = cam3d_three_retrieve_sgnodeobjectcontrollers();
  $sgnodeobject_controller_types = array_keys($sgnodeobject_controllers);

  $sgnodeobject_types = array();
  foreach ($sgnodeobject_controllers as $sgnodeobject_controller_type => $sgnodeobject_controller_details) {
    $sgnodeobject_types[$sgnodeobject_controller_type] = $sgnodeobject_controller_details['label'];
  }
  $default_sgnodeobject_type = reset($sgnodeobject_controller_types);
  
  if ($op == 'clone') {
    $sgnodeobject_type->name .= ' (cloned)';
    $option_set->type_id = '';
  }

  $form['name'] = array(
    '#title' => t('Name'),
    '#type' => 'textfield',
    '#default_value' => isset($sgnodeobject_type->name) ? $sgnodeobject_type->name : '',
    '#required' => TRUE,
    '#weight' => -50,
  );

  $form['type_name'] = array(
    '#type' => 'machine_name',
    '#title' => t('Type Name (machine name)'),
    '#default_value' => isset($sgnodeobject_type->type_name) ? $sgnodeobject_type->type_name : '',
    '#maxlength' => 21,
    '#machine_name' => array(
        'source' => array('name'),
        'replace_pattern' => '[^a-z0-9-]+',
        'replace' => '-',
        'exists' => 'sgnodeobject_type_type_name_exists',
    ),
    '#disabled' => !empty($sgnodeobject_type->type_name)
  );

  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textfield',
    '#default_value' => isset($sgnodeobject_type->description) ? $sgnodeobject_type->description : '',
    '#required' => TRUE,
    '#weight' => -40,
  );

  $form['default_three_type'] = array(
    '#title' => t('Default Three.js object type'),
    '#type' => 'select',
    '#options' => $sgnodeobject_types,
    '#required' => TRUE,
    '#default_value' => isset($sgnodeobject_type->default_three_type) ? $sgnodeobject_type->default_three_type : $default_sgnodeobject_type,
    '#weight' => -30,
  );
  
  $form['available_three_types'] = array(
    '#title' => t('Available Three.js types'),
    '#type' => 'checkboxes',
    '#options' => $sgnodeobject_types,
    '#default_value' => isset($sgnodeobject_type->available_three_types) ? $sgnodeobject_type->available_three_types : array(),
    '#weight' => -20,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 10,
  );

  return $form;
}
function sgnodeobject_type_type_name_exists($name) {
  $name_exists = db_query_range('SELECT 1 FROM {sgnodeobject_type} WHERE type_name = :name', 0, 1, array(':name' => $name))->fetchField();
  return $name_exists;
}
function sgnodeobject_type_form_submit($form, &$form_state) {
  
  $available_three_types = array();
  foreach ($form_state['values']['available_three_types'] as $controller_key => $state) {
    if (strcmp($controller_key, $state) == 0) $available_three_types[] = $controller_key;
  }
  $sgnodeobject_type->available_three_types = $available_three_types;
  
  $sgnodeobject_type = entity_ui_form_submit_build_entity($form, $form_state);
  
  sgnodeobject_type_save($sgnodeobject_type);

  drupal_set_message(t('@name sgnodeobject type has been saved.', array('@name' => $sgnodeobject_type->name)));
  $form_state['redirect'] = CAM3D_THREE_ENTITY_TYPES_PATH;
}

