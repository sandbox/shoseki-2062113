<?php

define('CAM3D_THREE_DEFAULT_IMAGE', -1);
define('CAM3D_THREE_NULL_IMAGE', -2);

function cam3d_three_retrieve_sgnodeobjectcontrollers() {
    $data = array();
    $hook_name = 'sgnodeobjectcontrollers_info';
    $data = module_invoke_all($hook_name);
    drupal_alter($hook_name, $data);
    return $data;
}

// Demo hook_sgnodeobjectcontrollers_info
/*function cam3d_three_sgnodeobjectcontrollers_info_alter(&$data) {
  drupal_add_http_header('Content-Type', 'application/json');
  print json_encode($data);
  drupal_exit();
}*/

/**
 * Implements hook_sgnodeobjectcontrollers_info().
 */
function cam3d_three_sgnodeobjectcontrollers_info() {

  global $base_root;

  // Default position for objects
  $default_position = new stdClass();
  $default_position->x = 0.0;
  $default_position->y = 0.0;
  $default_position->z = 0.0;

  // Default rotation for objects
  $default_rotation = new stdClass();
  $default_rotation->x = 0.0;
  $default_rotation->y = 0.0;
  $default_rotation->z = 0.0;

  // Default geometry and materials for mesh
  $default_geometry = new stdClass();
  $default_material = new stdClass();

  // Default scale for objects
  $default_scale = new stdClass();
  $default_scale->x = 1.0;
  $default_scale->y = 1.0;
  $default_scale->z = 1.0;

  // Default scripts for objects
  $default_scripts = array();

  // Default autoloaded objects
  $default_autoloaded_objects = array();

  $controllers = array(
    'scene' => array(
      'label' => 'Scene',
      'data_controller' => 'SceneObjectCAM3DController',
      'variables' => array(
        'object_type' => array('default_value' => 'scene', 'type' => 'string'),
      ),
    ),
    'mesh' => array(
      'label' => 'Mesh',
      'data_controller' => 'MeshObjectCAM3DController',
      'variables' => array(
        'geometry_id' => array('default_value' => -1, 'type' => 'integer'),
        'geometry' => array('default_value' => $default_geometry, 'type' => 'object'),
        'material_id' => array('default_value' => -1, 'type' => 'integer'),
        'material' => array('default_value' => $default_material, 'type' => 'object'),
        'rotation' => array('default_value' => $default_rotation, 'type' => 'object'),
        'scale' => array('default_value' => $default_scale, 'type' => 'object'),
        'object_type' => array('default_value' => 'mesh', 'type' => 'string'),
        'autoload_parameters' => array('default_value' => true, 'type' => 'bool'),
      ),
    ),
    'sprite' => array(
      'label' => 'Sprite',
      'data_controller' => 'SpriteObjectCAM3DController',
      'variables' => array(
        'material_id' => array('default_value' => -1, 'type' => 'integer'),
        'object_type' => array('default_value' => 'sprite', 'type' => 'string'),
      ),
    ),
    'pointlight' => array(
      'label' => 'Point Light',
      'data_controller' => 'PointLightObjectCAM3DController',
      'variables' => array(
        'color' => array('default_value' => 0xffffff, 'type' => 'integer'),
        'intensity' => array('default_value' => 1, 'type' => 'integer'),
        'distance' => array('default_value' => 0, 'type' => 'integer'),
        'object_type' => array('default_value' => 'pointlight', 'type' => 'string'),
      ),
    ),
    'spotlight' => array(
      'label' => 'Spot Light',
      'data_controller' => 'SpotLightObjectCAM3DController',
      'variables' => array(
        'color' => array('default_value' => 0xffffff, 'type' => 'integer'),
        'intensity' => array('default_value' => 1, 'type' => 'integer'),
        'distance' => array('default_value' => 0, 'type' => 'integer'),
        'angle' => array('default_value' => pi() * 0.1, 'type' => 'double'),
        'exponent' => array('default_value' => 10, 'type' => 'integer'),
        'object_type' => array('default_value' => 'spotlight', 'type' => 'string'),
      ),
    ),
    'directionallight' => array(
      'label' => 'Directional Light',
      'data_controller' => 'DirectionalLightObjectCAM3DController',
      'variables' => array(
        'color' => array('default_value' => 0xffffff, 'type' => 'integer'),
        'intensity' => array('default_value' => 1, 'type' => 'integer'),
        'object_type' => array('default_value' => 'directionallight', 'type' => 'string'),
      ),
    ),
    'hemispherelight' => array(
      'label' => 'Hemisphere Light',
      'data_controller' => 'HemisphereLightObjectCAM3DController',
      'variables' => array(
        'skyColor' => array('default_value' => 0x00aaff, 'type' => 'integer'),
        'groundColor' => array('default_value' => 0xffaa00, 'type' => 'integer'),
        'intensity' => array('default_value' => 1, 'type' => 'integer'),
        'object_type' => array('default_value' => 'hemispherelight', 'type' => 'string'),
      ),
    ),
    'ambientlight' => array(
      'label' => 'Ambient Light',
      'data_controller' => 'AmbientLightObjectCAM3DController',
      'variables' => array(
        'color' => array('default_value' => 0xffffff, 'type' => 'integer'),
        'object_type' => array('default_value' => 'ambientlight', 'type' => 'string'),
      ),
    ),
    'perspectivecamera' => array(
      'label' => 'Perspective Camera',
      'data_controller' => 'PerspectiveCameraObjectCAM3DController',
      'variables' => array(
        'rotation' => array('default_value' => $default_rotation, 'type' => 'object'),
        'scale' => array('default_value' => $default_scale, 'type' => 'object'),
        'fov' => array('default_value' => 75.0, 'type' => 'double'),
        'aspect' => array('default_value' => 640.0 / 480.0, 'type' => 'double'),
        'near' => array('default_value' => 1.0, 'type' => 'double'),
        'far' => array('default_value' => 1000.0, 'type' => 'double'),
        'object_type' => array('default_value' => 'perspectivecamera', 'type' => 'string'),
      ),
    ),
    'orthographiccamera' => array(
      'label' => 'Orthographic Camera',
      'data_controller' => 'OrthographicCameraObjectCAM3DController',
      'variables' => array(
        'rotation' => array('default_value' => $default_rotation, 'type' => 'object'),
        'scale' => array('default_value' => $default_scale, 'type' => 'object'),
        'left' => array('default_value' => -320.0, 'type' => 'double'),
        'right' => array('default_value' => 320.0, 'type' => 'double'),
        'top' => array('default_value' => 240.0, 'type' => 'double'),
        'bottom' => array('default_value' => -240.0, 'type' => 'double'),
        'near' => array('default_value' => 1.0, 'type' => 'double'),
        'far' => array('default_value' => 1000.0, 'type' => 'double'),
        'object_type' => array('default_value' => 'orthographiccamera', 'type' => 'string'),
      ),
    ),
  );

  foreach ($controllers as $controllers_key => $controller) {
    $controllers[$controllers_key]['variables']['children'] = array('default_value' => array(), 'type' => 'array');
    $controllers[$controllers_key]['variables']['object_id'] = array('default_value' => -1, 'type' => 'integer');
    $controllers[$controllers_key]['variables']['position'] = array('default_value' => $default_position, 'type' => 'object');
    $controllers[$controllers_key]['variables']['name'] = array('default_value' => '', 'type' => 'string');
    $controllers[$controllers_key]['variables']['scripts'] = array('default_value' => $default_scripts, 'type' => 'object');
    $controllers[$controllers_key]['variables']['autoload_children'] = array('default_value' => true, 'type' => 'bool');
    $controllers[$controllers_key]['variables']['autoloaded_objects'] = array('default_value' => $default_autoloaded_objects, 'type' => 'array');
  }

  return $controllers;
}

function cam3d_three_retrieve_variable_postprocessing_info() {
  $data = array();
  $hook_name = 'cam3d_three_variable_postprocessing_info';
  $data = module_invoke_all($hook_name);
  drupal_alter($hook_name, $data);
  return $data;
}

function cam3d_three_cam3d_three_variable_postprocessing_info() {

  $variable_postprocessing_info = array();
  $variable_postprocessing_info['image'] = '_cam3d_three_postprocess_image';
  return $variable_postprocessing_info;
}

function _cam3d_three_postprocess_image($consolidated_variables, $consolidated_variable_key) {
    
    $file_id = $consolidated_variables[$consolidated_variable_key];
    
    // If this function gets called multiple times, it only needs to convert an id into an object once
    if (is_object($file_id)) {
        return $file_id;
    }

    if ($file_id < 0) {
      if ($file_id == -1) $file_id = variable_get('cam3d_three_default_tex0_id', -1);
    }
    if ($file_id >= 0) {
        $file_entity = file_load($file_id);

        $image = new stdClass();
        $image->url = file_create_url($file_entity->uri);

        return $image;
    }
    return null;
}

// Base Class
class GenericCAM3DController {
    
    public function consolidate_variables($default_variables, $variables) {

        $consolidated_variables = array();
        
        // Copy across the default variables, assuming they are of the correct ype
        foreach ($default_variables as $default_variable_key => $default_variable_value) {
            $consolidated_variables[$default_variable_key] = $default_variables[$default_variable_key]['default_value'];
        }

        // Copy across the loaded variables where they exist in original defaults
        foreach ($default_variables as $default_variable_key => $default_variable_value) {
            if (!empty($variables[$default_variable_key])) {
                $consolidated_variables[$default_variable_key] = $variables[$default_variable_key];
            }
        }
        
        $variable_postprocessing_info = cam3d_three_retrieve_variable_postprocessing_info();

        // Finally, some postprocessing, from 
        foreach ($consolidated_variables as $consolidated_variable_key => $consolidated_variable_value) {
            $type = $default_variables[$consolidated_variable_key]['type'];
            
            if (!empty($variable_postprocessing_info[$type])) {
                $consolidated_variables[$consolidated_variable_key] = $variable_postprocessing_info[$type]($consolidated_variables, $consolidated_variable_key);
            }
        }

        return $consolidated_variables;
    }
    public function generate_json_wrapper($default_variables, $variables) {

        $consolidated_variables = $this->consolidate_variables($default_variables, $variables);
        
        $json_wrapper = new stdClass();       
        foreach($consolidated_variables as $key => $value) {
            $json_wrapper->{$key} = $value;
        }

        return $json_wrapper;
    }
    public function generate_json($default_variables, $variables) {
        return $this->generate_json_wrapper($default_variables, $variables);
    }
    public function front_end_controllers($variables) {
        
        $files = array();
        
        global $base_root;
        $files[] = $base_root . '/' . drupal_get_path('module', 'cam3d_three') . '/js/sgnodeobjectcontrollers.core.js';
        
        // We include the other two files to simplify the default loaded controllers
        $files[] = $base_root . '/' . drupal_get_path('module', 'cam3d_three') . '/js/sgnodegeometrycorecontrollers.core.js';
        $files[] = $base_root . '/' . drupal_get_path('module', 'cam3d_three') . '/js/sgnodematerialcorecontrollers.core.js';
        
        // Check to see if $variables contains the editor value, in which case add core.ui controllers
        if (!empty($variables['ui'])) {
            $files[] = $base_root . '/' . drupal_get_path('module', 'cam3d_three') . '/js/sgnodeobjectcontrollers.core.ui.js';
            $files[] = $base_root . '/' . drupal_get_path('module', 'cam3d_three') . '/js/sgnodegeometrycorecontrollers.core.ui.js';
            $files[] = $base_root . '/' . drupal_get_path('module', 'cam3d_three') . '/js/sgnodematerialcorecontrollers.core.ui.js';
        }
        
        return $files;
    }
    public function create($default_variables, $variables, $type = 'generic') {

      // Create an entity and save variables in it
      $entity = entity_get_controller('sgnodeobject')->create();
      
      $entity->variables = $this->consolidate_variables($default_variables, $variables);
      $entity->object_type = $type;
      $entity->title = $type;

      if (!empty($entity->variables['object_id'])) unset($entity->variables['object_id']);
      
      $entity = sgnodeobject_save($entity);
      
      // Save entity converts the variables array into a string      
      return $this->read($default_variables, $entity);
    }
    public function read_sub_controller($default_object_level_variables, $variables) {
        return (null);
    }
    public function read($default_variables, $entity) {
        
        // $id by default is the id of our entity
        // Parameters includes name => default_value and type

        if (is_string($entity->variables)) $entity->variables = unserialize($entity->variables);

        $variables = !empty($entity->variables) ? $entity->variables : array();
        $variables['object_id'] = $entity->basic_id;
        if (is_string($variables['object_id'])) $variables['object_id'] = intval($variables['object_id']);
        $variables['name'] = $entity->title;
        
        if ($variables['autoload_children'] || true) {
            $variables['autoloaded_objects'] = array();
            
            $controllers = cam3d_three_retrieve_sgnodeobjectcontrollers();
            
            $child_ids = $variables['children'];
            
            foreach ($child_ids as $child_id) {
                
                $child_entity = sgnodeobject_load($child_id);
                
                $child_object_type = $child_entity->object_type;
                $child_object_controller = new $controllers[$child_object_type]['data_controller']();
                $child_object_default_variables = $controllers[$child_object_type]['variables'];

                $child_object_json_object = $child_object_controller->read($child_object_default_variables, $child_entity);

                $variables['autoloaded_objects'][$child_id] = $child_object_json_object;
            }
        }
        
        return $this->generate_json($default_variables, $variables);
    }
    public function update($default_variables, $variables) {

      $id = $variables['object_id'];
      unset($variables['object_id']);
      
      $type = $variables['object_type'];
      unset($variables['object_type']);
      
      $name = $variables['name'];
      unset($variables['name']);
      
      // Load the entity from the db, update it and save
      $entity = sgnodeobject_load($id);

      // Save title
      $entity->title = $name;
      
      // Consolidate variables, this has been replaced
      // $original_variables = $entity->variables;
      
      // Save variables
      $entity->variables = $variables;
      
      // Save entity      
      $entity = sgnodeobject_save($entity);      

      return $this->read($default_variables, $entity);
    }
    public function delete($id) {
      // Delete the entity from the db
      $entity = sgnodeobject_load($id);
      sgnodeobject_delete($entity);
    }
}
class SceneObjectCAM3DController extends GenericCAM3DController {
    public function create($default_variables, $variables, $type = 'scene') {
        return parent::create($default_variables, $variables, $type);
    }
}
class MeshObjectCAM3DController extends GenericCAM3DController {

    public function create($default_variables, $variables, $type = 'mesh') {
        if (!empty($variables['geometry_type'])) {
            $geometry_type = $variables['geometry_type'];
            $controllers = cam3d_three_retrieve_sgnodegeometrycorecontrollers();
            $controller = new $controllers[$geometry_type]['data_controller']();
            $geometry_default_variables = $controllers[$geometry_type]['variables'];
            $geometry_json = $controller->create($geometry_default_variables, array());
            unset($variables['geometry_type']);
            $variables['geometry_id'] = $geometry_json->geometry_id;
            if (is_string($variables['geometry_id'])) $variables['geometry_id'] = intval($variables['geometry_id']);
        }
        if (!empty($variables['material_type'])) {
            $material_type = $variables['material_type'];
            $controllers = cam3d_three_retrieve_sgnodematerialcorecontrollers();
            $controller = new $controllers[$material_type]['data_controller']();
            $material_default_variables = $controllers[$material_type]['variables'];
            $material_json = $controller->create($material_default_variables, array());
            unset($variables['material_type']);
            $variables['material_id'] = $material_json->material_id;
            if (is_string($variables['material_id'])) $variables['material_id'] = intval($variables['material_id']);
        }
        return parent::create($default_variables, $variables, $type);
    }
    public function read_geometry($geometry_id) {
        if ($geometry_id < 0) $geometry_id = variable_get('cam3d_three_default_geometry_id', -1);

        $file_entity = file_load($geometry_id);
        
        $file_stream_wrapper = file_stream_wrapper_get_instance_by_uri($file_entity->uri);

        $parameters = $file_stream_wrapper->get_parameters();
        $geometry_type = $parameters['geometry_type'];
        
        $controllers = cam3d_three_retrieve_sgnodegeometrycorecontrollers();
        
        $controller = new $controllers[$geometry_type]['data_controller']();
        
        $default_variables = $controllers[$geometry_type]['variables'];
        
        return $controller->read($default_variables, $file_entity);
    }
    
    public function read_material($material_id) {
        if ($material_id < 0) $material_id = variable_get('cam3d_three_default_material_id', -1);

        $file_entity = file_load($material_id);

        $file_stream_wrapper = file_stream_wrapper_get_instance_by_uri($file_entity->uri);

        $parameters = $file_stream_wrapper->get_parameters();
        $material_type = $parameters['material_type'];

        $controllers = cam3d_three_retrieve_sgnodematerialcorecontrollers();
        $controller = new $controllers[$material_type]['data_controller']();
        $default_variables = $controllers[$material_type]['variables'];
        return $controller->read($default_variables, $file_entity);
    }
    
    public function read($default_variables, $entity) {
        
        // $id by default is the id of our entity
        // Parameters includes name => default_value and type

        if (is_string($entity->variables)) $entity->variables = unserialize($entity->variables);

        $variables = !empty($entity->variables) ? $entity->variables : array();
        $variables['object_id'] = $entity->basic_id;
        if (is_string($variables['object_id'])) $variables['object_id'] = intval($variables['object_id']);
        $variables['name'] = $entity->title;

        if ($variables['autoload_parameters'] || true) {
            
            $variables['geometry'] = $this->read_geometry($variables['geometry_id']);
            $variables['material'] = $this->read_material($variables['material_id']);            
        }

        if ($variables['autoload_children'] || true) {
            
            $variables['autoloaded_objects'] = array();
            
            $controllers = cam3d_three_retrieve_sgnodeobjectcontrollers();
            
            $child_ids = $variables['children'];
            
            foreach ($child_ids as $child_id) {
                
                $child_entity = sgnodeobject_load($child_id);
                
                $child_object_type = $child_entity->object_type;
                $child_object_controller = new $controllers[$child_object_type]['data_controller']();
                $child_object_default_variables = $controllers[$child_object_type]['variables'];

                $child_object_json_object = $child_object_controller->read($child_object_default_variables, $child_entity);

                $variables['autoloaded_objects'][$child_id] = $child_object_json_object;
            }
        }
        
        return $this->generate_json($default_variables, $variables);
    }
    public function update($default_variables, $variables) {

      // This could be called in three instances
      // Firstly, when the object itself is updated
      // Second and thirdly, when the geometry or material is updated
      if (!empty($variables['geometry'])) {
          
          $geometry = get_object_vars($variables['geometry']);

          $geometry_type = $geometry['geometry_type'];
          $controllers = cam3d_three_retrieve_sgnodegeometrycorecontrollers();
          $controller = new $controllers[$geometry_type]['data_controller']();
          
          $json_wrapper = new stdClass();
          $json_wrapper->geometry = $controller->update($controllers[$geometry_type]['variables'], $geometry);
          $json_wrapper->geometry->object_type = 'mesh';
          $json_wrapper->object_type = 'mesh';

          return $json_wrapper;
      }
      else if (!empty($variables['material'])) {

          $material = get_object_vars($variables['material']);
          $material_type = $material['material_type'];
          $controllers = cam3d_three_retrieve_sgnodematerialcorecontrollers();
          $controller = new $controllers[$material_type]['data_controller']();

          $json_wrapper = new stdClass();
          $json_wrapper->material = $controller->update($controllers[$material_type]['variables'], $material);
          $json_wrapper->material->object_type = 'mesh';
          $json_wrapper->object_type = 'mesh';

          return $json_wrapper;
      }
      else {
          return parent::update($default_variables, $variables);
      }
    }
    public function generate_json($default_variables, $variables) {
        $wrapper = $this->generate_json_wrapper($default_variables, $variables);
        $wrapper->object_type = 'mesh';
        if ($wrapper->material_id == -1) $wrapper->material_id = variable_get('cam3d_three_default_material_id', -1);
        return ($wrapper);
    }
    public function read_sub_controller($default_object_level_variables, $variables) {

        if (!empty($variables['geometry_id'])) {

            $json_wrapper = new stdClass();
            $json_wrapper->geometry = $this->read_geometry($variables['geometry_id']);
            $json_wrapper->geometry->object_type = 'mesh';
            $json_wrapper->object_type = 'mesh';

            return $json_wrapper;
        }

        if (!empty($variables['material_id'])) {

            $json_wrapper = new stdClass();
            $json_wrapper->material = $this->read_material($variables['material_id']);
            $json_wrapper->material->object_type = 'mesh';
            $json_wrapper->object_type = 'mesh';

            return $json_wrapper;
        }
        return (null);
    }
    public function front_end_controllers($variables) {
        $files = array();
        global $base_root;

        $files[] = $base_root . '/' . drupal_get_path('module', 'cam3d_three') . '/js/sgnodeobjectcontrollers.core.js';

        // Check to see if $variables contains the editor value, in which case add core.ui controllers
        if (!empty($variables['ui'])) {
            $files[] = $base_root . '/' . drupal_get_path('module', 'cam3d_three') . '/js/sgnodeobjectcontrollers.core.ui.js';
        }
        
        if (!empty($variables['geometry'])) {
            $geometry = get_object_vars($variables['geometry']);

            // Need to pass this through so subcontrollers get to declare their own ui elements if they wish
            if (!empty($variables['ui'])) $geometry['ui'] = $variables['ui'];

            $geometry_type = $geometry['geometry_type'];
            $controllers = cam3d_three_retrieve_sgnodegeometrycorecontrollers();
            $controller = new $controllers[$geometry_type]['data_controller']();
            $files = array_merge($files, $controller->front_end_controllers($geometry));
        }
        if (!empty($variables['material'])) {
            $material = get_object_vars($variables['material']);

            // Need to pass this through so subcontrollers get to declare their own ui elements if they wish
            if (!empty($variables['ui'])) $material['ui'] = $variables['ui'];

            $material_type = $material['material_type'];
            $controllers = cam3d_three_retrieve_sgnodematerialcorecontrollers();
            $controller = new $controllers[$material_type]['data_controller']();
            $files = array_merge($files, $controller->front_end_controllers($material));
        }
        return $files;
    }
}
class SpriteObjectCAM3DController extends GenericCAM3DController {
    public function create($default_variables, $variables, $type = 'sprite') {
      return parent::create($default_variables, $variables, $type);
    }
    public function generate_json($default_variables, $variables) {
      $wrapper = $this->generate_json_wrapper($default_variables, $variables);
      $wrapper->object_type = 'sprite';
      return ($wrapper);
    }
    public function read_sub_controller($default_object_level_variables, $variables) {
      if (!empty($variables['material_id'])) {
        $material_id = $variables['material_id'];
        if ($material_id < 0) $material_id = variable_get('cam3d_three_default_material_id', -1);
        
        $file_entity = file_load($material_id);

        $file_stream_wrapper = file_stream_wrapper_get_instance_by_uri($file_entity->uri);

        $parameters = $file_stream_wrapper->get_parameters();
        $material_type = $parameters['material_type'];

        $controllers = cam3d_three_retrieve_sgnodematerialcorecontrollers();
        $controller = new $controllers[$material_type]['data_controller']();
        $default_variables = $controllers[$material_type]['variables'];
        
        $json_wrapper = new stdClass();
        
        $json_wrapper->material = $controller->read($default_variables, $file_entity);
        $json_wrapper->object_type = 'sprite';
        return $json_wrapper;
      }
      return (null);
    }
    public function front_end_controllers($variables) {

        $files = array();
        global $base_root;

        $files[] = $base_root . '/' . drupal_get_path('module', 'cam3d_three') . '/js/sgnodeobjectcontrollers.core.js';
        
        if (!empty($variables['material'])) {
          $material = get_object_vars($variables['material']);
          
          $material_type = $material['material_type'];

          $controllers = cam3d_three_retrieve_sgnodematerialcorecontrollers();
          $controller = new $controllers[$material_type]['data_controller']();
          $files = array_merge($files, $controller->front_end_controllers($material));
        }

        return $files;
    }
}
class PointLightObjectCAM3DController extends GenericCAM3DController {
    public function create($default_variables, $variables, $type = 'pointlight') {
        return parent::create($default_variables, $variables, $type);
    }
}
class SpotLightObjectCAM3DController extends GenericCAM3DController {
    public function create($default_variables, $variables, $type = 'spotlight') {
        return parent::create($default_variables, $variables, $type);
    }
}
class DirectionalLightObjectCAM3DController extends GenericCAM3DController {
    public function create($default_variables, $variables, $type = 'directionallight') {
        return parent::create($default_variables, $variables, $type);
    }
}
class HemisphereLightObjectCAM3DController extends GenericCAM3DController {
    public function create($default_variables, $variables, $type = 'hemispherelight') {
        return parent::create($default_variables, $variables, $type);
    }
}
class AmbientLightObjectCAM3DController extends GenericCAM3DController {
    public function create($default_variables, $variables, $type = 'ambientlight') {
        return parent::create($default_variables, $variables, $type);
    }
}
class PerspectiveCameraObjectCAM3DController extends GenericCAM3DController {
    public function create($default_variables, $variables, $type = 'perspectivecamera') {
        return parent::create($default_variables, $variables, $type);
    }
}
class OrthographicCameraObjectCAM3DController extends GenericCAM3DController {
    public function create($default_variables, $variables, $type = 'orthographiccamera') {
        return parent::create($default_variables, $variables, $type);
    }
}

// A controller for File Entity based 3D Objects
class GenericFileEntityCAM3DController extends GenericCAM3DController {

    // This function converts a series of parameters from strings into other data according to the default variables' type
    public function deserialize_parameters($default_variables, $variables) {
        $processed_variables = array();
        
        // Variables enter this function as strings
        foreach ($default_variables as $default_variable_key => $default_variable_value) {
            if (isset($variables[$default_variable_key])) {

                switch($default_variables[$default_variable_key]['type']) {

                    case 'integer' : case 'image' :
                        $processed_variables[$default_variable_key] = intval($variables[$default_variable_key]);
                        break;
                    case 'double' :
                        $processed_variables[$default_variable_key] = floatval($variables[$default_variable_key]);
                        break;
                    case 'boolean' :
                        $processed_variables[$default_variable_key] = $variables[$default_variable_key] === 'true' ? true : false;
                        break;
                    case 'array' : case 'vec2f' :
                        $processed_variables[$default_variable_key] = unserialize(urldecode($variables[$default_variable_key]));
                        break;
                    case 'uri' : case 'string' :
                        $processed_variables[$default_variable_key] = urldecode($variables[$default_variable_key]);
                        break;
                }
            }
        }
        return $processed_variables;
    }

    // This function converts a series of parameters from strings into other data according to the default variables' type
    public function serialize_parameters($default_variables, $variables) {
        $processed_variables = array();

        // Variables enter this function as strings
        foreach ($default_variables as $default_variable_key => $default_variable_value) {
            if (isset($variables[$default_variable_key])) {
                switch($default_variables[$default_variable_key]['type']) {
                    case 'integer' : case 'image' :
                        $processed_variables[$default_variable_key] = strval($variables[$default_variable_key]);
                        break;
                    case 'double' :
                        $processed_variables[$default_variable_key] = strval($variables[$default_variable_key]);
                        break;
                    case 'boolean' :
                        $processed_variables[$default_variable_key] = ($variables[$default_variable_key]) ? 'true' : 'false';
                        break;
                    case 'array' : case 'vec2f' :
                        $processed_variables[$default_variable_key] = urlencode(serialize($variables[$default_variable_key]));
                        break;
                    case 'uri' : case 'string' :
                        $processed_variables[$default_variable_key] = urlencode($variables[$default_variable_key]);
                        break;
                }
            }
        }
        return $processed_variables;
    }

    public function generate_json_wrapper($default_variables, $variables) {
        
        // Need to convert pure string parameters / unfiltered parameters into filtered / processed parameters
        $processed_variables = $this->deserialize_parameters($default_variables, $variables);
        $consolidated_variables = $this->consolidate_variables($default_variables, $variables);

        $json_wrapper = new stdClass();       
        foreach($consolidated_variables as $key => $value) {
            $json_wrapper->{$key} = $value;
        }
        
        return $json_wrapper;
    }    
    
    public function delete($id) {
      // Delete the entity from the db
      $file = file_load($id);

      file_delete($file, TRUE);
    }
}