<?php

// Function to call when retrieving typefaces, in the form $typeface_fonts['font']['weight]['style] = 'font_file_name.js';
function cam3d_three_retrieve_additional_scripts() {
    $data = array();
    $hook_name = 'additional_scripts_info';
    $data = module_invoke_all($hook_name);
    drupal_alter($hook_name, $data);
    return $data;
}

/**
 * Implements hook_additional_scripts_info().
 */
function cam3d_three_additional_scripts_info() {
    $additional_scripts = array();

    // Declare available additional scripts here
    // global $base_root;
    // $three_physijs_path = libraries_get_path('physijs.js');
    // $additional_scripts['physijs'] = $base_root . '/' . $three_physijs_path . '/physijs.js';
    
    return $additional_scripts;
}