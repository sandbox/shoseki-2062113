<?php

/**
 * Implements hook_field_formatter_info().
 */
function cam3d_three_field_formatter_info() {
  return array(
    'entityreference_cam3d_three_scene' => array(
      'label' => t('CAM3D Three Scene Formatter'),
      'description' => t('Display the referenced entities rendered as a CAM3D Scene.'),
      'field types' => array('entityreference'),
      'settings' => array(
        'autosize' => false,
        'aspect_ratio' => 1.333,
        'width' => '320',
        'height' => '240',
        'enable_vr' => false,
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function cam3d_three_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {

    //This gets the view_mode where our settings are stored
    $display = $instance['display'][$view_mode];

    //This gets the actual settings
    $settings = $display['settings'];

    $element = array();

    $element['autosize'] = array(
      '#type'           => 'checkbox',                        // Use a textbox
      '#title'          => t('Autosize'),                      // Widget label
      '#description'    => t('This autoexpands the canvas to fit its\' parent container, useful for responsive designs.'),  // helper text
      '#default_value'  => $settings['autosize'],               // Get the value if it's already been set
    );
    $element['aspect_ratio'] = array(
      '#type'           => 'textfield',                        // Use a textbox
      '#title'          => t('Aspect Ratio'),                      // Widget label
      '#description'    => t('When using autoresize, this will generate the height based on the width.'),  // helper text
      '#default_value'  => $settings['aspect_ratio'],               // Get the value if it's already been set
    );    
    $element['width'] = array(
      '#type'           => 'textfield',                        // Use a textbox
      '#title'          => t('Width'),                      // Widget label
      '#description'    => t('This determines the width of the canvas if not using autosize.'),  // helper text
      '#default_value'  => $settings['width'],               // Get the value if it's already been set
    );
    $element['height'] = array(
      '#type'           => 'textfield',                        // Use a textbox
      '#title'          => t('Height'),                      // Widget label
      '#description'    => t('This determines the height of the canvas if not using autosize.'),  // helper text
      '#default_value'  => $settings['height'],               // Get the value if it's already been set
    );
    $element['enable_vr'] = array(
      '#type'           => 'checkbox',                        // Use a textbox
      '#title'          => t('Enable VR'),                      // Widget label
      '#description'    => t('This will enable VR for the Player where VR is supported, or simply fall back to original player if not.'),  // helper text
      '#default_value'  => $settings['enable_vr'],               // Get the value if it's already been set
    );

    return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function cam3d_three_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $summary_intro = t('Use a canvas of size ');

  if ($settings['autosize']) {
    $summary_size = t('(autosize)');
  }
  else $summary_size = t('(@width x @height)', array(
    '@width'     => $settings['width'],
    '@height'  => $settings['height'],
  )); // we use t() for translation and placeholders to guard against attacks

  $summary_vr_enabled = (!empty($settings['enable_vr']) && $settings['enable_vr']) ? t('WebVR Enabled') : '';
  
  if (strlen($summary_vr_enabled) > 0) $summary_vr_enabled = ', ' . $summary_vr_enabled;

  return $summary_intro . $summary_size . $summary_vr_enabled;
}

/**
 * Implements hook_field_formatter_view().
 */
function cam3d_three_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {

  $element = array();

  // Ensure this is applying to the correct tag
  switch ($display['type']) {

    case 'entityreference_cam3d_three_scene' :

      $display_settings = $display['settings'];

      // Define base tag
      $default_scene_div = array(
        '#theme' => 'html_tag',
        '#tag' => 'div',
        '#value' => 'We are sorry, but your browser does not seem to support WebGL.',
      );

      global $base_root;

      // Define base settings to be used in any case.
      $base_settings = array(
        'cam3d_three' => array(
          'base_root' => $base_root,
          'api_root' => 'cam3d_three/',
        ),
      );

      // Retrieve library paths
      $cam3d_three_path = drupal_get_path('module', 'cam3d_three');
      $three_path = libraries_get_path('three.js');

      // Loop through each item and attach html and javascript as required
      foreach ($items as $delta => $item) {

        $selector_class_pieces = array('cam3d_three_scene', $item['target_id'], $instance['field_id'], $instance['field_name']);
        if ($display_settings['autosize'] == 0) {
            array_push($selector_class_pieces, 'w', $display_settings['width'], 'h', $display_settings['height']);
        }
        else {
            array_push($selector_class_pieces, 'autoresize', 'aspect_ratio', $display_settings['aspect_ratio']);
        }
        
        // Create selector class
        $selector_class = drupal_html_class(implode(' ', $selector_class_pieces));

        // Create default div
        $element[$delta] = $default_scene_div;

        // Add selector class
        $element[$delta]['#attributes']['class'][] = $selector_class;

        // Copy across base JS settings
        $settings = $base_settings;

        $enable_vr = !empty($display_settings['enable_vr']) ? $display_settings['enable_vr'] : false;
        $aspect_ratio = !empty($display_settings['aspect_ratio']) ? $display_settings['aspect_ratio'] : 1.333;

        // Add selector / scene ID pair
        $div_settings = array(
          'scene_id' => intval($item['target_id']),
          'aspect_ratio' => floatval($aspect_ratio),
          'autosize' => $display_settings['autosize'],
          'width' => intval($display_settings['width']),
          'height' => intval($display_settings['height']),
          'enable_vr' => $enable_vr,
        );

        $settings['cam3d_three']['instances'][$selector_class] = $div_settings;
        $settings['cam3d_three']['vr_goggles_image'] = $base_root . '/' . $cam3d_three_path . '/images/vr_goggles_sm.png';

        // Attach settings to element to be accumulated into page settings
        $element[$delta]['#attached']['js'][] = array(
          'data' => $settings,
          'type' => 'setting',
        );

        // Attach the three.js and cam3d_three javascript behaviour
        $element[$delta]['#attached']['js'][] = $three_path . '/build/three.min.js';
        $element[$delta]['#attached']['js'][] = $cam3d_three_path . '/js/cam3d_three.js';

        if ($enable_vr) {
          //$element[$delta]['#attached']['js'][] = $three_path . '/examples/js/effects/VREffect.js';
          //$element[$delta]['#attached']['js'][] = $three_path . '/examples/js/controls/VRControls.js';
          $element[$delta]['#attached']['js'][] = $cam3d_three_path . '/js/effects/VREffect.js';
          $element[$delta]['#attached']['js'][] = $cam3d_three_path . '/js/controls/VRControls.js';
        }
      }
      break;
  }

  return $element;
}