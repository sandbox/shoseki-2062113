(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = 
          window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); }, 
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());

// Using the closure to map jQuery to $.
(function ($) {
	// Store our function as a property of Drupal.behaviors.
	Drupal.behaviors.cam3d_three = {
		attach: function (context, settings) {
			
			$.each(settings.cam3d_three, function(i, v) {
				
				var classSelectedCanvasContainers = $(i, context);
				
				$.each(classSelectedCanvasContainers, function(cscci, csccv) {

					var $currentCanvasContainer = $(csccv), canvasWidth = $currentCanvasContainer.width(), canvasHeight = $currentCanvasContainer.height();
					
					$currentCanvasContainer.once('cam3d-three', function() {
											
						var newMC  = {
							container : $currentCanvasContainer,
							canvas : null,
							renderer : null,
							uniforms : {
								"time": { type: "f", value: 1.0 },
								"resolution": { type: "v2", value: new THREE.Vector2() },
								"mouse" : { type: "v4", value: new THREE.Vector4(0, 0, 0, 0) }
							},
							scene : null,
							camera : null
						};

						newMC.renderer = new THREE.WebGLRenderer();

						newMC.scene = new THREE.Scene();

						newMC.camera = new THREE.Camera();
						newMC.camera.position.z = 1;

						newMC.renderer.setSize( canvasWidth, canvasHeight );
						newMC.uniforms.resolution.value.x = canvasWidth;
						newMC.uniforms.resolution.value.y = canvasHeight;

						newMC.canvas = newMC.renderer.domElement;
 
						$.each(["tex0", "tex1", "tex2", "tex3", "tex4"], function(ti, tv) {
							newMC['uniforms'][tv] = {
														type: "t",
														value: 0,
														texture: THREE.ImageUtils.loadTexture(v.images[ti])
							};
							newMC['uniforms'][tv]['texture']['wrapS'] = THREE.RepeatWrapping;
							newMC['uniforms'][tv]['texture']['wrapT'] = THREE.RepeatWrapping;
						});

						newMC.material = new THREE.ShaderMaterial({
							uniforms : newMC.uniforms,
							vertexShader: v.vertexShader,
							fragmentShader: v.fragmentShader
						});

						newMC.mesh = new THREE.Mesh( eval(v.model), newMC.material );
						newMC.scene.addObject( newMC.mesh );
	
						newMC.update = function() {
							newMC.uniforms.time.value += 0.025;
						};
						newMC.render = function() {
							newMC.renderer.render( newMC.scene, newMC.camera );
						};
	
						newMC.container	.empty()
							.append(newMC.canvas);
						
						if (!Drupal.behaviors.cam3d_three.managedCanvasCollection[i]) Drupal.behaviors.cam3d_three.managedCanvasCollection[i] = [];
						
						Drupal.behaviors.cam3d_three.managedCanvasCollection[i][cscci] = newMC;
					});
				});
			});
			
			if (!Drupal.behaviors.cam3d_three.animationThread) Drupal.behaviors.cam3d_three.animate();
		},
		// List of canvases we are managing
		managedCanvasCollection : {},
		render : function() {
			$.each(Drupal.behaviors.cam3d_three.managedCanvasCollection, function(i, v) {
				$.each(v, function(ci, cv) {
					cv.update();
					cv.render();
				});
			});
		},
		animate : function () {
			Drupal.behaviors.cam3d_three.render();
			Drupal.behaviors.cam3d_three.animationThread = requestAnimationFrame( Drupal.behaviors.cam3d_three.animate );
		},
		animationThread : null
	};
}(jQuery));
