
(function ($) {

    Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers = Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers || {};
    Drupal.behaviors.cam3d_three.sgnodegeometrycorecontrollers = Drupal.behaviors.cam3d_three.sgnodegeometrycorecontrollers || {};
    Drupal.behaviors.cam3d_three.sgnodematerialcorecontrollers = Drupal.behaviors.cam3d_three.sgnodematerialcorecontrollers || {};
    Drupal.behaviors.cam3d_three.objectcollection = Drupal.behaviors.cam3d_three.objectcollection || {};
    Drupal.behaviors.cam3d_three.geometrycollection = Drupal.behaviors.cam3d_three.geometrycollection || {};
    Drupal.behaviors.cam3d_three.materialcollection = Drupal.behaviors.cam3d_three.materialcollection || {};

    var genericControllerPrototype = {
        'create' : function(data, callback) {
            console.log('Generic Object controller create!');
        },
        'read' : function(data, callback) {
            console.log('Generic Object controller read!');
        },
        'update' : function(data, callback) {
            console.log('Generic Object controller update!');
        },
        'delete' : function(data, callback) {
            console.log('Generic Object controller delete!');
        }
    };

    var is_defined = function(parameter) {
        return typeof parameter !== 'undefined' && parameter !== null;
    };
    
    var getKeys = function(obj){
       var keys = [];
       for(var key in obj){
         if (obj.hasOwnProperty(key)) {
           keys.push(key);
         }
       }
       return keys;
    };
    
    var generic_update_callback = function(data, update_callback) {

        // This function could be called in one of three ways - to update a mesh object, to update a piece of geometry or to update a material
        var update_key = {
            object_type : data.object_type,
            object_id : data.object_id
        };
        var update_data = $.extend({}, data);

        Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers[data.object_type].create(data, update_callback);

        var object_update_callback = function(data, callback) {
            //console.log('Backend returned this after update :');
            //console.log(data);
        };

        Drupal.behaviors.cam3d_three_editor.update_remote(update_data, object_update_callback, JSON.stringify(update_key));
    };

    Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers.scene = {
        create : function(data, scene_create_callback){
            var children = data.children !== undefined ? data.children : [];
            var autoload_children = data.autoload_children;
            var autoloaded_objects = data.autoloaded_objects;
            
            var create_scene = function() {
            
                var scene = new THREE.Scene();
                scene.name = data.name;
                scene.parameters = data;
                scene.object_type = 'scene';

                if (children.length > 0) {

                    var objects_loaded = function(objects_loaded) {
                        $.each(objects_loaded, function(index, object_loaded) {
                            scene.add(object_loaded);
                        });
                        scene_create_callback(scene);
                    };

                    var scene_default_object = {};
                    var scene_objects_to_load = [];

                    $.each(children, function(index, child_object_index) {
                        if (is_defined(Drupal.behaviors.cam3d_three.objectcollection['id' + child_object_index])) {
                            scene.add(Drupal.behaviors.cam3d_three.objectcollection['id' + child_object_index]);
                        }
                        else {
                            var scene_object_to_load = $.extend({object_id:child_object_index}, scene_default_object);
                            scene_objects_to_load.push(scene_object_to_load);
                        }
                    });

                    Drupal.behaviors.cam3d_three.read_remote_collection(scene_objects_to_load, objects_loaded);
                }
                else {
                    scene_create_callback(scene);
                }
            };
            
            if (autoload_children) {
                
                var objects_created_counter = 0, total_objects_to_create = Object.keys(autoloaded_objects).length;
                
                var dummy_callback = function(object) {
                    console.log(object);
                    console.log('' + object.object_type + " <<");
                    objects_created_counter ++;
                    if (objects_created_counter == (total_objects_to_create - 1)) {
                        create_scene();
                    }
                };
                
                $.each(autoloaded_objects, function(index, autoloaded_object) {
                    console.log('' + autoloaded_object.object_type + " >>");
                    Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers[autoloaded_object.object_type].create(autoloaded_object, dummy_callback);
                });
            }
            else {
                create_scene();
            }

        },
        update : generic_update_callback
    };

    Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers.mesh = {
        create : function(data, mesh_create_callback) {

            var resolve_geometry = function(geometry_data, resolve_geometry_callback) {
                var geometry_data = $.extend({}, geometry_data);
                var geometry = geometry_data.geometry;
                var geometry_type = geometry.geometry_type;
                if (is_defined(Drupal.behaviors.cam3d_three.sgnodegeometrycorecontrollers[geometry_type])) {
                  Drupal.behaviors.cam3d_three.sgnodegeometrycorecontrollers[geometry_type].create(geometry, resolve_geometry_callback);
                }
                else {
                  // Time to negotiate to load in javascript for unknown controller, must be FULL object
                  Drupal.behaviors.cam3d_three.request_controller('create', geometry_data, resolve_geometry_callback);
                }
            };
            var resolve_material = function(material_data, resolve_material_callback) {
                var material_data = $.extend({}, material_data);
                var material = material_data.material;
                var material_type = material.material_type;
                if (is_defined(Drupal.behaviors.cam3d_three.sgnodematerialcorecontrollers[material_type])) {
                  Drupal.behaviors.cam3d_three.sgnodematerialcorecontrollers[material_type].create(material, resolve_material_callback);
                }
                else {
                  // Time to negotiate to load in javascript for unknown controller, must be FULL object
                  Drupal.behaviors.cam3d_three.request_controller('create', material_data, resolve_material_callback);
                }
            };


            // Core Mesh Object defined as having object_id, geometry_id and material_id
            // May also have the actual mesh and geometry objects themselves, but we will remove them
            if (is_defined(data.object_id) && is_defined(data.geometry_id) && is_defined(data.material_id)) {

                var object_data = $.extend({}, data);
                
                var geometry_id = object_data.geometry_id;
                var geometry = object_data.geometry;
                delete object_data.geometry;
                
                var material_id = object_data.material_id;
                var material = object_data.material;
                delete object_data.material;

                if (!is_defined(object_data.position)) object_data.position = {x : 0.0, y : 0.0, z : 0.0};
                if (!is_defined(object_data.rotation)) object_data.rotation = {x : 0.0, y : 0.0, z : 0.0};
                if (!is_defined(object_data.scale)) object_data.scale = {x : 1.0, y : 1.0, z : 1.0};
                if (!is_defined(object_data.name)) object_data.name = 'Mesh ' + ('id' + object_data.object_id);

                var geometry_complete = false, material_complete = false;
                var assets_gathered_test_callback = function() {
                    if (geometry_complete && material_complete) create_mesh();
                };
                var create_mesh = function() {

                  var geometry = Drupal.behaviors.cam3d_three.geometrycollection['id' + geometry_id];
                  var material = Drupal.behaviors.cam3d_three.materialcollection['id' + material_id];

                  var mesh = new THREE.Mesh( geometry, material );
                  
                  // Need to apply position, rotation and scale values
                  mesh.position.set(object_data.position.x, object_data.position.y, object_data.position.z);
                  mesh.rotation.set(object_data.rotation.x, object_data.rotation.y, object_data.rotation.z);
                  mesh.scale.set(object_data.scale.x, object_data.scale.y, object_data.scale.z);

                  mesh.parameters = object_data;
                  mesh.object_type = 'mesh';
                  mesh.name = object_data.name;

                  Drupal.behaviors.cam3d_three.objectcollection['id' + object_data.object_id] = mesh;
                  mesh_create_callback(mesh);
                };
                var mesh_object = {object_type:'mesh'};

                var mesh_geometry_object = $.extend({geometry_id:geometry_id}, mesh_object);
                var geometry_complete_callback = function(geometry) {
                    // Need to attach object_type : mesh metadata to the geometry in order for ui to be able to pass the geometry back to object update
                    geometry.object_type = 'mesh';
                    geometry_complete = true;
                    assets_gathered_test_callback();
                };

                var mesh_material_object = $.extend({material_id:material_id}, mesh_object);
                var material_complete_callback = function(material) {
                    // Need to attach object_type : mesh metadata to the material in order for ui to be able to pass the material back to object update
                    material.object_type = 'mesh';
                    material_complete = true;
                    assets_gathered_test_callback();
                };
                
                // If geometry has already been created, use that instead (this constructor is also used during object updates)
                if (is_defined(geometry.geometry_type)) {
                    resolve_geometry({object_type:'mesh', geometry:geometry}, geometry_complete_callback);
                }
                else if (is_defined(Drupal.behaviors.cam3d_three.geometrycollection['id' + geometry_id])) geometry_complete_callback(Drupal.behaviors.cam3d_three.geometrycollection['id' + geometry_id]);
                else Drupal.behaviors.cam3d_three.read_remote(mesh_geometry_object, geometry_complete_callback);
                
                // If material has already been created, use that instead (this constructor is also used during object updates)
                if (is_defined(material.material_type)) {
                    resolve_material({object_type:'mesh', material:material}, material_complete_callback);
                }
                else if (is_defined(Drupal.behaviors.cam3d_three.materialcollection['id' + material_id])) material_complete_callback(Drupal.behaviors.cam3d_three.materialcollection['id' + material_id]);
                else Drupal.behaviors.cam3d_three.read_remote(mesh_material_object, material_complete_callback);
            }
            else if (is_defined(data.geometry)) {
                resolve_geometry(data, mesh_create_callback);
            }
            else if (is_defined(data.material)) {
                resolve_material(data, mesh_create_callback);
            }
        },
        update : function(data, callback) {
            
            // This function is called to produce a piece of geometry and update the backend
            // First we update the data in the scene, then we trigger the update, and when the update comes back, update the mesh/geometry/material once again

            // This function could be called in one of three ways - to update a mesh object, to update a piece of geometry or to update a material
            var update_key = {
                object_type : 'mesh'
            };
            var update_data = {
                object_type : 'mesh'
            };

            // Use these properties to contruct a key
            // If its a mesh object update, should just have basic object properties
            if (is_defined(data.object_id)) {
                update_key.object_id = data.object_id;
                
                // @TODO We also need to rebuild the object based on the parameters
                update_data = $.extend(update_data, data);
                
                Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers[data.object_type].create(data, callback);
            }

            // If its a geometry update, should have a geometry type and id
            if (is_defined(data.geometry_type) && is_defined(data.geometry_id)) {

                update_key.geometry_type = data.geometry_type;
                update_key.geometry_id = data.geometry_id;

                update_data.geometry = data;
                if (update_data.geometry.object_type !== undefined) delete update_data.geometry.object_type;

                var set_mesh_object_type = function(geometry) {
                    geometry.object_type = 'mesh';
                    callback(geometry);
                };

                Drupal.behaviors.cam3d_three.sgnodegeometrycorecontrollers[data.geometry_type].create(data, set_mesh_object_type);
            }

            // If its a material update, should have a material type and id
            if (is_defined(data.material_type) && is_defined(data.material_id)) {
                update_key.material_type = data.material_type;
                update_key.material_id = data.material_id;

                update_data.material = $.extend({}, data);

                // Need to convert mapping objects to ids
                $.each(Drupal.behaviors.cam3d_three.getKeys(update_data.material), function(i, v) {
                    // If its an object with an fid, convert to just the fid
                    if (is_defined(update_data.material[v].fid)) update_data.material[v] = update_data.material[v].fid;
                });

                if (update_data.material.object_type !== undefined) delete update_data.material.object_type;

                var set_mesh_object_type = function(material) {
                    material.object_type = 'mesh';
                    callback(material);
                };

                Drupal.behaviors.cam3d_three.sgnodematerialcorecontrollers[data.material_type].create(data, set_mesh_object_type);
            }

            var object_update_callback = function(data, callback) {
                //console.log('Backend returned this after update :');
                //console.log(data);
            };

            // Finally, ping the remote server with the data, managing massive flux of data where events are multiple
            Drupal.behaviors.cam3d_three_editor.update_remote(update_data, object_update_callback, JSON.stringify(update_key));
        }
    };

    Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers.sprite = {
        create : function(data, callback) {
            // This controller could be called in two different ways
            // First as the stub sprite, will have an object_type, object_id, material_id etc
            if (is_defined(data.object_id) && is_defined(data.material_id)) {
                var object_id = data.object_id;
                var material_id = data.material_id;
                if (!is_defined(data.name)) data.name = 'Sprite ' + (data.object_id);

                var create_sprite = function(material) {
                    var sprite = new THREE.Sprite( material );
                    sprite.parameters = $.extend({}, data);
                    sprite.name = data.name;
                    sprite.object_type = 'sprite';
                    sprite.behaviors.cam3d_three.objectcollection['id' + data.object_id] = sprite;
                    callback(sprite);
                };
                Drupal.behaviors.cam3d_three.read_remote({ object_type : 'sprite', id : object_id, material_id : material_id}, create_sprite);
            }
            // The second as loading the sprite material, so it has object_type, material definition
            else if (is_defined(data.material)) {

                var material = data.material;
                var material_type = material.material_type;
                if (is_defined(Drupal.behaviors.cam3d_three.sgnodematerialcorecontrollers[material_type])) {
                  Drupal.behaviors.cam3d_three.sgnodematerialcorecontrollers[material_type].create(material, callback);
                }
                else {
                  // Time to negotiate to load in javascript for unknown controller
                  Drupal.behaviors.cam3d_three.request_controller('create', data, callback);
                }
            }
        },
        update : generic_update_callback
    };

    Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers.pointlight = {
        create : function(data, callback){
            
            if (!is_defined(data.color)) data.color = 0xffffff;
            if (!is_defined(data.intensity)) data.intensity = 1;
            if (!is_defined(data.distance)) data.distance = 0;
            if (!is_defined(data.name)) data.name = 'PointLight ' + ( data.object_id );
            if (!is_defined(data.position)) data.position = {x : 0.0, y : 0.0, z : 0.0};

            var light = new THREE.PointLight( data.color, data.intensity, data.distance );
            light.parameters = $.extend({}, data);
            light.name = data.name;
            light.position.set(data.position.x, data.position.y, data.position.z);
            light.object_type = 'pointlight';
            Drupal.behaviors.cam3d_three.objectcollection['id' + data.object_id] = light;
            callback(light);
        },
        update : generic_update_callback
    };

    Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers.spotlight = {
        create : function(data, callback){
            var color = data.color || 0xffffff;
            var intensity = data.intensity || 1;
            var distance = data.distance || 0;
            var angle = data.angle || Math.PI * 0.1;
            var exponent = data.exponent || 10;
            if (!is_defined(data.name)) data.name = 'SpotLight ' + ( data.object_id );

            var light = new THREE.SpotLight( color, intensity, distance, angle, exponent );
            light.parameters = $.extend({}, data);
            light.name = data.name;
            light.target.name = 'SpotLight ' + ( data.object_id ) + ' Target';
            light.object_type = 'spotlight';

            light.position.set( 0, 1, 0 ).multiplyScalar( 200 );
            Drupal.behaviors.cam3d_three.objectcollection['id' + data.object_id] = light;
            callback(light);
        },
        update : generic_update_callback
    };

    Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers.directionallight = {
        create : function(data, callback){
            var color = data.color || 0xffffff;
            var intensity = data.intensity || 1;
            if (!is_defined(data.name)) data.name = 'DirectionalLight ' + ( data.object_id ) + ' Target';

            var light = new THREE.DirectionalLight( color, intensity );
            light.parameters = $.extend({}, data);
            light.name = data.name;
            light.object_type = 'directionallight';

            light.position.set( 1, 1, 1 ).multiplyScalar( 200 );
            Drupal.behaviors.cam3d_three.objectcollection['id' + data.object_id] = light;
            callback(light);
        },
        update : generic_update_callback
    };

    Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers.hemispherelight = {
        create : function(data, callback){
            var skyColor = data.skycolor || 0x00aaff;
            var groundColor = data.groundcolor || 0xffaa00;
            var intensity = data.intensity || 1;
            if (!is_defined(data.name)) data.name = 'HemisphereLight ' + ( data.object_id );

            var light = new THREE.HemisphereLight( skyColor, groundColor, intensity );
            light.parameters = $.extend({}, data);
            light.name = data.name;
            light.object_type = 'hemispherelight';

            light.position.set( 1, 1, 1 ).multiplyScalar( 200 );
            Drupal.behaviors.cam3d_three.objectcollection['id' + data.object_id] = light;
            callback(light);
        },
        update : generic_update_callback
    };

    Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers.ambientlight = {
        create : function(data, callback){
            var color = data.color || 0x222222;
            if (!is_defined(data.name)) data.name = 'AmbientLight ' + ( data.object_id );

            var light = new THREE.AmbientLight( color );
            light.parameters = $.extend({}, data);
            light.name = data.name;
            light.object_type = 'ambientlight';
            Drupal.behaviors.cam3d_three.objectcollection['id' + data.object_id] = light;
            callback(light);
        },
        update : generic_update_callback
    };
    
    Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers.perspectivecamera = {
        create : function(data, callback){
            
            if (!is_defined(data.fov)) data.fov = 75.0;
            if (!is_defined(data.aspect)) data.aspect = 640.0 / 480.0;
            if (!is_defined(data.near)) data.near = 1.0;
            if (!is_defined(data.far)) data.far = 1000.0;

            if (!is_defined(data.name)) data.name = 'PerspectiveCamera ' + ( data.object_id );

            if (!is_defined(data.position)) data.position = {x : 0.0, y : 0.0, z : 0.0};
            if (!is_defined(data.rotation)) data.rotation = {x : 0.0, y : 0.0, z : 0.0};
            if (!is_defined(data.scale)) data.scale = {x : 1.0, y : 1.0, z : 1.0};

            var camera = new THREE.PerspectiveCamera( data.fov, data.aspect, data.near, data.far );
            camera.parameters = $.extend({}, data);
            camera.name = data.name;
            camera.position.set(data.position.x, data.position.y, data.position.z);
            camera.rotation.set(data.rotation.x, data.rotation.y, data.rotation.z);
            camera.scale.set(data.scale.x, data.scale.y, data.scale.z);
            
            camera.object_type = 'perspectivecamera';
            Drupal.behaviors.cam3d_three.objectcollection['id' + data.object_id] = camera;
            callback(camera);
        },
        update : generic_update_callback
    };
    
    Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers.orthographiccamera = {
        create : function(data, callback){
            
            if (!is_defined(data.left)) data.left = -75.0;
            if (!is_defined(data.right)) data.right = 75.0;
            if (!is_defined(data.top)) data.top = 75.0;
            if (!is_defined(data.bottom)) data.bottom = -75.0;
            if (!is_defined(data.near)) data.near = 1.0;
            if (!is_defined(data.far)) data.far = 1000.0;

            if (!is_defined(data.name)) data.name = 'OrthographicCamera ' + ( data.object_id );

            if (!is_defined(data.position)) data.position = {x : 0.0, y : 0.0, z : 0.0};
            if (!is_defined(data.rotation)) data.rotation = {x : 0.0, y : 0.0, z : 0.0};
            if (!is_defined(data.scale)) data.scale = {x : 1.0, y : 1.0, z : 1.0};

            var camera = new THREE.OrthographicCamera( data.left, data.right, data.top, data.bottom, data.near, data.far );
            camera.parameters = $.extend({}, data);
            camera.name = data.name;
            camera.position.set(data.position.x, data.position.y, data.position.z);
            camera.rotation.set(data.rotation.x, data.rotation.y, data.rotation.z);
            camera.scale.set(data.scale.x, data.scale.y, data.scale.z);
            camera.object_type = 'orthographiccamera';
            Drupal.behaviors.cam3d_three.objectcollection['id' + data.object_id] = camera;
            callback(camera);
        },
        update : generic_update_callback
    };

    $.each(Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers, function(key, controller){
        controller.prototype = genericControllerPrototype;
    });

}(jQuery));