(function ($) {

    Drupal.behaviors.cam3d_three_editor.base_material_ui_elements = function() {

        var default_ui_elements = {
            opacity : {
                label : new UI.Text( 'Opacity' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' ).setRange( 0, 1 )
            },
            transparent : {
                label : new UI.Text( 'Transparent' ).setWidth( '90px' ),
                element : new UI.Checkbox().setLeft( '100px' )
            },
            blending : {
                label : new UI.Text( 'Blending' ).setWidth( '90px' ),
                element : new UI.Select().setOptions( {
                    0 : 'No',
                    1 : 'Normal',
                    2 : 'Additive',
                    3 : 'Subtractive',
                    4 : 'Multiply',
                    5 : 'Custom'
                } ).setWidth( '150px' ).setColor( '#444' ).setFontSize( '12px' )
            },
            blendSrc : {
                label : new UI.Text( 'Blending Source' ).setWidth( '90px' ),
                element : new UI.Select().setOptions( {
                    200 : 'Zero',
                    201 : 'One',
                    202 : 'SrcColor',
                    203 : 'OneMinusSrcColor',
                    204 : 'SrcAlpha',
                    205 : 'OneMinusSrcAlpha',
                    206 : 'DstAlpha',
                    207 : 'OneMinusDstAlpha',
                    208 : 'DstColor',
                    209 : 'OneMinusDstColor',
                    210 : 'SrcAlphaSaturate'                        
                } ).setWidth( '150px' ).setColor( '#444' ).setFontSize( '12px' )
            },
            blendDst : {
                label : new UI.Text( 'Blending Destination' ).setWidth( '90px' ),
                element : new UI.Select().setOptions( {
                    200 : 'Zero',
                    201 : 'One',
                    202 : 'SrcColor',
                    203 : 'OneMinusSrcColor',
                    204 : 'SrcAlpha',
                    205 : 'OneMinusSrcAlpha',
                    206 : 'DstAlpha',
                    207 : 'OneMinusDstAlpha',
                    208 : 'DstColor',
                    209 : 'OneMinusDstColor',
                    210 : 'SrcAlphaSaturate'                        
                } ).setWidth( '150px' ).setColor( '#444' ).setFontSize( '12px' )
            },
            blendEquation : {
                label : new UI.Text( 'Blend Equation' ).setWidth( '90px' ),
                element : new UI.Select().setOptions( {
                   100 : 'Add',
                   101 : 'Subtract',
                   102 : 'ReverseSubtract'
                } ).setWidth( '150px' ).setColor( '#444' ).setFontSize( '12px' )
            },
            depthTest : {
                label : new UI.Text( 'Depth test' ).setWidth( '90px' ),
                element : new UI.Checkbox()
            },
            depthWrite : {
                label : new UI.Text( 'Depth write' ).setWidth( '90px' ),
                element : new UI.Checkbox()
            },
            polygonOffset : {
                label : new UI.Text( 'Polygon offset' ).setWidth( '90px' ),
                element : new UI.Checkbox()
            },
            polygonOffsetFactor : {
                label : new UI.Text( 'Polygon offset factor' ).setWidth( '90px' ),
                element : new UI.Integer().setWidth( '60px' ).setRange( 1, Infinity )
            },
            polygonOffsetUnits : {
                label : new UI.Text( 'Polygon offset units' ).setWidth( '90px' ),
                element : new UI.Integer().setWidth( '60px' ).setRange( 1, Infinity )
            },
            alphaTest : {
                label : new UI.Text( 'Alpha test' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' ).setRange( 0, 1 )
            },
            overdraw : {
                label : new UI.Text( 'Overdraw' ).setWidth( '90px' ),
                element : new UI.Checkbox()
            },
            visible : {
                label : new UI.Text( 'Visible' ).setWidth( '90px' ),
                element : new UI.Checkbox()
            },
            side : {
                label : new UI.Text( 'Side' ).setWidth( '90px' ),
                element : new UI.Select().setOptions( {
                   0 : 'FrontSide',
                   1 : 'BackSide',
                   2 : 'DoubleSide'
                } ).setWidth( '150px' ).setColor( '#444' ).setFontSize( '12px' )
            }
        };

        default_ui_elements = Drupal.behaviors.cam3d_three_editor.attach_typed_value_functions(default_ui_elements, ['blending', 'blendSrc', 'blendDst', 'blendEquation', 'polygonOffsetFactor', 'polygonOffsetUnits', 'side'], 'int');

        return default_ui_elements;
    };
    Drupal.behaviors.cam3d_three_editor.general_material_update = function(sidebar_material_signals, sidebar_material_object, ui_elements, parent_ui_update) {
        return function() {
            var material = sidebar_material_object.material;

            var material_update_data = {
                object_type : material.object_type,
                material_type : material.material_type,
                material_id : material.material_id
            };
            $.each(Drupal.behaviors.cam3d_three.getKeys(ui_elements), function(i, v) {
                var ui_value = ui_elements[v].element.getTypedValue();
                if (ui_value !== null) material_update_data[v] = ui_value;
            });

            var process_material_object = function(controller_generated_material) {
                sidebar_material_object.material.dispose();
                sidebar_material_object.material = controller_generated_material;
                sidebar_material_object.material.buffersNeedUpdate = true;
                //sidebar_material_signals.objectChanged.dispatch( sidebar_material_object );
                sidebar_material_signals.materialChanged.dispatch( material );
            };

            Drupal.behaviors.cam3d_three.sgnodematerialcorecontrollers[material.material_type].update(material_update_data, process_material_object);
            parent_ui_update();
        };
    };

    Sidebar.Material.linebasic = function ( sidebar_material_signals, sidebar_material_object, object_update ) {

        var material = sidebar_material_object.material;
        var ui_elements = $.extend({
            color : {
                label : new UI.Text( 'Color' ).setWidth( '90px' ),
                element : new UI.Color()
            },
            linewidth : {
                label : new UI.Text( 'Line width' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            },
            linecap : {
                label : new UI.Text( 'Line cap' ).setWidth( '90px' ),
                element : new UI.Select().setOptions( {
                   round : 'Round',
                   butt : 'Butt',
                   square : 'Square'
                } ).setWidth( '150px' ).setColor( '#444' ).setFontSize( '12px' )
            },
            linejoin : {
                label : new UI.Text( 'Line join' ).setWidth( '90px' ),
                element : new UI.Select().setOptions( {
                   round : 'Round',
                   butt : 'Butt',
                   square : 'Square'
                } ).setWidth( '150px' ).setColor( '#444' ).setFontSize( '12px' )
            },
            vertexColors : {
                label : new UI.Text( 'Vertex colors' ).setWidth( '90px' ),
                element : new UI.Select().setOptions( {
                   0 : 'NoColors',
                   1 : 'FaceColors',
                   2 : 'VertexColors'
                } ).setWidth( '150px' ).setColor( '#444' ).setFontSize( '12px' )
            },
            fog : {
                label : new UI.Text( 'Fog' ).setWidth( '90px' ),
                element : new UI.Checkbox()
            }
        }, Drupal.behaviors.cam3d_three_editor.base_material_ui_elements());

        ui_elements = Drupal.behaviors.cam3d_three_editor.attach_typed_value_functions(ui_elements, ['color'], 'hex_alias');
        ui_elements = Drupal.behaviors.cam3d_three_editor.attach_typed_value_functions(ui_elements, ['vertexColors'], 'int');

        var update = Drupal.behaviors.cam3d_three_editor.general_material_update(sidebar_material_signals, sidebar_material_object, ui_elements, object_update);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), material);
    };
    Sidebar.Material.linedashed = function ( sidebar_material_signals, sidebar_material_object, object_update ) {

        var material = sidebar_material_object.material;

        var ui_elements = $.extend({
            color : {
                label : new UI.Text( 'Color' ).setWidth( '90px' ),
                element : new UI.Color()
            },
            linewidth : {
                label : new UI.Text( 'Line width' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            },
            scale : {
                label : new UI.Text( 'Scale' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            },
            dashSize : {
                label : new UI.Text( 'Dash size' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            },
            vertexColors : {
                label : new UI.Text( 'Vertex colors' ).setWidth( '90px' ),
                element : new UI.Select().setOptions( {
                   0 : 'No Colors',
                   1 : 'FaceColors',
                   2 : 'VertexColors'
                } ).setWidth( '150px' ).setColor( '#444' ).setFontSize( '12px' )
            },
            fog : {
                label : new UI.Text( 'Fog' ).setWidth( '90px' ),
                element : new UI.Checkbox()
            }

        }, Drupal.behaviors.cam3d_three_editor.base_material_ui_elements());

        ui_elements = Drupal.behaviors.cam3d_three_editor.attach_typed_value_functions(ui_elements, ['vertexColors'], 'int');
        ui_elements = Drupal.behaviors.cam3d_three_editor.attach_typed_value_functions(ui_elements, ['color'], 'hex_alias');

        var update = Drupal.behaviors.cam3d_three_editor.general_material_update(sidebar_material_signals, sidebar_material_object, ui_elements, object_update);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), material);
    };
    Sidebar.Material.meshbasic = function ( sidebar_material_signals, sidebar_material_object, object_update ) {

        var material = sidebar_material_object.material;

        var ui_elements = $.extend({
            color : {
                label : new UI.Text( 'Color' ).setWidth( '90px' ),
                element : new UI.Color()
            },
            map : {
                label : new UI.Text( 'Map' ).setWidth( '90px' ),
                element : new UI.CAM3DTexture('Select').setColor( '#444' )
            },
            lightMap : {
                label : new UI.Text( 'Light Map' ).setWidth( '90px' ),
                element : new UI.CAM3DTexture('Select').setColor( '#444' )
            },
            specularMap : {
                label : new UI.Text( 'Specular Map' ).setWidth( '90px' ),
                element : new UI.CAM3DTexture('Select').setColor( '#444' )
            },
            /*envMap : {
                label : new UI.Text( 'Env Map' ).setWidth( '90px' ),
                element : new UI.CubeTexture().setColor( '#444' )
            },*/
            combine : {
                label : new UI.Text( 'Combine' ).setWidth( '90px' ),
                element : new UI.Select().setOptions( {
                   0 : 'Multiply',
                   1 : 'Mix',
                   2 : 'Add'
                } ).setWidth( '150px' ).setColor( '#444' ).setFontSize( '12px' )
            },
            reflectivity : {
                label : new UI.Text( 'Reflectivity' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            },
            refractionRatio : {
                label : new UI.Text( 'Refraction Ratio' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            },
            shading : {
                label : new UI.Text( 'Refraction Ratio' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            },
            wireframe : {
                label : new UI.Text( 'Wireframe' ).setWidth( '90px' ),
                element : new UI.Checkbox()
            },
            wireframeLinewidth : {
                label : new UI.Text( 'Wireframe line width' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            },
            vertexColors : {
                label : new UI.Text( 'Vertex colors' ).setWidth( '90px' ),
                element : new UI.Select().setOptions( {
                   0 : 'No Colors',
                   1 : 'FaceColors',
                   2 : 'VertexColors'
                } ).setWidth( '150px' ).setColor( '#444' ).setFontSize( '12px' )
            },
            skinning : {
                label : new UI.Text( 'Skinning' ).setWidth( '90px' ),
                element : new UI.Checkbox()
            },
            morphTargets : {
                label : new UI.Text( 'Morph Targets' ).setWidth( '90px' ),
                element : new UI.Checkbox()
            },
            fog : {
                label : new UI.Text( 'Fog' ).setWidth( '90px' ),
                element : new UI.Checkbox()
            }
        }, Drupal.behaviors.cam3d_three_editor.base_material_ui_elements());

        ui_elements = Drupal.behaviors.cam3d_three_editor.attach_typed_value_functions(ui_elements, ['color'], 'hex_alias');
        ui_elements = Drupal.behaviors.cam3d_three_editor.attach_typed_value_functions(ui_elements, ['vertexColors', 'combine'], 'int');

        var update = Drupal.behaviors.cam3d_three_editor.general_material_update(sidebar_material_signals, sidebar_material_object, ui_elements, object_update);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), material);
    };
    Sidebar.Material.meshdepth = function ( sidebar_material_signals, sidebar_material_object, object_update ) {

        var material = sidebar_material_object.material;

        var ui_elements = $.extend({
            wireframe : {
                label : new UI.Text( 'Wireframe' ).setWidth( '90px' ),
                element : new UI.Checkbox()
            },
            wireframeLinewidth : {
                label : new UI.Text( 'Wireframe line width' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            }
        }, Drupal.behaviors.cam3d_three_editor.base_material_ui_elements());

        var update = Drupal.behaviors.cam3d_three_editor.general_material_update(sidebar_material_signals, sidebar_material_object, ui_elements, object_update);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), material);
    };
    Sidebar.Material.meshface = function ( sidebar_material_signals, sidebar_material_object, object_update ) {

        var container = new UI.Panel();
        var material = sidebar_material_object.material;

        var ui_elements = $.extend({}, Drupal.behaviors.cam3d_three_editor.base_material_ui_elements());

        var update = Drupal.behaviors.cam3d_three_editor.general_material_update(sidebar_material_signals, sidebar_material_object, ui_elements, object_update);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), material);
    };
    Sidebar.Material.meshlambert = function ( sidebar_material_signals, sidebar_material_object, object_update ) {

        var material = sidebar_material_object.material;

        var ui_elements = $.extend({
            color : {
                label : new UI.Text( 'Color' ).setWidth( '90px' ),
                element : new UI.Color()
            },
            ambient : {
                label : new UI.Text( 'Ambient' ).setWidth( '90px' ),
                element : new UI.Color()
            },
            emissive : {
                label : new UI.Text( 'Emissive' ).setWidth( '90px' ),
                element : new UI.Color()
            },
            map : {
                label : new UI.Text( 'Map' ).setWidth( '90px' ),
                element : new UI.CAM3DTexture('Select').setColor( '#444' )
            },
            lightMap : {
                label : new UI.Text( 'Light Map' ).setWidth( '90px' ),
                element : new UI.CAM3DTexture('Select').setColor( '#444' )
            },
            specularMap : {
                label : new UI.Text( 'Specular Map' ).setWidth( '90px' ),
                element : new UI.CAM3DTexture('Select').setColor( '#444' )
            },
            /*envMap : {
                label : new UI.Text( 'Env Map' ).setWidth( '90px' ),
                element : new UI.CubeTexture().setColor( '#444' )
            },*/
            combine : {
                label : new UI.Text( 'Combine' ).setWidth( '90px' ),
                element : new UI.Select().setOptions( {
                   0 : 'Multiply',
                   1 : 'Mix',
                   2 : 'Add'
                } ).setWidth( '150px' ).setColor( '#444' ).setFontSize( '12px' )
            },
            reflectivity : {
                label : new UI.Text( 'Reflectivity' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            },
            refractionRatio : {
                label : new UI.Text( 'Refraction Ratio' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            },
            shading : {
                label : new UI.Text( 'Refraction Ratio' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            },
            wireframe : {
                label : new UI.Text( 'Wireframe' ).setWidth( '90px' ),
                element : new UI.Checkbox()
            },
            wireframeLinewidth : {
                label : new UI.Text( 'Wireframe line width' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            },
            vertexColors : {
                label : new UI.Text( 'Vertex colors' ).setWidth( '90px' ),
                element : new UI.Select().setOptions( {
                   0 : 'No Colors',
                   1 : 'FaceColors',
                   2 : 'VertexColors'
                } ).setWidth( '150px' ).setColor( '#444' ).setFontSize( '12px' )
            },
            skinning : {
                label : new UI.Text( 'Skinning' ).setWidth( '90px' ),
                element : new UI.Checkbox()
            },
            morphTargets : {
                label : new UI.Text( 'Morph Targets' ).setWidth( '90px' ),
                element : new UI.Checkbox()
            },
            morphNormals : {
                label : new UI.Text( 'Morph Normals' ).setWidth( '90px' ),
                element : new UI.Checkbox()
            },
            fog : {
                label : new UI.Text( 'Fog' ).setWidth( '90px' ),
                element : new UI.Checkbox()
            }
        }, Drupal.behaviors.cam3d_three_editor.base_material_ui_elements());

        //ui_elements = Drupal.behaviors.cam3d_three_editor.attach_typed_value_functions(ui_elements, ['envMap'], 'json');
        ui_elements = Drupal.behaviors.cam3d_three_editor.attach_typed_value_functions(ui_elements, ['color', 'ambient', 'emissive'], 'hex_alias');
        ui_elements = Drupal.behaviors.cam3d_three_editor.attach_typed_value_functions(ui_elements, ['vertexColors', 'combine'], 'int');

        var update = Drupal.behaviors.cam3d_three_editor.general_material_update(sidebar_material_signals, sidebar_material_object, ui_elements, object_update);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), material);
    };
    Sidebar.Material.meshnormal = function ( sidebar_material_signals, sidebar_material_object, object_update ) {

        var material = sidebar_material_object.material;

        var ui_elements = $.extend({
            shading : {
                label : new UI.Text( 'Refraction Ratio' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            },
            wireframe : {
                label : new UI.Text( 'Wireframe' ).setWidth( '90px' ),
                element : new UI.Checkbox()
            },
            wireframeLinewidth : {
                label : new UI.Text( 'Wireframe line width' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            }
        }, Drupal.behaviors.cam3d_three_editor.base_material_ui_elements());

        var update = Drupal.behaviors.cam3d_three_editor.general_material_update(sidebar_material_signals, sidebar_material_object, ui_elements, object_update);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), material);
    };
    Sidebar.Material.meshphong = function ( sidebar_material_signals, sidebar_material_object, object_update ) {

        var material = sidebar_material_object.material;
        var ui_elements = $.extend({
            color : {
                label : new UI.Text( 'Color' ).setWidth( '90px' ),
                element : new UI.Color()
            },
            ambient : {
                label : new UI.Text( 'Ambient' ).setWidth( '90px' ),
                element : new UI.Color()
            },
            emissive : {
                label : new UI.Text( 'Emissive' ).setWidth( '90px' ),
                element : new UI.Color()
            },
            specular : {
                label : new UI.Text( 'Specular' ).setWidth( '90px' ),
                element : new UI.Color()
            },
            shininess : {
                label : new UI.Text( 'Shininess' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            },
            map : {
                label : new UI.Text( 'Map' ).setWidth( '90px' ),
                element : new UI.CAM3DTexture('Select').setColor( '#444' )
            },
            lightMap : {
                label : new UI.Text( 'Light Map' ).setWidth( '90px' ),
                element : new UI.CAM3DTexture('Select').setColor( '#444' )
            },
            bumpMap : {
                label : new UI.Text( 'Bump Map' ).setWidth( '90px' ),
                element : new UI.CAM3DTexture('Select').setColor( '#444' )
            },
            bumpScale : {
                label : new UI.Text( 'Bump Map Scale' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            },
            normalMap : {
                label : new UI.Text( 'Normal Map' ).setWidth( '90px' ),
                element : new UI.CAM3DTexture('Select').setColor( '#444' )
            },
            normalScale : {
                label : new UI.Text( 'Normal Map Scale' ).setWidth( '90px' ),
                element : new UI.TextArea().setWidth( '150px' ).setHeight( '80px' )
            },
            specularMap : {
                label : new UI.Text( 'Specular Map' ).setWidth( '90px' ),
                element : new UI.CAM3DTexture('Select').setColor( '#444' )
            },
            /*envMap : {
                label : new UI.Text( 'Env Map' ).setWidth( '90px' ),
                element : new UI.CubeTexture().setColor( '#444' )
            },*/
            combine : {
                label : new UI.Text( 'Combine' ).setWidth( '90px' ),
                element : new UI.Select().setOptions( {
                   0 : 'Multiply',
                   1 : 'Mix',
                   2 : 'Add'
                } ).setWidth( '150px' ).setColor( '#444' ).setFontSize( '12px' )
            },
            reflectivity : {
                label : new UI.Text( 'Reflectivity' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            },
            refractionRatio : {
                label : new UI.Text( 'Refraction Ratio' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            },
            shading : {
                label : new UI.Text( 'Refraction Ratio' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            },
            wireframe : {
                label : new UI.Text( 'Wireframe' ).setWidth( '90px' ),
                element : new UI.Checkbox()
            },
            wireframeLinewidth : {
                label : new UI.Text( 'Wireframe line width' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            },
            vertexColors : {
                label : new UI.Text( 'Vertex colors' ).setWidth( '90px' ),
                element : new UI.Select().setOptions( {
                   0 : 'No Colors',
                   1 : 'FaceColors',
                   2 : 'VertexColors'
                } ).setWidth( '150px' ).setColor( '#444' ).setFontSize( '12px' )
            },
            skinning : {
                label : new UI.Text( 'Skinning' ).setWidth( '90px' ),
                element : new UI.Checkbox()
            },
            morphTargets : {
                label : new UI.Text( 'Morph Targets' ).setWidth( '90px' ),
                element : new UI.Checkbox()
            },
            morphNormals : {
                label : new UI.Text( 'Morph Normals' ).setWidth( '90px' ),
                element : new UI.Checkbox()
            },
            fog : {
                label : new UI.Text( 'Fog' ).setWidth( '90px' ),
                element : new UI.Checkbox()
            }
        }, Drupal.behaviors.cam3d_three_editor.base_material_ui_elements());

        ui_elements = Drupal.behaviors.cam3d_three_editor.attach_typed_value_functions(ui_elements, ['color', 'ambient', 'emissive', 'specular'], 'hex_alias');
        ui_elements = Drupal.behaviors.cam3d_three_editor.attach_typed_value_functions(ui_elements, ['combine', 'vertexColors'], 'int');
        ui_elements = Drupal.behaviors.cam3d_three_editor.attach_typed_value_functions(ui_elements, ['normalScale'], 'json');

        var update = Drupal.behaviors.cam3d_three_editor.general_material_update(sidebar_material_signals, sidebar_material_object, ui_elements, object_update);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), material);
    };
    Sidebar.Material.particlesystem = function ( sidebar_material_signals, sidebar_material_object, object_update ) {

        var material = sidebar_material_object.material;

        var ui_elements = $.extend({
            color : {
                label : new UI.Text( 'Color' ).setWidth( '90px' ),
                element : new UI.Color()
            },
            map : {
                label : new UI.Text( 'Map' ).setWidth( '90px' ),
                element : new UI.CAM3DTexture('Select').setColor( '#444' )
            },
            size : {
                label : new UI.Text( 'Size' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            },
            vertexColors : {
                label : new UI.Text( 'Vertex colors' ).setWidth( '90px' ),
                element : new UI.Select().setOptions( {
                   0 : 'NoColors',
                   1 : 'FaceColors',
                   2 : 'VertexColors'
                } ).setWidth( '150px' ).setColor( '#444' ).setFontSize( '12px' )
            },
            fog : {
                label : new UI.Text( 'Fog' ).setWidth( '90px' ),
                element : new UI.Checkbox()
            }
        }, Drupal.behaviors.cam3d_three_editor.base_material_ui_elements());

        ui_elements = Drupal.behaviors.cam3d_three_editor.attach_typed_value_functions(ui_elements, ['color'], 'hex_alias');
        ui_elements = Drupal.behaviors.cam3d_three_editor.attach_typed_value_functions(ui_elements, ['combine', 'vertexColors'], 'int');

        var update = Drupal.behaviors.cam3d_three_editor.general_material_update(sidebar_material_signals, sidebar_material_object, ui_elements, object_update);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), material);
    };
    Sidebar.Material.shader = function ( sidebar_material_signals, sidebar_material_object, object_update ) {

        var material = sidebar_material_object.material;

        var ui_elements = $.extend({}, Drupal.behaviors.cam3d_three_editor.base_material_ui_elements());

        var update = Drupal.behaviors.cam3d_three_editor.general_material_update(sidebar_material_signals, sidebar_material_object, ui_elements, object_update);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), material);
    };
    Sidebar.Material.spritecanvas = function ( sidebar_material_signals, sidebar_material_object, object_update ) {

        var material = sidebar_material_object.material;

        var ui_elements = $.extend({
            color : {
                label : new UI.Text( 'Color' ).setWidth( '90px' ),
                element : new UI.Color()
            },
            map : {
                label : new UI.Text( 'Map' ).setWidth( '90px' ),
                element : new UI.CAM3DTexture('Select').setColor( '#444' )
            }
        }, Drupal.behaviors.cam3d_three_editor.base_material_ui_elements());

        ui_elements = Drupal.behaviors.cam3d_three_editor.attach_typed_value_functions(ui_elements, ['color'], 'hex_alias');

        var update = Drupal.behaviors.cam3d_three_editor.general_material_update(sidebar_material_signals, sidebar_material_object, ui_elements, object_update);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), material);
    };
    Sidebar.Material.sprite = function ( sidebar_material_signals, sidebar_material_object, object_update ) {

        var material = sidebar_material_object.material;

        var ui_elements = $.extend({}, Drupal.behaviors.cam3d_three_editor.base_material_ui_elements());

        var update = Drupal.behaviors.cam3d_three_editor.general_material_update(sidebar_material_signals, sidebar_material_object, ui_elements, object_update);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), material);
    };

}(jQuery));