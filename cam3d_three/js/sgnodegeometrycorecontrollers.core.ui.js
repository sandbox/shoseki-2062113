(function ($) {

    Drupal.behaviors.cam3d_three_editor.base_geometry_ui_elements = function() {

        var default_ui_elements = {
            
        };

        return default_ui_elements;
    };
    Drupal.behaviors.cam3d_three_editor.general_geometry_update = function(sidebar_geometry_signals, sidebar_geometry_object, ui_elements) {
        return function() {
            var geometry = sidebar_geometry_object.geometry;

            var geometry_update_data = {
                object_type : geometry.object_type,
                geometry_type : geometry.geometry_type,
                geometry_id : geometry.geometry_id
            };
            $.each(Drupal.behaviors.cam3d_three.getKeys(ui_elements), function(i, v) {
                var ui_value = ui_elements[v].element.getTypedValue();
                if (ui_value !== null) geometry_update_data[v] = ui_value;
            });

            var process_geometry_object = function(controller_generated_geometry) {
                sidebar_geometry_object.geometry.dispose();
                sidebar_geometry_object.geometry = controller_generated_geometry;
                sidebar_geometry_object.geometry.buffersNeedUpdate = true;
                sidebar_geometry_object.geometry.computeBoundingSphere();
                sidebar_geometry_signals.objectChanged.dispatch( sidebar_geometry_object );
            };

            Drupal.behaviors.cam3d_three.sgnodegeometrycorecontrollers[geometry.geometry_type].update(geometry_update_data, process_geometry_object);
        };
    };
    Sidebar.Geometry.box = function ( sidebar_geometry_signals, sidebar_geometry_object ) {

        var geometry = sidebar_geometry_object.geometry;

        var ui_elements = $.extend({
            width : {
                label : new UI.Text( 'Width' ).setWidth( '90px' ),
                element : new UI.Number( geometry.parameters.width )
            },
            height : {
                label : new UI.Text( 'Height' ).setWidth( '90px' ),
                element : new UI.Number( geometry.parameters.height )
            },
            depth : {
                label : new UI.Text( 'Depth' ).setWidth( '90px' ),
                element : new UI.Number( geometry.parameters.depth )
            },
            widthSegments : {
                label : new UI.Text( 'Width segments' ).setWidth( '90px' ),
                element : new UI.Integer( geometry.parameters.widthSegments ).setRange( 1, Infinity )
            },
            heightSegments : {
                label : new UI.Text( 'Height segments' ).setWidth( '90px' ),
                element : new UI.Integer( geometry.parameters.heightSegments ).setRange( 1, Infinity )
            },
            depthSegments : {
                label : new UI.Text( 'Depth segments' ).setWidth( '90px' ),
                element : new UI.Integer( geometry.parameters.depthSegments ).setRange( 1 , Infinity )
            }
        }, Drupal.behaviors.cam3d_three_editor.base_geometry_ui_elements());
            
        var update = Drupal.behaviors.cam3d_three_editor.general_geometry_update(sidebar_geometry_signals, sidebar_geometry_object, ui_elements, geometry);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), geometry);
    };
    Sidebar.Geometry.circle = function ( sidebar_geometry_signals, sidebar_geometry_object ) {

        var geometry = sidebar_geometry_object.geometry;

        var ui_elements = $.extend({
            radius : {
                label : new UI.Text( 'Radius' ).setWidth( '90px' ),
                element : new UI.Number( geometry.parameters.radius )
            },
            segments : {
                label : new UI.Text( 'Segments' ).setWidth( '90px' ),
                element : new UI.Integer( geometry.parameters.segments ).setRange( 3, Infinity )
            },
            thetaStart : {
                label : new UI.Text( 'Theta start' ).setWidth( '90px' ),
                element : new UI.Number( geometry.parameters.thetaStart )
            },
            thetaLength : {
                label : new UI.Text( 'Theta length' ).setWidth( '90px' ),
                element : new UI.Number( geometry.parameters.thetaLength )
            }
        }, Drupal.behaviors.cam3d_three_editor.base_geometry_ui_elements());

        var update = Drupal.behaviors.cam3d_three_editor.general_geometry_update(sidebar_geometry_signals, sidebar_geometry_object, ui_elements, geometry);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), geometry);
    };
    Sidebar.Geometry.cylinder = function ( sidebar_geometry_signals, sidebar_geometry_object ) {

        var geometry = sidebar_geometry_object.geometry;

        var ui_elements = $.extend({
            radiusTop : {
                label : new UI.Text( 'Radius top' ).setWidth( '90px' ),
                element : new UI.Number( geometry.parameters.radiusTop )
            },
            radiusBottom : {
                label : new UI.Text( 'Radius bottom' ).setWidth( '90px' ),
                element : new UI.Number( geometry.parameters.radiusBottom )
            },
            height : {
                label : new UI.Text( 'Height' ).setWidth( '90px' ),
                element : new UI.Number( geometry.parameters.height )
            },
            radialSegments : {
                label : new UI.Text( 'Radial segments' ).setWidth( '90px' ),
                element : new UI.Integer( geometry.parameters.radialSegments ).setRange( 1, Infinity )
            },
            heightSegments : {
                label : new UI.Text( 'Height segments' ).setWidth( '90px' ),
                element : new UI.Integer( geometry.parameters.heightSegments ).setRange( 1, Infinity )
            },
            openEnded : {
                label : new UI.Text( 'Open ended' ).setWidth( '90px' ),
                element : new UI.Checkbox( geometry.parameters.openEnded )
            }
        }, Drupal.behaviors.cam3d_three_editor.base_geometry_ui_elements());

        var update = Drupal.behaviors.cam3d_three_editor.general_geometry_update(sidebar_geometry_signals, sidebar_geometry_object, ui_elements, geometry);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), geometry);
    };
    Sidebar.Geometry.icosahedron = function ( sidebar_geometry_signals, sidebar_geometry_object ) {

        var geometry = sidebar_geometry_object.geometry;

        var ui_elements = $.extend({
            radius : {
                label : new UI.Text( 'Radius' ).setWidth( '90px' ),
                element : new UI.Number( geometry.parameters.radius )
            },
            detail : {
                label : new UI.Text( 'Detail' ).setWidth( '90px' ),
                element : new UI.Integer( geometry.parameters.detail ).setRange( 0, Infinity )
            }
        }, Drupal.behaviors.cam3d_three_editor.base_geometry_ui_elements());

        var update = Drupal.behaviors.cam3d_three_editor.general_geometry_update(sidebar_geometry_signals, sidebar_geometry_object, ui_elements, geometry);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), geometry);
    };
    Sidebar.Geometry.plane = function ( sidebar_geometry_signals, sidebar_geometry_object ) {

        var geometry = sidebar_geometry_object.geometry;

        var ui_elements = $.extend({
            width : {
                label : new UI.Text( 'Width' ).setWidth( '90px' ),
                element : new UI.Number( geometry.parameters.width )
            },
            height : {
                label : new UI.Text( 'Height' ).setWidth( '90px' ),
                element : new UI.Number( geometry.parameters.height )
            },
            widthSegments : {
                label : new UI.Text( 'Width segments' ).setWidth( '90px' ),
                element : new UI.Integer( geometry.parameters.widthSegments ).setRange( 1, Infinity )
            },
            heightSegments : {
                label : new UI.Text( 'Height segments' ).setWidth( '90px' ),
                element : new UI.Integer( geometry.parameters.heightSegments ).setRange( 1, Infinity )
            }
        }, Drupal.behaviors.cam3d_three_editor.base_geometry_ui_elements());

        var update = Drupal.behaviors.cam3d_three_editor.general_geometry_update(sidebar_geometry_signals, sidebar_geometry_object, ui_elements, geometry);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), geometry);
    };
    Sidebar.Geometry.sphere = function ( sidebar_geometry_signals, sidebar_geometry_object ) {

        var geometry = sidebar_geometry_object.geometry;

        var ui_elements = $.extend({
            radius : {
                label : new UI.Text( 'Radius' ).setWidth( '90px' ),
                element : new UI.Number( geometry.parameters.radius )
            },
            widthSegments : {
                label : new UI.Text( 'Width segments' ).setWidth( '90px' ),
                element : new UI.Integer( geometry.parameters.widthSegments ).setRange( 1, Infinity )
            },
            heightSegments : {
                label : new UI.Text( 'Height segments' ).setWidth( '90px' ),
                element : new UI.Integer( geometry.parameters.heightSegments ).setRange( 1, Infinity )
            },
            phiStart : {
                label : new UI.Text( 'Phi start' ).setWidth( '90px' ),
                element : new UI.Number( geometry.parameters.phiStart )
            },
            phiLength : {
                label : new UI.Text( 'Phi length' ).setWidth( '90px' ),
                element : new UI.Number( geometry.parameters.phiLength )
            },
            thetaStart : {
                label : new UI.Text( 'Theta start' ).setWidth( '90px' ),
                element : new UI.Number( geometry.parameters.thetaStart )
            },
            thetaLength : {
                label : new UI.Text( 'Theta length' ).setWidth( '90px' ),
                element : new UI.Number( geometry.parameters.thetaLength )
            }
        }, Drupal.behaviors.cam3d_three_editor.base_geometry_ui_elements());

        var update = Drupal.behaviors.cam3d_three_editor.general_geometry_update(sidebar_geometry_signals, sidebar_geometry_object, ui_elements, geometry);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), geometry);
    };
    Sidebar.Geometry.torus = function ( sidebar_geometry_signals, sidebar_geometry_object ) {

        var geometry = sidebar_geometry_object.geometry;

        var ui_elements = $.extend({
            radius : {
                label : new UI.Text( 'Radius' ).setWidth( '90px' ),
                element : new UI.Number( geometry.parameters.radius )
            },
            tube : {
                label : new UI.Text( 'Tube' ).setWidth( '90px' ),
                element : new UI.Number( geometry.parameters.tube )
            },
            radialSegments : {
                label : new UI.Text( 'Radial segments' ).setWidth( '90px' ),
                element : new UI.Integer( geometry.parameters.radialSegments ).setRange( 1, Infinity )
            },
            tubularSegments : {
                label : new UI.Text( 'Tubular segments' ).setWidth( '90px' ),
                element : new UI.Integer( geometry.parameters.tubularSegments ).setRange( 1, Infinity )
            },
            arc : {
                label : new UI.Text( 'Arc' ).setWidth( '90px' ),
                element : new UI.Number( geometry.parameters.arc )
            }
        }, Drupal.behaviors.cam3d_three_editor.base_geometry_ui_elements());

        var update = Drupal.behaviors.cam3d_three_editor.general_geometry_update(sidebar_geometry_signals, sidebar_geometry_object, ui_elements, geometry);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), geometry);
    };
    Sidebar.Geometry.torusknot = function ( sidebar_geometry_signals, sidebar_geometry_object ) {

        var geometry = sidebar_geometry_object.geometry;

        var ui_elements = $.extend({
            radius : {
                label : new UI.Text( 'Radius' ).setWidth( '90px' ),
                element : new UI.Number( geometry.parameters.radius )
            },
            tube : {
                label :new UI.Text( 'Tube' ).setWidth( '90px' ),
                element : new UI.Number( geometry.parameters.tube )
            },
            radialSegments : {
                label : new UI.Text( 'Radial segments' ).setWidth( '90px' ),
                element : new UI.Integer( geometry.parameters.radialSegments ).setRange( 1, Infinity )
            },
            tubularSegments : {
                label : new UI.Text( 'Tubular segments' ).setWidth( '90px' ),
                element : new UI.Integer( geometry.parameters.tubularSegments ).setRange( 1, Infinity )
            },
            p : {
                label : new UI.Text( 'P' ).setWidth( '90px' ),
                element : new UI.Number( geometry.parameters.p )
            },
            q : {
                label : new UI.Text( 'Q' ).setWidth( '90px' ),
                element : new UI.Number( geometry.parameters.q )
            },
            heightScale : {
                label : new UI.Text( 'Height scale' ).setWidth( '90px' ),
                element : new UI.Number( geometry.parameters.heightScale )
            }
        }, Drupal.behaviors.cam3d_three_editor.base_geometry_ui_elements());

        var update = Drupal.behaviors.cam3d_three_editor.general_geometry_update(sidebar_geometry_signals, sidebar_geometry_object, ui_elements, geometry);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), geometry);
    };
    Sidebar.Geometry.text = function ( sidebar_geometry_signals, sidebar_geometry_object ) {

        var geometry = sidebar_geometry_object.geometry;

        var ui_elements = $.extend({
            text : {
                label : new UI.Text( 'Text' ).setWidth( '90px' ),
                element : new UI.TextArea().setWidth( '150px' ).setHeight( '80px' )
            },
            size : {
                label : new UI.Text( 'Size' ).setWidth( '90px' ),
                element : new UI.Number()
            },
            height : {
                label : new UI.Text( 'Height' ).setWidth( '90px' ),
                element : new UI.Number()
            },
            curveSegments : {
                label : new UI.Text( 'Curve segments' ).setWidth( '90px' ),
                element : new UI.Integer().setRange( 1, Infinity )
            },
            font : {
                label : new UI.Text( 'Font' ).setWidth( '90px' ),
                element : new UI.Input().setWidth( '150px' ).setColor( '#444' ).setFontSize( '12px' )
            },
            weight : {
                label : new UI.Text( 'Weight' ).setWidth( '90px' ),
                element : new UI.Input().setWidth( '150px' ).setColor( '#444' ).setFontSize( '12px' )
            },
            style : {
                label : new UI.Text( 'Style' ).setWidth( '90px' ),
                element : new UI.Input().setWidth( '150px' ).setColor( '#444' ).setFontSize( '12px' )
            },
            bevelEnabled : {
                label : new UI.Text( 'Bevel Enabled' ).setWidth( '90px' ),
                element : new UI.Checkbox().setLeft( '100px' )
            },
            bevelThickness : {
                label : new UI.Text( 'Bevel Thickness' ).setWidth( '90px' ),
                element : new UI.Number()
            },
            bevelSize : {
                label : new UI.Text( 'Bevel Thickness' ).setWidth( '90px' ),
                element : new UI.Number()
            }
        }, Drupal.behaviors.cam3d_three_editor.base_geometry_ui_elements());

        var update = Drupal.behaviors.cam3d_three_editor.general_geometry_update(sidebar_geometry_signals, sidebar_geometry_object, ui_elements, geometry);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), geometry);
    };
    
}(jQuery));

