
(function ($) {

    // Create default object if not defined
    Drupal.behaviors.cam3d_three_editor = Drupal.behaviors.cam3d_three_editor || {};

    Drupal.behaviors.cam3d_three_editor.general_object_update_timers = {};

    var is_defined = function(parameter) {
        return typeof parameter !== 'undefined' && parameter !== null;
    };

    Drupal.behaviors.cam3d_three_editor.general_object_update = function(editor, sidebar_object_signals, sidebar_object, ui_elements, parent_ui_update) {
      return function() {

        // If timeout already set, clear it, prevents callback
        if (is_defined(Drupal.behaviors.cam3d_three_editor.general_object_update_timers['id' + sidebar_object.object_id])) {
            clearTimeout(Drupal.behaviors.cam3d_three_editor.general_object_update_timers['id' + sidebar_object.object_id]);
            delete Drupal.behaviors.cam3d_three_editor.general_object_update_timers['id' + sidebar_object.object_id];
        }

        // Take original data and only override it with what is 
        var object_update_data = sidebar_object.parameters;

        $.each(Drupal.behaviors.cam3d_three.getKeys(ui_elements), function(i, v) {
          var ui_value = ui_elements[v].element.getTypedValue();
          if (ui_value !== null) object_update_data[v] = ui_value;
        });

        var process_object_function = function(controller_generated_object) {
          if (controller_generated_object instanceof THREE.Scene) editor.setScene(controller_generated_object);
          else { 
            editor.removeObject(sidebar_object, true);
            editor.addObject(controller_generated_object);
            editor.select(controller_generated_object);
          }
          sidebar_object_signals.objectChanged.dispatch( controller_generated_object );
        };

        var timeoutCallback = function() {
            Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers[sidebar_object.object_type].update(object_update_data, process_object_function);
            parent_ui_update();
        };

        Drupal.behaviors.cam3d_three_editor.general_object_update_timers['id' + sidebar_object.object_id] = setTimeout(timeoutCallback, 500);
      };
    };
    Drupal.behaviors.cam3d_three_editor.base_object_ui_elements = function() {

        // Populate with base ui elements
        var default_ui_elements = {
            object_id : {
                label : new UI.Text( 'Object ID' ).setWidth( '90px' ),
                element : new UI.Input().setWidth( '150px' ).setColor( '#444' ).setFontSize( '12px' ).setDisabled( true )
            },
            name : {
                label : new UI.Text( 'Name' ).setWidth( '90px' ),
                element : new UI.Input().setWidth( '150px' ).setColor( '#444' ).setFontSize( '12px' )
            },
            position : {
                label : new UI.Text( 'Position' ).setWidth( '90px' ),
                element : new UI.TextArea().setWidth( '150px' ).setHeight( '80px' )
            }
        };

        default_ui_elements = Drupal.behaviors.cam3d_three_editor.attach_typed_value_functions(default_ui_elements, ['object_id'], 'int');
        default_ui_elements = Drupal.behaviors.cam3d_three_editor.attach_typed_value_functions(default_ui_elements, ['name'], 'string');
        default_ui_elements = Drupal.behaviors.cam3d_three_editor.attach_typed_value_functions(default_ui_elements, ['position'], 'vector3');

        return default_ui_elements;
    };
    
    Sidebar.Object3D.scene = function ( editor, sidebar_object_signals, sidebar_object, parent_update ) {
        
        var ui_elements = $.extend(Drupal.behaviors.cam3d_three_editor.base_object_ui_elements(), {});
        var update = Drupal.behaviors.cam3d_three_editor.general_object_update(editor, sidebar_object_signals, sidebar_object, ui_elements, parent_update);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), sidebar_object);
    };
    Sidebar.Object3D.mesh = function ( editor, sidebar_object_signals, sidebar_object, parent_update ) {

        var ui_elements = $.extend(Drupal.behaviors.cam3d_three_editor.base_object_ui_elements(), {
            rotation : {
                label : new UI.Text( 'Rotation' ).setWidth( '90px' ),
                element : new UI.TextArea().setWidth( '150px' ).setHeight( '80px' )
            },
            scale : {
                label : new UI.Text( 'Scale' ).setWidth( '90px' ),
                element : new UI.TextArea().setWidth( '150px' ).setHeight( '80px' )
            },
            geometry_id : {
                label : new UI.Text( 'Geometry ID' ).setWidth( '90px' ),
                element : new UI.Input().setWidth( '150px' ).setColor( '#444' ).setFontSize( '12px' ).setDisabled( true )
            },
            material_id : {
                label : new UI.Text( 'Material ID' ).setWidth( '90px' ),
                element : new UI.Input().setWidth( '150px' ).setColor( '#444' ).setFontSize( '12px' ).setDisabled( true )
            }
        });

        ui_elements = Drupal.behaviors.cam3d_three_editor.attach_typed_value_functions(ui_elements, ['geometry_id', 'material_id'], 'int');
        ui_elements = Drupal.behaviors.cam3d_three_editor.attach_typed_value_functions(ui_elements, ['rotation', 'scale'], 'vector3');

        var update = Drupal.behaviors.cam3d_three_editor.general_object_update(editor, sidebar_object_signals, sidebar_object, ui_elements, parent_update);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), sidebar_object);
    };
    Sidebar.Object3D.sprite = function ( editor, sidebar_object_signals, sidebar_object, parent_update ) {

        var ui_elements = $.extend(Drupal.behaviors.cam3d_three_editor.base_object_ui_elements(), {});
        var update = Drupal.behaviors.cam3d_three_editor.general_object_update(editor, sidebar_object_signals, sidebar_object, ui_elements, parent_update);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), sidebar_object);
    };
    Sidebar.Object3D.pointlight = function ( editor, sidebar_object_signals, sidebar_object, parent_update ) {

        var ui_elements = $.extend(Drupal.behaviors.cam3d_three_editor.base_object_ui_elements(), {
            color : {
                label : new UI.Text( 'Color' ).setWidth( '90px' ),
                element : new UI.Color()
            },
            intensity : {
                label : new UI.Text( 'Intensity' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            },
            distance : {
                label : new UI.Text( 'Distance' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            }
        });

        ui_elements = Drupal.behaviors.cam3d_three_editor.attach_typed_value_functions(ui_elements, ['color'], 'hex_alias');

        var update = Drupal.behaviors.cam3d_three_editor.general_object_update(editor, sidebar_object_signals, sidebar_object, ui_elements, parent_update);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), sidebar_object);
    };
    Sidebar.Object3D.spotlight = function ( editor, sidebar_object_signals, sidebar_object, parent_update ) {

        var ui_elements = $.extend(Drupal.behaviors.cam3d_three_editor.base_object_ui_elements(), {});
        var update = Drupal.behaviors.cam3d_three_editor.general_object_update(editor, sidebar_object_signals, sidebar_object, ui_elements, parent_update);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), sidebar_object);
    };
    Sidebar.Object3D.directionallight = function ( editor, sidebar_object_signals, sidebar_object, parent_update ) {

        var ui_elements = $.extend(Drupal.behaviors.cam3d_three_editor.base_object_ui_elements(), {});
        var update = Drupal.behaviors.cam3d_three_editor.general_object_update(editor, sidebar_object_signals, sidebar_object, ui_elements, parent_update);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), sidebar_object);
    };
    Sidebar.Object3D.perspectivecamera = function ( editor, sidebar_object_signals, sidebar_object, parent_update ) {

        var ui_elements = $.extend(Drupal.behaviors.cam3d_three_editor.base_object_ui_elements(), {
            rotation : {
                label : new UI.Text( 'Rotation' ).setWidth( '90px' ),
                element : new UI.TextArea().setWidth( '150px' ).setHeight( '80px' )
            },
            scale : {
                label : new UI.Text( 'Scale' ).setWidth( '90px' ),
                element : new UI.TextArea().setWidth( '150px' ).setHeight( '80px' )
            },
            fov : {
                label : new UI.Text( 'Field of Vision' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            },
            aspect : {
                label : new UI.Text( 'Aspect Ratio' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            },
            near : {
                label : new UI.Text( 'Near Plane' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            },
            far : {
                label : new UI.Text( 'Far Plane' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            }
        });
        
        ui_elements = Drupal.behaviors.cam3d_three_editor.attach_typed_value_functions(ui_elements, ['rotation', 'scale'], 'vector3');

        var update = Drupal.behaviors.cam3d_three_editor.general_object_update(editor, sidebar_object_signals, sidebar_object, ui_elements, parent_update);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), sidebar_object);
    };
    Sidebar.Object3D.orthographiccamera = function ( editor, sidebar_object_signals, sidebar_object, parent_update ) {

        var ui_elements = $.extend(Drupal.behaviors.cam3d_three_editor.base_object_ui_elements(), {
            rotation : {
                label : new UI.Text( 'Rotation' ).setWidth( '90px' ),
                element : new UI.TextArea().setWidth( '150px' ).setHeight( '80px' )
            },
            scale : {
                label : new UI.Text( 'Scale' ).setWidth( '90px' ),
                element : new UI.TextArea().setWidth( '150px' ).setHeight( '80px' )
            },
            left : {
                label : new UI.Text( 'Left Plane' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            },
            right : {
                label : new UI.Text( 'Right Plane' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            },
            top : {
                label : new UI.Text( 'Top Plane' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            },
            bottom : {
                label : new UI.Text( 'Bottom Plane' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            },
            near : {
                label : new UI.Text( 'Near Plane' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            },
            far : {
                label : new UI.Text( 'Far Plane' ).setWidth( '90px' ),
                element : new UI.Number().setWidth( '60px' )
            }
        });
        
        ui_elements = Drupal.behaviors.cam3d_three_editor.attach_typed_value_functions(ui_elements, ['rotation', 'scale'], 'vector3');

        var update = Drupal.behaviors.cam3d_three_editor.general_object_update(editor, sidebar_object_signals, sidebar_object, ui_elements, parent_update);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), sidebar_object);
    };

}(jQuery));
