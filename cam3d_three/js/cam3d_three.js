
THREE.Object3D.prototype.replace = function ( object ) {

    if ( arguments.length > 1 ) {

      for ( var i = 0; i < arguments.length; i++ ) {

        this.replace( arguments[ i ] );
      }
    };

    var object_id = object.parameters.object_id;
    var indices = [];

    for ( var i = 0; i < this.children.length; i++ ) {
      if (this.children[i].parameters.object_id == object_id) {
        this.children[i] = object;
      }
    }
};

// Using the closure to map jQuery to $.
(function ($) {
    
    // Store our function as a property of Drupal.behaviors.
    Drupal.behaviors.cam3d_three = {
        
      loaded_controller_uris : [],

      // Negotiated threejs controllers
      sgnodeobjectcontrollers : {},
      
      scenes : {},
      
      players : [],
      
      attach: function (context, settings) {

        if (! (Drupal.behaviors.cam3d_three.is_defined(settings.cam3d_three))) return;
        
        this.base_root = Drupal.settings.cam3d_three.base_root;
        this.api_root = this.base_root + '/' + Drupal.settings.cam3d_three.api_root;

        this.read_url = this.api_root + 'sgnodeobject/read';
        this.controllers_url = this.api_root + 'controllers';

        // If there are no scenes to be loaded, don't worry about it
        if (! Drupal.behaviors.cam3d_three.is_defined(settings.cam3d_three.instances) ) return;
            
        var instances = settings.cam3d_three.instances;
        var scenes_to_be_loaded_objects = [];

        $.each(instances, function(scene_div_selector_class, instance_settings) {
            var scene_object = {object_id : instance_settings.scene_id};
            scenes_to_be_loaded_objects.push(scene_object);
        });
        
        var process_scenes_callback = function(scenes) {
            $.each(scenes, function(i, scene) {
                Drupal.behaviors.cam3d_three.scenes['i' + scene.parameters.object_id] = scene;
            });
            
            $.each(instances, function(scene_div_selector_class, instance_settings) {

                var $scene_divs = $('.' + scene_div_selector_class);

                $.each($scene_divs, function(i, scene_div) {

                  var $scene_div = $(scene_div);
                  $scene_div.css({'position' : 'relative'});

                  $scene_div.once('cam3d-three-processed', function() {

                    $scene_div.empty();
                    $scene_div.width(instance_settings.width);
                    $scene_div.height(instance_settings.height);
                    
                    var player;
                    if (instance_settings.enable_vr) {
                        player = new Drupal.behaviors.cam3d_three.VRPlayer();
                    }
                    else {
                        player = new Drupal.behaviors.cam3d_three.Player();
                    }

                    player.load(Drupal.behaviors.cam3d_three.scenes['i' + instance_settings.scene_id], $scene_div, instance_settings.autosize, instance_settings.aspect_ratio);

                    Drupal.behaviors.cam3d_three.players.push(player);
                    player.render();
                    player.play();
                  });
                });
            });
        };
        Drupal.behaviors.cam3d_three.read_remote_collection(scenes_to_be_loaded_objects, process_scenes_callback);
      }
    };

    Drupal.behaviors.cam3d_three.Player = function () {

        var camera, scene, renderer;

        var scripts = {};

        this.dom = undefined;
        this.$scene_container_div = undefined;

        this.width = 640;
        this.height = 480;
        this.aspectRatio = 1;

        this.setAutosize = function(aspectRatio) {
            console.log('setAutosize');
        };

        this.load = function ( scene_loaded, $scene_div, autoresize, aspectRatio ) {

            this.$scene_container_div = $scene_div;

            if (autoresize) {
                this.aspectRatio = aspectRatio;
                var player_reference = this;

                $(window).bind('resize', function(){
                    var $parent = player_reference.$scene_container_div.parent();
                    var containerWidth = $parent.width(), containerHeight = containerWidth / player_reference.aspectRatio;
                    player_reference.setSize(containerWidth, containerHeight);
                });
                
                var $parent = this.$scene_container_div.parent();
                var containerWidth = $parent.width(), containerHeight = containerWidth / this.aspectRatio;
                this.width = containerWidth;
                this.height = containerHeight;
            }
            else {
                this.width = this.$scene_container_div.width();
                this.height = this.$scene_container_div.height();
            }

            renderer = new THREE.WebGLRenderer( { antialias: true } );
            renderer.setSize(this.width, this.height);

            scene = scene_loaded;

            scripts = {
                keydown: [],
                keyup: [],
                mousedown: [],
                mouseup: [],
                mousemove: [],
                update: []
            };

            var scene_scripts = [];
            scene.traverse(function (child) {
                if (child.parameters.scripts !== undefined) {
                    scene_scripts[child.uuid] = {
                        object : child,
                        scripts : child.parameters.scripts
                    };
                }
            });

            for ( var uuid in scene_scripts ) {

                var object = scene_scripts[ uuid ].object;
                var sources = scene_scripts[ uuid ].scripts;

                for ( var i = 0; i < sources.length; i ++ ) {

                    var script = sources[ i ];
                    var events = (
                        new Function(
                            'player',
                            'scene',
                            'keydown',
                            'keyup',
                            'mousedown',
                            'mouseup',
                            'mousemove',
                            'update',
                            script.source + '\nreturn { keydown: keydown, keyup: keyup, mousedown: mousedown, mouseup: mouseup, mousemove: mousemove, update: update };'
                        )
                        .bind( object ))( this, scene );

                    for ( var name in events ) {

                        if ( events[ name ] === undefined ) continue;

                        if ( scripts[ name ] === undefined ) {
                            console.warn( 'APP.Player: event type not supported (', name, ')' );
                            continue;
                        }

                        scripts[ name ].push( events[ name ].bind( object ) );
                    }
                }
            }

            this.dom = renderer.domElement;
            this.$scene_container_div.append(this.dom);

            if (camera === undefined) camera = new THREE.PerspectiveCamera( 75, this.width / this.height, 0.1, 1000 );
        };

        this.setCamera = function ( value ) {
            camera = value;
            camera.aspect = this.width / this.height;
            camera.updateProjectionMatrix();
        };

        this.setSize = function ( width, height ) {
            this.width = width;
            this.height = height;
            camera.aspect = this.width / this.height;
            camera.updateProjectionMatrix();
            renderer.setSize( width, height );
        };

        var dispatch = function ( array, event ) {
            for ( var i = 0, l = array.length; i < l; i ++ ) {
                array[ i ]( event );
            }
        };

        var request;

        var animate = function ( time ) {
            request = requestAnimationFrame( animate );
            if (scripts.update !== undefined) dispatch( scripts.update, { time: time } );
            renderer.render( scene, camera );
        };

        this.play = function () {
            this.$scene_container_div.bind( "keydown", onDocumentKeyDown );
            this.$scene_container_div.bind( "keyup", onDocumentKeyUp );
            this.$scene_container_div.bind( "mousedown", onDocumentMouseDown );
            this.$scene_container_div.bind( "mouseup", onDocumentMouseUp );
            this.$scene_container_div.bind( "mousemove", onDocumentMouseMove );
            request = requestAnimationFrame( animate );
        };

        this.stop = function () {
            this.$scene_container_div.unbind( 'keydown', onDocumentKeyDown );
            this.$scene_container_div.unbind( 'keyup', onDocumentKeyUp );
            this.$scene_container_div.unbind( 'mousedown', onDocumentMouseDown );
            this.$scene_container_div.unbind( 'mouseup', onDocumentMouseUp );
            this.$scene_container_div.unbind( 'mousemove', onDocumentMouseMove );
            cancelAnimationFrame( request );
        };
        
        this.render = function() {
            renderer.render( scene, camera );
        };

        var onDocumentKeyDown = function ( event ) {
            dispatch( scripts.keydown, event );
        };

        var onDocumentKeyUp = function ( event ) {
            dispatch( scripts.keyup, event );
        };

        var onDocumentMouseDown = function ( event ) {
            dispatch( scripts.mousedown, event );
        };

        var onDocumentMouseUp = function ( event ) {
            dispatch( scripts.mouseup, event );
        };

        var onDocumentMouseMove = function ( event ) {
            dispatch( scripts.mousemove, event );
        };
    };
    Drupal.behaviors.cam3d_three.VRPlayer = function () {

        var camera, scene, renderer, vrEffect, vrControls
        var vrAvailable = false, vrMode = false;
        var $vrButton;

        var scripts = {};

        this.dom = undefined;
        this.$scene_container_div = undefined;

        this.width = 640;
        this.height = 480;

        this.load = function ( scene_loaded, $scene_div ) {

            this.$scene_container_div = $scene_div;

            this.width = this.$scene_container_div.width();
            this.height = this.$scene_container_div.height();

            renderer = new THREE.WebGLRenderer( { antialias: true } );
            renderer.setSize(this.width, this.height);
            
            var VREffectLoaded = function (error) {
              if (error) {
                console.log(error);
                $vrButton.hide();
              }
            };
            
            vrEffect = new THREE.VREffect(renderer, VREffectLoaded);

            scene = scene_loaded;

            scripts = {
                keydown: [],
                keyup: [],
                mousedown: [],
                mouseup: [],
                mousemove: [],
                update: []
            };

            var scene_scripts = [];
            scene.traverse(function (child) {
                if (child.parameters.scripts !== undefined) {
                    scene_scripts[child.uuid] = {
                        object : child,
                        scripts : child.parameters.scripts
                    };
                }
            });

            for ( var uuid in scene_scripts ) {

                var object = scene_scripts[ uuid ].object;
                var sources = scene_scripts[ uuid ].scripts;

                for ( var i = 0; i < sources.length; i ++ ) {

                    var script = sources[ i ];
                    var events = (
                        new Function(
                            'player',
                            'scene',
                            'keydown',
                            'keyup',
                            'mousedown',
                            'mouseup',
                            'mousemove',
                            'update',
                            script.source + '\nreturn { keydown: keydown, keyup: keyup, mousedown: mousedown, mouseup: mouseup, mousemove: mousemove, update: update };'
                        )
                        .bind( object ))( this, scene );

                    for ( var name in events ) {

                        if ( events[ name ] === undefined ) continue;

                        if ( scripts[ name ] === undefined ) {
                            console.warn( 'APP.Player: event type not supported (', name, ')' );
                            continue;
                        }

                        scripts[ name ].push( events[ name ].bind( object ) );
                    }
                }
            }

            this.dom = renderer.domElement;
            this.$scene_container_div.append(this.dom);
            
            $vrButton = $('<img />')
              .attr({
                src : Drupal.settings.cam3d_three.vr_goggles_image
              })
              .css({
                position : 'absolute',
                bottom : '0px',
                'margin-left' : '-22px',
                'margin-top' : '-1px',
                'z-index' : '100'
              })
              .bind('click', function() {
                  alert('going vr mode!');
                  vrMode = true;
                  vrEffect.setFullScreen( true );
                  console.log(vrEffect);
              });

            var onFullscreenChange = function() {
              if(!document.webkitFullscreenElement && !document.mozFullScreenElement) {
                vrMode = false;
              }
              this.setSize(this.width, this.height);
            };

            $(document).bind("webkitfullscreenchange", onFullscreenChange);
            $(document).bind("mozfullscreenchange", onFullscreenChange);

            this.$scene_container_div.append($vrButton);

            if (camera === undefined) {
                camera = new THREE.PerspectiveCamera( 75, this.width / this.height, 0.1, 1000 );
                vrControls = new THREE.VRControls(camera);
            }
        };

        this.setCamera = function ( value ) {
            camera = value;
            camera.aspect = this.width / this.height;
            camera.updateProjectionMatrix();
            vrControls = new THREE.VRControls(camera);
        };

        this.setSize = function ( width, height ) {
            this.width = width;
            this.height = height;
            camera.aspect = this.width / this.height;
            camera.updateProjectionMatrix();
            renderer.setSize( width, height );
            vrEffect.setSize( this.width, this.height );
        };

        var dispatch = function ( array, event ) {
            for ( var i = 0, l = array.length; i < l; i ++ ) {
                array[ i ]( event );
            }
        };

        var request;

        var animate = function ( time ) {
            request = requestAnimationFrame( animate );
            if (scripts.update !== undefined) dispatch( scripts.update, { time: time } );
            
            if (vrMode) {
              vrControls.update();
              vrEffect.render( scene, camera );
            }
            else {
                renderer.render( scene, camera );
            }
        };

        this.play = function () {
            this.$scene_container_div.bind( "keydown", onDocumentKeyDown );
            this.$scene_container_div.bind( "keyup", onDocumentKeyUp );
            this.$scene_container_div.bind( "mousedown", onDocumentMouseDown );
            this.$scene_container_div.bind( "mouseup", onDocumentMouseUp );
            this.$scene_container_div.bind( "mousemove", onDocumentMouseMove );
            request = requestAnimationFrame( animate );
        };

        this.stop = function () {
            this.$scene_container_div.unbind( 'keydown', onDocumentKeyDown );
            this.$scene_container_div.unbind( 'keyup', onDocumentKeyUp );
            this.$scene_container_div.unbind( 'mousedown', onDocumentMouseDown );
            this.$scene_container_div.unbind( 'mouseup', onDocumentMouseUp );
            this.$scene_container_div.unbind( 'mousemove', onDocumentMouseMove );
            cancelAnimationFrame( request );
        };
        
        this.render = function() {
            renderer.render( scene, camera );
        };

        var onDocumentKeyDown = function ( event ) {
            dispatch( scripts.keydown, event );
        };

        var onDocumentKeyUp = function ( event ) {
            dispatch( scripts.keyup, event );
        };

        var onDocumentMouseDown = function ( event ) {
            dispatch( scripts.mousedown, event );
        };

        var onDocumentMouseUp = function ( event ) {
            dispatch( scripts.mouseup, event );
        };

        var onDocumentMouseMove = function ( event ) {
            dispatch( scripts.mousemove, event );
        };
    };
    Drupal.behaviors.cam3d_three.resolve_object = function(resolve_object_id, resolve_callback) {
        if (Drupal.behaviors.cam3d_three.is_defined[Drupal.behaviors.cam3d_three.objectcollection['id'+resolve_object_id]])
                resolve_callback(Drupal.behaviors.cam3d_three.objectcollection['id'+resolve_object_id]);
        else {
            var resolve_object = {'object_id' : resolve_object_id};
            Drupal.behaviors.cam3d_three.read_remote(resolve_object, resolve_callback);
        }
    };
    Drupal.behaviors.cam3d_three.stackTrace = function() {
        var err = new Error();
        return err.stack;
    };
    Drupal.behaviors.cam3d_three.read_remote = function(object, read_remote_callback) {
      var data_json = JSON.stringify(object);

      var request = $.ajax({
        type: 'POST',
        url: Drupal.behaviors.cam3d_three.read_url,
        data: {data:data_json},
        success : function( data ) {
          Drupal.behaviors.cam3d_three.process_create(data, read_remote_callback);
        },
        error : function( jqXHR, textStatus, errorThrown ) {
          var failure_object = {
              object_requested : object,
              message : "Request failed: " + errorThrown
          };
          console.log(failure_object.message);

          // Failure not used yet
          read_remote_callback(null);
        }
      });
    };
    Drupal.behaviors.cam3d_three.read_remote_collection = function(collection, read_remote_callback) {

      var objects_accounted_for = 0;
      var objects_read = [];
      var test_all_objects_accounted_for = function(object) {
          if (object !== null) objects_read.push(object);

          objects_accounted_for ++;
          if (objects_accounted_for === collection.length) read_remote_callback(objects_read);
      };
      $.each(collection, function(i, object_to_read){
          Drupal.behaviors.cam3d_three.read_remote(object_to_read, test_all_objects_accounted_for);
      });
    };
    Drupal.behaviors.cam3d_three.is_defined = function(parameter) {
      return typeof parameter !== 'undefined' && parameter !== null;
    };
    Drupal.behaviors.cam3d_three.getKeys = function(obj){
      var keys = [];
      for(var key in obj){
        if (obj.hasOwnProperty(key)) {
          keys.push(key);
        }
      }
      return keys;
    };
    Drupal.behaviors.cam3d_three.request_controller = function(function_name, object_data, request_controller_callback, callback_immediately) {

      if (!Drupal.behaviors.cam3d_three.is_defined(callback_immediately)) var callback_immediately = false;

      var controller_data = $.extend({}, object_data);

      var data_json = JSON.stringify(controller_data);

      var request = $.ajax({
        type: 'POST',
        url: this.controllers_url,
        data : {data:data_json},
        success : (function( data ) {
          if (data.files && data.files.length > 0) {
            Drupal.behaviors.cam3d_three.load_controllers(data.files, function_name, object_data, request_controller_callback, callback_immediately);
          }
        }),
        fail : (function( jqXHR, textStatus ) {
          alert( "Request failed: " + textStatus );
        })
      });
    };
    Drupal.behaviors.cam3d_three.load_controllers = function(urls, function_name, object_data, load_controllers_callback, callback_immediately) {

      if (!Drupal.behaviors.cam3d_three.is_defined(callback_immediately)) var callback_immediately = false;

      var number_of_controllers_loaded = 0;
      var test_if_controller_loading_complete = function(object_data, ticlc_callback) {

        if (number_of_controllers_loaded === urls.length) {

          if (callback_immediately) load_controllers_callback();
          else if (Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers[object_data.object_type]) {
            Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers[object_data.object_type][function_name](object_data, ticlc_callback);  
          }
          else {
            if (console) console.log('Unable to negotiate controllers for ');
            if (console) console.log(object_data);
          }
        }
      };

      $.each(urls, function(i, url){
        if ($.inArray(url, Drupal.behaviors.cam3d_three.loaded_controller_uris) === -1) {

          var request = $.ajax({
            type: "GET",
            url: url,
            dataType: "script",
            success : (function( data ) {
              Drupal.behaviors.cam3d_three.loaded_controller_uris.push(url);
              number_of_controllers_loaded ++;
              test_if_controller_loading_complete(object_data, load_controllers_callback);
            }),
            fail : (function( jqXHR, textStatus ) {
              alert( "Request failed: " + textStatus );
            })
          });
        }
        else {
          number_of_controllers_loaded ++;
          test_if_controller_loading_complete(object_data, load_controllers_callback);
        }
      });
    };
    Drupal.behaviors.cam3d_three.process_create = function(data, process_create_callback){
      if (Drupal.behaviors.cam3d_three.is_defined(Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers[data.object_type])) {
        Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers[data.object_type].create(data, process_create_callback);
      }
      else {
        // Time to negotiate to load in javascript for unknown controller
        Drupal.behaviors.cam3d_three.request_controller('create', data, process_create_callback);
      }
    };
        
}(jQuery));

