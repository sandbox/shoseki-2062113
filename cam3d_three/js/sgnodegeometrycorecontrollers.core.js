
(function ($) {
    
    Drupal.behaviors.cam3d_three.sgnodegeometrycorecontrollers = Drupal.behaviors.cam3d_three.sgnodegeometrycorecontrollers || {};
    Drupal.behaviors.cam3d_three.geometrycollection = Drupal.behaviors.cam3d_three.geometrycollection || {};

    var genericControllerPrototype = {
        'create' : function(){
            console.log('Generic Geometry controller create!');
        },
        'read' : function(){
            console.log('Generic Geometry controller read!');
        },
        'update' : function(){
            console.log('Generic Geometry controller update!');
        },
        'delete' : function(){
            console.log('Generic Geometry controller delete!');
        }
    };
    var generic_update = function(data, callback) {
        if (data.object_type !== undefined && Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers[data.object_type] !== undefined) Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers[data.object_type].update(data, callback);
        else {
            if (data.object_type !== undefined) {
                console.log('No object type defined on ');
                console.log(data);
            }
            else {
                console.log('No controller defined for type ' + data.object_type);
            }
        }
    };

    Drupal.behaviors.cam3d_three.sgnodegeometrycorecontrollers.plane = {
        create : function(data, callback){
            
            var geometry = new THREE.PlaneGeometry(
                    data.width,
                    data.height,
                    data.widthSegments,
                    data.heightSegments
            );
            geometry.parameters = $.extend({}, data);
            geometry.geometry_type = 'plane';
            geometry.geometry_id = data.geometry_id;
            Drupal.behaviors.cam3d_three.geometrycollection['id' + data.geometry_id] = geometry;
            callback(geometry);
        },
        update : generic_update
    };
    
    Drupal.behaviors.cam3d_three.sgnodegeometrycorecontrollers.box = {
        create : function(data, callback){

            var geometry = new THREE.BoxGeometry(
                    data.width,
                    data.height,
                    data.depth,
                    data.widthSegments,
                    data.heightSegments,
                    data.depthSegments
            );
            geometry.parameters = $.extend({}, data);
            geometry.geometry_type = 'box';
            geometry.geometry_id = data.geometry_id;
            Drupal.behaviors.cam3d_three.geometrycollection['id' + data.geometry_id] = geometry;
            callback(geometry);
        },
        update : generic_update
    };

    Drupal.behaviors.cam3d_three.sgnodegeometrycorecontrollers.circle = {
        create : function(data, callback){
            
            var geometry = new THREE.CircleGeometry(
                    data.radius,
                    data.segments,
                    data.thetaStart,
                    data.thetaLength
            );
            geometry.parameters = $.extend({}, data);
            geometry.geometry_type = 'circle';
            geometry.geometry_id = data.geometry_id;
            Drupal.behaviors.cam3d_three.geometrycollection['id' + data.geometry_id] = geometry;
            callback(geometry);
        },
        update : generic_update
    };

    Drupal.behaviors.cam3d_three.sgnodegeometrycorecontrollers.cylinder = {
        create : function(data, callback){

            var geometry = new THREE.CylinderGeometry(
                    data.radiusTop,
                    data.radiusBottom,
                    data.height,
                    data.radialSegments,
                    data.heightSegments,
                    data.openEnded
            );
            geometry.parameters = $.extend({}, data);
            geometry.geometry_type = 'cylinder';
            geometry.geometry_id = data.geometry_id;
            Drupal.behaviors.cam3d_three.geometrycollection['id' + data.geometry_id] = geometry;
            callback(geometry);
        },
        update : generic_update
    };

    Drupal.behaviors.cam3d_three.sgnodegeometrycorecontrollers.sphere = {
        create : function(data, callback){

            var geometry = new THREE.SphereGeometry(
                    data.radius,
                    data.widthSegments,
                    data.heightSegments,
                    data.phiStart,
                    data.phiLength,
                    data.thetaStart,
                    data.thetaLength
            );
            geometry.parameters = $.extend({}, data);
            geometry.geometry_type = 'sphere';
            geometry.geometry_id = data.geometry_id;
            Drupal.behaviors.cam3d_three.geometrycollection['id' + data.geometry_id] = geometry;
            callback(geometry);
        },
        update : generic_update
    };

    Drupal.behaviors.cam3d_three.sgnodegeometrycorecontrollers.icosahedron = {
        create : function(data, callback){

            var geometry = new THREE.IcosahedronGeometry (
                    data.radius,
                    data.detail
            );
            geometry.parameters = $.extend({}, data);
            geometry.geometry_type = 'icosahedron';
            geometry.geometry_id = data.geometry_id;
            Drupal.behaviors.cam3d_three.geometrycollection['id' + data.geometry_id] = geometry;
            callback(geometry);
        },
        update : generic_update
    };

    Drupal.behaviors.cam3d_three.sgnodegeometrycorecontrollers.torus = {
        create : function(data, callback){

            var geometry = new THREE.TorusGeometry(
                    data.radius,
                    data.tube,
                    data.radialSegments,
                    data.tubularSegments,
                    data.arc
            );
            geometry.parameters = $.extend({}, data);
            geometry.geometry_type = 'torus';
            geometry.geometry_id = data.geometry_id;
            Drupal.behaviors.cam3d_three.geometrycollection['id' + data.geometry_id] = geometry;
            callback(geometry);
        },
        update : generic_update
    };

    Drupal.behaviors.cam3d_three.sgnodegeometrycorecontrollers.torusknot = {
        create : function(data, callback){

            var geometry = new THREE.TorusKnotGeometry(
                    data.radius,
                    data.tube,
                    data.radialSegments,
                    data.tubularSegments,
                    data.p,
                    data.q,
                    data.heightScale
            );
            geometry.parameters = $.extend({}, data);
            geometry.geometry_type = 'torusknot';
            geometry.geometry_id = data.geometry_id;
            Drupal.behaviors.cam3d_three.geometrycollection['id' + data.geometry_id] = geometry;
            callback(geometry);
        },
        update : generic_update
    };
    
    Drupal.behaviors.cam3d_three.sgnodegeometrycorecontrollers.text = {
        create : function(data, callback){

            var parameters = {
                size : data.size,
                height : data.height,
                curveSegments : data.curveSegments,
                bevelEnabled : data.bevelEnabled,
                bevelThickness : data.bevelThickness,
                bevelSize : data.bevelSize
            };
            // We have to define default in js to protect font detection, even though it should never be called
            if (Drupal.behaviors.cam3d_three.is_defined(data.font) && data.font.length > 0) parameters.font = data.font;
            else parameters.font = 'helvetiker';

            if (Drupal.behaviors.cam3d_three.is_defined(data.weight) && data.weight.length > 0) parameters.weight = data.weight;
            else parameters.weight = 'normal';

            if (Drupal.behaviors.cam3d_three.is_defined(data.style) && data.style.length > 0) parameters.style = data.style;
            else parameters.style = 'normal';

            var is_typeface_loaded = function(parameters) {
                var typeface_loaded = false;
                if (Drupal.behaviors.cam3d_three.is_defined(THREE.typeface_js) &&
                    Drupal.behaviors.cam3d_three.is_defined(THREE.typeface_js.faces) &&
                    Drupal.behaviors.cam3d_three.is_defined(THREE.typeface_js.faces[parameters.font]) &&
                    Drupal.behaviors.cam3d_three.is_defined(THREE.typeface_js.faces[parameters.font][parameters.weight]) &&
                    Drupal.behaviors.cam3d_three.is_defined(THREE.typeface_js.faces[parameters.font][parameters.weight][parameters.style]) ) typeface_loaded = true;
                return typeface_loaded;
            };
        
            var create_geometry_callback = function() {
                if (!is_typeface_loaded(parameters)) {
                    console.log('Font ' + parameters.font + ' ' + parameters.weight + ' ' + parameters.style + ' could not be loaded, using helvetiker normal normal instead.');
                    parameters.font = 'helvetiker';
                    parameters.weight = 'normal';
                    parameters.style = 'normal';
                }
            
                try {
                    var geometry = new THREE.TextGeometry(
                            data.text,
                            parameters
                    );
                    geometry.parameters = $.extend({}, data);
                    geometry.geometry_type = 'text';
                    geometry.geometry_id = data.geometry_id;
                    Drupal.behaviors.cam3d_three.geometrycollection['id' + data.geometry_id] = geometry;
                    callback(geometry);
                }
                catch (e) {
                    console.log(e);
                }
            };
            
            if (is_typeface_loaded(parameters)) {
                create_geometry_callback();
            }
            // Time to negotiate to load in javascript for unknown controller, must be FULL object
            else {
                // Such a weird situation
                // We need to trigger the load controllers for the piece of geometry
                // Except that usually this happens at the object level, not the sub object (geomtry) level
                // So we need to repackage the geometry inside parent object to retrieve the controllers
                var packaged_object = {
                    object_type : 'mesh',
                    geometry : data
                };
                Drupal.behaviors.cam3d_three.request_controller(null, packaged_object, create_geometry_callback, true);
            }
        },
        update : generic_update
    };

    $.each(Drupal.behaviors.cam3d_three.sgnodegeometrycorecontrollers, function(key, controller){
        controller.prototype = genericControllerPrototype;
    });

}(jQuery));