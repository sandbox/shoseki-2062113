
(function ($) {

    Drupal.behaviors.cam3d_three.sgnodematerialcorecontrollers = Drupal.behaviors.cam3d_three.sgnodematerialcorecontrollers || {};
    Drupal.behaviors.cam3d_three.materialcollection = Drupal.behaviors.cam3d_three.materialcollection || {};

    // For testing purposes
    //Drupal.behaviors.cam3d_three_editor.previous_material = null;

    var genericControllerPrototype = {
        'create' : function(){
            console.log('Generic Material controller create!');
        },
        'read' : function(){
            console.log('Generic Material controller read!');
        },
        'update' : function(){
            console.log('Generic Material controller update!');
        },
        'delete' : function(){
            console.log('Generic Material controller delete!');
        }
    };

    var is_defined = Drupal.behaviors.cam3d_three.is_defined;
    
    var getKeys = Drupal.behaviors.cam3d_three.getKeys;

    var generic_update = function(data, callback) {
        if (data.object_type !== undefined && Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers[data.object_type] !== undefined) {
            Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers[data.object_type].update(data, callback);
        }
        else {
            if (data.object_type !== undefined) {
                console.log('No object type defined on ');
                console.log(data);
            }
            else {
                console.log('No controller defined for type ' + data.object_type);
            }
        }
    };

    // By default, remove the object_type, material_type, material_id items
    // Copy rest in by value, will need overwrites for specific postprocessing
    var extract_material_parameters = function(original_data) {
        var data = $.extend({}, original_data);
        var materialProperties = {};
        if (is_defined(data.object_type)) delete data.object_type;
        if (is_defined(data.material_type)) delete data.material_type;
        if (is_defined(data.material_id)) delete data.material_id;
        
        $.each(Drupal.behaviors.cam3d_three.getKeys(data), function(i, v) {
            materialProperties[v] = data[v];
        });

        return materialProperties;
    };

    Drupal.behaviors.cam3d_three.sgnodematerialcorecontrollers.linebasic = {
        create : function(data, callback){
            
            var materialProperties = extract_material_parameters(data);

            var material = new THREE.LineBasicMaterial(materialProperties);
            material.parameters = $.extend({}, data);
            material.uuid = data.material_id;
            material.material_type = 'linebasic';
            material.material_id = data.material_id;

            Drupal.behaviors.cam3d_three.materialcollection['id' + data.material_id] = material;

            callback(material);
        },
        update : generic_update
    };
    Drupal.behaviors.cam3d_three.sgnodematerialcorecontrollers.linedashed = {
        create : function(data, callback){
            var materialProperties = extract_material_parameters(data);

            var material = new THREE.LineDashedMaterial(materialProperties);
            material.parameters = $.extend({}, data);
            material.uuid = data.material_id;
            material.material_type = 'linedashed';
            material.material_id = data.material_id;

            Drupal.behaviors.cam3d_three.materialcollection['id' + data.material_id] = material;
            callback(material);
        },
        update : generic_update
    };
    Drupal.behaviors.cam3d_three.sgnodematerialcorecontrollers.meshbasic = {
        create : function(data, callback){
            var materialProperties = extract_material_parameters(data);
            
            if (is_defined(data.map)) materialProperties.map = THREE.ImageUtils.loadTexture(data.map.url);
            if (is_defined(data.lightMap)) materialProperties.lightMap = THREE.ImageUtils.loadTexture(data.lightMap.url);
            if (is_defined(data.specularMap)) materialProperties.specularMap = THREE.ImageUtils.loadTexture(data.specularMap.url);
            
            var material = new THREE.MeshBasicMaterial(materialProperties);
            material.parameters = $.extend({}, data);

            // Copy back the original parameters into material.parameters
            if (is_defined(data.map)) material.parameters.map = data.map;
            if (is_defined(data.lightMap)) material.parameters.lightMap = data.lightMap;
            if (is_defined(data.specularMap)) material.parameters.specularMap = data.specularMap;
            
            material.uuid = data.material_id;
            material.material_type = 'meshbasic';
            material.material_id = data.material_id;

            Drupal.behaviors.cam3d_three.materialcollection['id' + data.material_id] = material;
            callback(material);
        },
        update : generic_update
    };
    Drupal.behaviors.cam3d_three.sgnodematerialcorecontrollers.meshdepth = {
        create : function(data, callback){
            var materialProperties = extract_material_parameters(data);

            var material = new THREE.MeshDepthMaterial(materialProperties);
            material.parameters = $.extend({}, data);
            material.uuid = data.material_id;
            material.material_type = 'meshdepth';
            material.material_id = data.material_id;

            Drupal.behaviors.cam3d_three.materialcollection['id' + data.material_id] = material;
            callback(material);
        },
        update : generic_update
    };
    Drupal.behaviors.cam3d_three.sgnodematerialcorecontrollers.meshface = {
        create : function(data, callback){
            var materialProperties = extract_material_parameters(data);

            var material = new THREE.MeshFaceMaterial(materialProperties);
            material.parameters = $.extend({}, data);
            material.uuid = data.material_id;
            material.material_type = 'meshface';
            material.material_id = data.material_id;

            Drupal.behaviors.cam3d_three.materialcollection['id' + data.material_id] = material;
            callback(material);
        },
        update : generic_update
    };
    Drupal.behaviors.cam3d_three.sgnodematerialcorecontrollers.meshlambert = {
        create : function(data, callback){
            var materialProperties = extract_material_parameters(data);
            
            if (is_defined(data.map)) materialProperties.map = THREE.ImageUtils.loadTexture(data.map.url);
            if (is_defined(data.lightMap)) materialProperties.lightMap = THREE.ImageUtils.loadTexture(data.lightMap.url);
            if (is_defined(data.specularMap)) materialProperties.specularMap = THREE.ImageUtils.loadTexture(data.specularMap.url);

            var material = new THREE.MeshLambertMaterial(materialProperties);
            material.parameters = $.extend({}, data);

            // Copy back the original parameters into material.parameters
            if (is_defined(data.map)) material.parameters.map = data.map;
            if (is_defined(data.lightMap)) material.parameters.lightMap = data.lightMap;
            if (is_defined(data.specularMap)) material.parameters.specularMap = data.specularMap;
            
            material.uuid = data.material_id;
            material.material_type = 'meshlambert';
            material.material_id = data.material_id;

            Drupal.behaviors.cam3d_three.materialcollection['id' + data.material_id] = material;
            callback(material);
        },
        update : generic_update
    };
    Drupal.behaviors.cam3d_three.sgnodematerialcorecontrollers.meshnormal = {
        create : function(data, callback){
            var materialProperties = extract_material_parameters(data);

            var material = new THREE.MeshNormalMaterial(materialProperties);
            material.parameters = $.extend({}, data);
            material.uuid = data.material_id;
            material.material_type = 'meshnormal';
            material.material_id = data.material_id;

            Drupal.behaviors.cam3d_three.materialcollection['id' + data.material_id] = material;
            callback(material);
        },
        update : generic_update
    };
    Drupal.behaviors.cam3d_three.sgnodematerialcorecontrollers.meshphong = {
        create : function(data, callback){

            var materialProperties = extract_material_parameters(data);
            
            if (is_defined(data.map) && is_defined(data.map.url)) materialProperties.map = THREE.ImageUtils.loadTexture(data.map.url);
            if (is_defined(data.lightMap) && is_defined(data.lightMap.url)) materialProperties.lightMap = THREE.ImageUtils.loadTexture(data.lightMap.url);
            if (is_defined(data.bumpMap) && is_defined(data.bumpMap.url)) materialProperties.bumpMap = THREE.ImageUtils.loadTexture(data.bumpMap.url);
            if (is_defined(data.normalMap) && is_defined(data.normalMap.url)) materialProperties.normalMap = THREE.ImageUtils.loadTexture(data.normalMap.url);
            if (is_defined(data.specularMap) && is_defined(data.specularMap.url)) materialProperties.specularMap = THREE.ImageUtils.loadTexture(data.specularMap.url);

            var material = new THREE.MeshPhongMaterial(materialProperties);
            material.parameters = $.extend({}, data);

            // Copy back the original parameters into material.parameters
            if (is_defined(data.map)) material.parameters.map = data.map;
            if (is_defined(data.lightMap)) material.parameters.lightMap = data.lightMap;
            if (is_defined(data.bumpMap)) material.parameters.bumpMap = data.bumpMap;
            if (is_defined(data.normalMap)) material.parameters.normalMap = data.normalMap;
            if (is_defined(data.specularMap)) material.parameters.specularMap = data.specularMap;

            material.uuid = data.material_id;
            material.material_type = 'meshphong';
            material.material_id = data.material_id;
            
            Drupal.behaviors.cam3d_three.materialcollection['id' + data.material_id] = material;
            
            callback(material);
        },
        update : generic_update
    };
    Drupal.behaviors.cam3d_three.sgnodematerialcorecontrollers.particlesystem = {
        create : function(data, callback){
            var materialProperties = extract_material_parameters(data);
                        
            var material = new THREE.ParticleSystemMaterial(materialProperties);
            material.parameters = $.extend({}, data);
            material.uuid = data.material_id;
            material.material_type = 'particlesystem';
            material.material_id = data.material_id;

            Drupal.behaviors.cam3d_three.materialcollection['id' + data.material_id] = material;
            callback(material);
        },
        update : generic_update
    };
    Drupal.behaviors.cam3d_three.sgnodematerialcorecontrollers.shader = {
        create : function(data, callback){
            var materialProperties = extract_material_parameters(data);
            
            var material = new THREE.ShaderMaterial(materialProperties);
            material.parameters = $.extend({}, data);
            material.uuid = data.material_id;
            material.material_type = 'shader';
            material.material_id = data.material_id;

            Drupal.behaviors.cam3d_three.materialcollection['id' + data.material_id] = material;
            callback(material);
        },
        update : generic_update
    };
    Drupal.behaviors.cam3d_three.sgnodematerialcorecontrollers.spritecanvas = {
        create : function(data, callback){
            var materialProperties = extract_material_parameters(data);

            var material = new THREE.SpriteCanvasMaterial(materialProperties);
            material.parameters = $.extend({}, data);
            material.uuid = data.material_id;
            material.material_type = 'spritecanvas';
            material.material_id = data.material_id;

            Drupal.behaviors.cam3d_three.materialcollection['id' + data.material_id] = material;
            callback(material);
        },
        update : generic_update
    };
    Drupal.behaviors.cam3d_three.sgnodematerialcorecontrollers.sprite = {
        create : function(data, callback){
            var materialProperties = extract_material_parameters(data);

            if (is_defined(data.map)) materialProperties.map = THREE.ImageUtils.loadTexture(data.map.url);
            if (is_defined(data.uvOffset)) materialProperties.uvOffset = new THREE.Vector2(data.uvOffset[0], data.uvOffset[1]);
            if (is_defined(data.uvScale)) materialProperties.uvScale = new THREE.Vector2(data.uvScale[0], data.uvScale[1]);

            var material = new THREE.SpriteMaterial(materialProperties);
            material.parameters = $.extend({}, data);

            // Copy back the original parameters into material.parameters
            if (is_defined(data.map)) material.parameters.map = data.map;
            if (is_defined(data.uvOffset)) material.parameters.uvOffset = data.uvOffset;
            if (is_defined(data.uvScale)) material.parameters.uvScale = data.uvScale;
            
            material.name = 'SpriteMaterial ' + data.material_id;
            material.uuid = data.material_id;
            material.material_type = 'sprite';
            material.material_id = data.material_id;

            Drupal.behaviors.cam3d_three.materialcollection['id' + data.material_id] = material;
            callback(material);
        },
        update : generic_update
    };

    $.each(Drupal.behaviors.cam3d_three.sgnodematerialcorecontrollers, function(key, controller){
        controller.prototype = genericControllerPrototype;
    });
}(jQuery));