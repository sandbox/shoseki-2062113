<?php

/**
 *  Create an instance like this:
 *  $sgnodegeometrycorewrapper = new SGNodeGeometryCoreStreamWrapper('sgnodegeometrycore://type/object/param1/param1value');
 */
class SGNodeGeometryCoreStreamWrapper extends MediaReadOnlyStreamWrapper {
  static function getMimeType($uri, $mapping = NULL) {      
    return 'application/sgnodegeometrycore';
  }

  static function generate_uri($parameters) {
    $scheme = 'sgnodegeometrycore://';
    $packaged_parameters = array();
    foreach ($parameters as $parameter_key => $parameter_value) {
        $packaged_parameters[] = implode('/', array($parameter_key, urlencode($parameter_value)));
    }
    $uri = $scheme . implode('/', $packaged_parameters);
    return $uri;
  }

  public function get_parameters() {
  	
    $unprocessed_parameters = $this->parameters;
    $processed_parameters = array();
    foreach($unprocessed_parameters as $unprocessed_parameter_key => $unprocessed_parameter) {
        $processed_parameters[$unprocessed_parameter_key] = urldecode($unprocessed_parameter);
    }

    return $processed_parameters;
  }
  
  public function set_parameters($parameters) {
    $processed_parameters = array();
    foreach($unprocessed_parameters as $unprocessed_parameter_key => $unprocessed_parameter) {
        $processed_parameters[$unprocessed_parameter_key] = urlencode($unprocessed_parameter);
    }

    $this->parameters = $processed_parameters;
  }
  
  function getTarget($f) {
    return FALSE;
  }

  function interpolateUrl() {
    return;
  }

  function getOriginalThumbnailStylePath($style) {
    return '';
  }

  function getOriginalThumbnailPath() {
    return '';
  }

  function getLocalThumbnailPath() {
    return '';
  }
}