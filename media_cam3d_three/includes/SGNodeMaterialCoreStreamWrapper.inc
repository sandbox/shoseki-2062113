<?php

/**
 *  Create an instance like this:
 *  $sgnodematerialcorewrapper = new SGNodeMaterialCoreStreamWrapper('sgnodematerialcore://material_type/phong/param1/param1value');
 */
class SGNodeMaterialCoreStreamWrapper extends MediaReadOnlyStreamWrapper {
  static function getMimeType($uri, $mapping = NULL) {      
    return 'application/sgnodematerialcore';
  }

  static function generate_uri($parameters) {
      
    // Functions for making the parameters safe should have already been called, these parameters are already considered safe
    $scheme = 'sgnodematerialcore://';
    $packaged_parameters = array();
    foreach ($parameters as $parameter_key => $parameter_value) {
        $packaged_parameters[] = implode('/', array($parameter_key, urlencode($parameter_value)));
    }
    $uri = $scheme . implode('/', $packaged_parameters);
    return $uri;
  }
  
  // This function overrides the default one, as all parameters are urlencoded during storage
  public function get_parameters() {
  	
    $unprocessed_parameters = $this->parameters;
    $processed_parameters = array();
    foreach($unprocessed_parameters as $unprocessed_parameter_key => $unprocessed_parameter) {
        $processed_parameters[$unprocessed_parameter_key] = urldecode($unprocessed_parameter);
    }

    return $processed_parameters;
  }
  
  public function set_parameters($parameters) {
    $processed_parameters = array();
    foreach($unprocessed_parameters as $unprocessed_parameter_key => $unprocessed_parameter) {
        $processed_parameters[$unprocessed_parameter_key] = urlencode($unprocessed_parameter);
    }

    $this->parameters = $processed_parameters;
  }
  
  function getTarget($f) {
    return FALSE;
  }

  function interpolateUrl() {
    return;
  }

  function getOriginalThumbnailStylePath($style) {
    return '';
  }

  function getOriginalThumbnailPath() {
    return '';
  }

  function getLocalThumbnailPath() {
    return '';
  }
}