<?php
/**
 * @file
 * sgnodegeometrycore_file_entity.file_type.inc
 */

/**
 * Implements hook_file_default_types().
 */
function sgnodegeometrycore_file_entity_file_default_types() {
  $export = array();

  $file_type = new stdClass();
  $file_type->disabled = FALSE; /* Edit this to true to make a default file_type disabled initially */
  $file_type->api_version = 1;
  $file_type->type = 'sgnodegeometrycore';
  $file_type->label = 'SGNodeGeometryCore';
  $file_type->description = 'A virtual file representing a piece of geometry, contains parameters for THREE.js to process.';
  $file_type->mimetypes = array(
    0 => 'application/sgnodegeometrycore',
  );
  $export['sgnodegeometrycore'] = $file_type;

  return $export;
}
