<?php
/**
 * @file
 * sgnodematerialcore_file_entity.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sgnodematerialcore_file_entity_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "file_entity" && $api == "file_type") {
    return array("version" => "1");
  }
}
