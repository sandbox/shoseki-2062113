<?php
/**
 * @file
 * sgnodematerialcore_file_entity.file_type.inc
 */

/**
 * Implements hook_file_default_types().
 */
function sgnodematerialcore_file_entity_file_default_types() {
  $export = array();

  $file_type = new stdClass();
  $file_type->disabled = FALSE; /* Edit this to true to make a default file_type disabled initially */
  $file_type->api_version = 1;
  $file_type->type = 'sgnodematerialcore';
  $file_type->label = 'SGNodeMaterialCore';
  $file_type->description = 'A virtual file representing a material, contains parameters for THREE.js to process.';
  $file_type->mimetypes = array(
    0 => 'application/sgnodematerialcore',
  );
  $export['sgnodematerialcore'] = $file_type;

  return $export;
}
