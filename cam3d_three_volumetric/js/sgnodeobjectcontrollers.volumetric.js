
(function ($) {

    Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers = Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers || {};
    Drupal.behaviors.cam3d_three.sgnodegeometrycorecontrollers = Drupal.behaviors.cam3d_three.sgnodegeometrycorecontrollers || {};
    Drupal.behaviors.cam3d_three.sgnodematerialcorecontrollers = Drupal.behaviors.cam3d_three.sgnodematerialcorecontrollers || {};
    Drupal.behaviors.cam3d_three.objectcollection = Drupal.behaviors.cam3d_three.objectcollection || {};
    Drupal.behaviors.cam3d_three.geometrycollection = Drupal.behaviors.cam3d_three.geometrycollection || {};
    Drupal.behaviors.cam3d_three.materialcollection = Drupal.behaviors.cam3d_three.materialcollection || {};

    var genericControllerPrototype = {
        'create' : function(data, callback) {
            console.log('Generic Object controller create!');
        },
        'read' : function(data, callback) {
            console.log('Generic Object controller read!');
        },
        'update' : function(data, callback) {
            console.log('Generic Object controller update!');
        },
        'delete' : function(data, callback) {
            console.log('Generic Object controller delete!');
        }
    };
    
    // Takes in two arrays (bottom left, top right) and a function
    // And generates the i,j,k values and uses the passed in function
    // To generate a color, then stores it
    var makeVoxels = function(l, h, f) {

        var d = [ h[0]-l[0], h[1]-l[1], h[2]-l[2] ]
          , v = new Int32Array(d[0]*d[1]*d[2])
          , n = 0;

        for(var k=l[2]; k<h[2]; ++k)
        for(var j=l[1]; j<h[1]; ++j)
        for(var i=l[0]; i<h[0]; ++i, ++n) {
          v[n] = f(i,j,k);
        }
        return {voxels:v, dims:d};
    };
    
    var endsWith = function(string, suffix) {
        return string.indexOf(suffix, this.length - suffix.length) !== -1;
    };
    
    var load_image_from_url_and_convert_to_data = function(slice_index, url, slice_callback) {
        var $image = $( '<img />' );
        $image.bind('load', function() {
            var image_width = this.width, image_height = this.height;
            var $canvas = $('<canvas />').attr({
                width : image_width,
                height : image_height
            });

            var canvas_ctx = $canvas.get(0).getContext( '2d' );
            
            canvas_ctx.drawImage($image.get(0) , 0, 0);
            
            var myImageData = canvas_ctx.getImageData(0, 0, image_width, image_height);
            slice_callback({index : slice_index, width : image_width, height : image_height, data : myImageData.data});
        });
        $image.attr({src : url});
    };

    var is_defined = function(parameter) {
        return typeof parameter !== 'undefined' && parameter !== null;
    };
    
    var getKeys = function(obj){
       var keys = [];
       for(var key in obj){
         if (obj.hasOwnProperty(key)) {
           keys.push(key);
         }
       }
       return keys;
    };
    
    Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers.volumetric = {
        create : function(volumetric_data, volumetric_create_callback){
            
            var slices = volumetric_data.slices.urls;
            
            // If it isn't an array 
            if (slices == null || slices.constructor != Array) {
               if (!endsWith(volumetric_data.slices.url, 'gif')) slices = [volumetric_data.slices.url];
               else slices = [];
            }

            var create_volumetric_mesh = function(volumetric_geometry) {
                var children = volumetric_data.children;
                if (!is_defined(volumetric_data.position)) volumetric_data.position = {x : 0.0, y : 0.0, z : 0.0};
                if (!is_defined(volumetric_data.rotation)) volumetric_data.rotation = {x : 0.0, y : 0.0, z : 0.0};
                if (!is_defined(volumetric_data.scale)) volumetric_data.scale = {x : 1.0, y : 1.0, z : 1.0};

                var volumetric_material = new THREE.MeshBasicMaterial({vertexColors: true});

                var volumetric = new THREE.Mesh(volumetric_geometry, volumetric_material);
                volumetric.parameters = volumetric_data;
                volumetric.object_type = 'volumetric';
                volumetric.name = volumetric_data.name;

                volumetric.position.set(volumetric_data.position.x, volumetric_data.position.y, volumetric_data.position.z);
                volumetric.rotation.set(volumetric_data.rotation.x, volumetric_data.rotation.y, volumetric_data.rotation.z);
                volumetric.scale.set(volumetric_data.scale.x, volumetric_data.scale.y, volumetric_data.scale.z);

                volumetric_create_callback(volumetric);
            };

            if (slices.length > 0) {
              var total_slices_loaded = 0, processed_slices = [];

              var process_all_slices = function(processed_slices) {
                  
                var dims = [processed_slices[0].width, processed_slices[0].height, processed_slices.length];
                var total_length = dims[0] * dims[1] * dims[2];
                
                var volumetric_data_array = new Int32Array(total_length);
                var source_index = 0, n = 0;
                var current_slice = undefined;

                for(var k = 0; k < dims[2]; ++k) {
                    
                    current_slice = processed_slices[k];

                    for(var j=0; j < dims[1]; ++j) {
                        for(var i = 0; i < dims[0]; ++i, ++n) {
                          source_index = (j * dims[0] + i) * 4;
                          
                          volumetric_data_array[n] =  ((current_slice.data[source_index + 0] & 0xff) << 16) | // red
                                    ((current_slice.data[source_index + 1] & 0xff) << 8) | // green
                                    ((current_slice.data[source_index + 2] & 0xff) << 0); // blue
                        }
                    }
                }   
                var data = {voxels:volumetric_data_array, dims:dims};
                
                /*ar data = makeVoxels([0,0,0], [16,16,16], function(i,j,k) {
                  return Math.random() < 0.1 ? Math.random() * 0xffffff : 0;
                });*/
                  
                var geometry = new THREE.Geometry();

                result = window[volumetric_data.function_name](data.voxels, data.dims);
                geometry.vertices.length = 0;
                geometry.faces.length = 0;
                  
                for (var i=0; i < result.vertices.length; ++i) {
                  var q = result.vertices[i];
                  geometry.vertices.push(new THREE.Vector3(q[0], q[1], q[2]));
                }
                for(var i=0; i < result.faces.length; ++i) {
                  var q = result.faces[i];
                  if (q.length == 4) {
                    var f = new THREE.Face3(q[0], q[1], q[2]);
                    f.color = new THREE.Color(q[3]);
                    f.vertexColors = [f.color,f.color,f.color];
                    geometry.faces.push(f);
                  }
                }

                geometry.computeFaceNormals();

                geometry.verticesNeedUpdate = true;
                geometry.elementsNeedUpdate = true;
                geometry.normalsNeedUpdate = true;

                geometry.computeBoundingBox();
                geometry.computeBoundingSphere();
                  
                create_volumetric_mesh(geometry);
              };

              var slice_callback = function(slice_object) {
                  processed_slices[slice_object.index] = slice_object;
                  total_slices_loaded ++;
                  
                  if (total_slices_loaded == slices.length) {
                      process_all_slices(processed_slices);
                  }
              };
                
              $.each(slices, function(index, slice_url) {
                  load_image_from_url_and_convert_to_data(index, slice_url, slice_callback);
              });
            }
            else {
                volumetric_create_callback(null);
            }
        },
        update : function(data, update_callback) {

            var update_key = {
                object_type : data.object_type,
                object_id : data.object_id
            };
            var update_data = $.extend({}, data);

            if (is_defined(update_data.slices.fid)) update_data.slices = update_data.slices.fid;

            //Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers[data.object_type].create(data, update_callback);
            
            var object_update_callback = function(data, callback) {
                Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers[data.object_type].create(data, update_callback);
                //console.log('Backend returned this after update :');
                //console.log(data);
            };

            Drupal.behaviors.cam3d_three_editor.update_remote(update_data, Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers[data.object_type].create, JSON.stringify(update_key));
        }
    };


    $.each(Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers, function(key, controller){
        controller.prototype = genericControllerPrototype;
    });

}(jQuery));