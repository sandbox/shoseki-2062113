
(function ($) {

    // Create default object if not defined
    Drupal.behaviors.cam3d_three_editor = Drupal.behaviors.cam3d_three_editor || {};

    Drupal.behaviors.cam3d_three_editor.general_object_update_timers = Drupal.behaviors.cam3d_three_editor.general_object_update_timers || {};

    Sidebar.Object3D.volumetric = function ( editor, sidebar_object_signals, sidebar_object, parent_update ) {

        var ui_elements = $.extend(Drupal.behaviors.cam3d_three_editor.base_object_ui_elements(), {
            rotation : {
                label : new UI.Text( 'Rotation' ).setWidth( '90px' ),
                element : new UI.TextArea().setWidth( '150px' ).setHeight( '80px' )
            },
            scale : {
                label : new UI.Text( 'Scale' ).setWidth( '90px' ),
                element : new UI.TextArea().setWidth( '150px' ).setHeight( '80px' )
            },
            slices : {
                label : new UI.Text( 'Slices image' ).setWidth( '90px' ),
                element : new UI.CAM3DTexture('Select').setColor( '#444' )
            },
            slices_style : {
                label : new UI.Text( 'Image style' ).setWidth( '90px' ),
                element : new UI.TextArea().setWidth( '150px' ).setHeight( '80px' )
            }
            
        });
        
        ui_elements = Drupal.behaviors.cam3d_three_editor.attach_typed_value_functions(ui_elements, ['rotation', 'scale'], 'vector3');

        var update = Drupal.behaviors.cam3d_three_editor.general_object_update(editor, sidebar_object_signals, sidebar_object, ui_elements, parent_update);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), sidebar_object);
    };

}(jQuery));
