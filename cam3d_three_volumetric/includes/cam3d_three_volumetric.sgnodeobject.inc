<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function cam3d_three_volumetric_sgnodeobjectcontrollers_info() {

    // {$view->name}--{$display->id}
    
    // Default position for objects
    $default_position = new stdClass();
    $default_position->x = 0.0;
    $default_position->y = 0.0;
    $default_position->z = 0.0;

    // Default rotation for objects
    $default_rotation = new stdClass();
    $default_rotation->x = 0.0;
    $default_rotation->y = 0.0;
    $default_rotation->z = 0.0;

    // Default scale for objects
    $default_scale = new stdClass();
    $default_scale->x = 1.0;
    $default_scale->y = 1.0;
    $default_scale->z = 1.0;
    
    // Default scripts for objects
    $default_scripts = array();
    
    // Default paradata
    $default_paradata = new stdClass();
    
    // Default view data
    $default_view_data = new stdClass();
  
    $controllers['volumetric'] = array(
        'label' => 'Volumetric Object',
        'data_controller' => 'VolumetricObjectCAM3DController',
        'variables' => array(
            'slices' => array('default_value' => CAM3D_THREE_DEFAULT_IMAGE, 'type' => 'image_gif'),
            'slices_style' => array('default_value' => '', 'type' => 'string'),
            'rotation' => array('default_value' => $default_rotation, 'type' => 'object'),
            'scale' => array('default_value' => $default_scale, 'type' => 'object'),
            'algorithm' => array('default_value' => 'monotone', 'type' => 'string'),
            'function_name' => array('default_value' => '', 'type' => 'string'),
            'object_type' => array('default_value' => 'volumetric', 'type' => 'string'),
        ),
    );
    
    foreach ($controllers as $controllers_key => $controller) {
        $controllers[$controllers_key]['variables']['children'] = array('default_value' => array(), 'type' => 'array');
        $controllers[$controllers_key]['variables']['object_id'] = array('default_value' => -1, 'type' => 'integer');
        $controllers[$controllers_key]['variables']['position'] = array('default_value' => $default_position, 'type' => 'object');
        $controllers[$controllers_key]['variables']['name'] = array('default_value' => '', 'type' => 'string');
        $controllers[$controllers_key]['variables']['scripts'] = array('default_value' => $default_scripts, 'type' => 'object');
    }
  
    return $controllers;
}

class VolumetricObjectCAM3DController extends GenericCAM3DController {
    public function create($default_variables, $variables) {
        return parent::create($default_variables, $variables, 'volumetric');
    }
    
    public function read($default_variables, $entity) {
        
        // $id by default is the id of our entity
        // Parameters includes name => default_value and type
        
        if (is_string($entity->variables)) $entity->variables = unserialize($entity->variables);

        $variables = !empty($entity->variables) ? $entity->variables : array();
        $variables['object_id'] = $entity->basic_id;
        if (is_string($variables['object_id'])) $variables['object_id'] = intval($variables['object_id']);
        $variables['name'] = $entity->title;
        
        $algorithms = cam3d_three_volumetric_retrieve_algorithms();
        if (!empty($algorithms[$variables['algorithm']]) && !empty($algorithms[$variables['algorithm']]['function_name']))
            $variables['function_name'] = $algorithms[$variables['algorithm']]['function_name'];
        
        return $this->generate_json($default_variables, $variables);
    }
    
    public function front_end_controllers($variables) {
        
        $files = array();
        
        global $base_root;
        $files[] = $base_root . '/' . drupal_get_path('module', 'cam3d_three_volumetric') . '/js/sgnodeobjectcontrollers.volumetric.js';
        
        // Check to see if $variables contains the editor value, in which case add core.ui controllers
        if (!empty($variables['ui'])) {
          $files[] = $base_root . '/' . drupal_get_path('module', 'cam3d_three_volumetric') . '/js/sgnodeobjectcontrollers.volumetric.ui.js';
        }
        
        $algorithms = cam3d_three_volumetric_retrieve_algorithms();
        $files[] = $algorithms[$variables['algorithm']]['file'];
        
        return $files;
    }
}

function cam3d_three_volumetric_cam3d_three_variable_postprocessing_info() {

  $variable_postprocessing_info = array();
  
  $variable_postprocessing_info['image_gif'] = '_cam3d_three_split_gif';
  
  return $variable_postprocessing_info;
}

function _cam3d_three_volumetric_startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
}

function _cam3d_three_split_gif($consolidated_variables, $consolidated_variable_key) {
    
    $file_id = $consolidated_variables[$consolidated_variable_key];
    $file_style_key = $consolidated_variable_key . '_style';
    
    if (!empty($consolidated_variables[$file_style_key])) $file_style = $consolidated_variables[$file_style_key];
    else $file_style = '';

    // If this function gets called multiple times, it only needs to convert an id into an object once
    if (is_object($file_id)) {
        return $file_id;
    }

    if ($file_id < 0) {
      if ($file_id == -1) $file_id = variable_get('cam3d_three_default_tex0_id', -1);
    }
    if ($file_id >= 0) {
        $file_entity = file_load($file_id);

        $image = new stdClass();

        $url = $file_entity->uri;

        module_load_include('module', 'gif_frame_extractor');
        $extracted_urls = _gif_frame_extractor_extract_images($url);

        $image->url = file_create_url($url);
        $image->urls = $extracted_urls != null ? $extracted_urls : array($url);
        
        foreach($image->urls as $image_delta => $image_url) {
          if ($file_style != '') $image->urls[$image_delta] = image_style_url ($file_style, $image_url);
          else {
              if (_cam3d_three_volumetric_startsWith($image_url, 'http')) {
                  $image->urls[$image_delta] = $image_url;
              }
              else {
                  $image->urls[$image_delta] = file_create_url($image_url);
              }
              
          }
        }
        
        return $image;
    }
}

function cam3d_three_volumetric_retrieve_algorithms() {
    $data = array();
    $hook_name = 'volumetric_algorithms_info';
    $data = module_invoke_all($hook_name);
    drupal_alter($hook_name, $data);
    return $data;
}

function cam3d_three_volumetric_volumetric_algorithms_info() {

    global $base_root;

    $cam3d_three_volumetric_path = drupal_get_path('module', 'cam3d_three_volumetric');

    $algorithms = array();

    $algorithms['culled'] = array(
      'file' => $base_root . '/' . $cam3d_three_volumetric_path . '/js/algorithms/culled.js',
      'function_name' => 'CulledMesh',
    );
    $algorithms['monotone'] = array(
      'file' => $base_root . '/' . $cam3d_three_volumetric_path . '/js/algorithms/monotone.js',
      'function_name' => 'MonotoneMesh',
    );
    $algorithms['greedy'] = array(
      'file' => $base_root . '/' . $cam3d_three_volumetric_path . '/js/algorithms/greedy.js',
      'function_name' => 'GreedyMesh',
    );

    return $algorithms;
}