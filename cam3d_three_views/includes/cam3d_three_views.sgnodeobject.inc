<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function cam3d_three_views_sgnodeobjectcontrollers_info() {

    // {$view->name}--{$display->id}
    
    // Default position for objects
    $default_position = new stdClass();
    $default_position->x = 0.0;
    $default_position->y = 0.0;
    $default_position->z = 0.0;

    // Default rotation for objects
    $default_rotation = new stdClass();
    $default_rotation->x = 0.0;
    $default_rotation->y = 0.0;
    $default_rotation->z = 0.0;

    // Default scale for objects
    $default_scale = new stdClass();
    $default_scale->x = 1.0;
    $default_scale->y = 1.0;
    $default_scale->z = 1.0;
    
    // Default scripts for objects
    $default_scripts = array();
    
    // Default paradata
    $default_paradata = new stdClass();
    
    // Default view data
    $default_view_data = new stdClass();
  
    $controllers['view'] = array(
        'label' => 'View Generated Object',
        'data_controller' => 'ViewObjectCAM3DController',
        'variables' => array(
            'rotation' => array('default_value' => $default_rotation, 'type' => 'object'),
            'scale' => array('default_value' => $default_scale, 'type' => 'object'),
            'view_name' => array('default_value' => '', 'type' => 'string'),
            'view_display' => array('default_value' => '', 'type' => 'string'),
            'view_parameters' => array('default_value' => $default_paradata, 'type' => 'object'),
            'view_data' => array('default_value' => $default_paradata, 'type' => 'object'),
            'object_type' => array('default_value' => 'view', 'type' => 'string'),
        ),
    );
    
    foreach ($controllers as $controllers_key => $controller) {
        $controllers[$controllers_key]['variables']['children'] = array('default_value' => array(), 'type' => 'array');
        $controllers[$controllers_key]['variables']['object_id'] = array('default_value' => -1, 'type' => 'integer');
        $controllers[$controllers_key]['variables']['position'] = array('default_value' => $default_position, 'type' => 'object');
        $controllers[$controllers_key]['variables']['name'] = array('default_value' => '', 'type' => 'string');
        $controllers[$controllers_key]['variables']['scripts'] = array('default_value' => $default_scripts, 'type' => 'object');
    }
  
    return $controllers;
}

class ViewObjectCAM3DController extends GenericCAM3DController {
    public function create($default_variables, $variables) {
        return parent::create($default_variables, $variables, 'view');
    }
    public function read($default_variables, $entity) {
        
        // $id by default is the id of our entity
        // Parameters includes name => default_value and type

        if (is_string($entity->variables)) $entity->variables = unserialize($entity->variables);

        $variables = !empty($entity->variables) ? $entity->variables : array();
        $variables['object_id'] = $entity->basic_id;
        if (is_string($variables['object_id'])) $variables['object_id'] = intval($variables['object_id']);
        $variables['name'] = $entity->title;
        
        $view_name = $variables['view_name'];
        $view_display = $variables['view_display'];

        $variables['view_data'] = views_json_get($view_name, $view_display);
        
        return $this->generate_json($default_variables, $variables);
    }
    public function update($default_variables, $variables) {

      $id = $variables['object_id'];
      unset($variables['object_id']);
      
      $type = $variables['object_type'];
      unset($variables['object_type']);
      
      $name = $variables['name'];
      unset($variables['name']);
      
      if (!empty($variables['view_data'])) unset($variables['view_data']);
      
      // Load the entity from the db, update it and save
      $entity = sgnodeobject_load($id);

      // Save title
      $entity->title = $name;
      
      // Consolidate variables, this has been replaced
      // $original_variables = $entity->variables;
      
      // Save variables
      $entity->variables = $variables;
      
      // Save entity      
      $entity = sgnodeobject_save($entity);      

      return $this->read($default_variables, $entity);
    }
    public function front_end_controllers($variables) {
        
        $files = array();
        
        global $base_root;
        $files[] = $base_root . '/' . drupal_get_path('module', 'cam3d_three_views') . '/js/sgnodeobjectcontrollers.views.js';
        
        // Check to see if $variables contains the editor value, in which case add core.ui controllers
        if (!empty($variables['ui'])) {
            $files[] = $base_root . '/' . drupal_get_path('module', 'cam3d_three_views') . '/js/sgnodeobjectcontrollers.views.ui.js';
        }
        
        return $files;
    }
}