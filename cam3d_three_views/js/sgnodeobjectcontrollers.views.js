
(function ($) {

    Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers = Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers || {};
    Drupal.behaviors.cam3d_three.sgnodegeometrycorecontrollers = Drupal.behaviors.cam3d_three.sgnodegeometrycorecontrollers || {};
    Drupal.behaviors.cam3d_three.sgnodematerialcorecontrollers = Drupal.behaviors.cam3d_three.sgnodematerialcorecontrollers || {};
    Drupal.behaviors.cam3d_three.objectcollection = Drupal.behaviors.cam3d_three.objectcollection || {};
    Drupal.behaviors.cam3d_three.geometrycollection = Drupal.behaviors.cam3d_three.geometrycollection || {};
    Drupal.behaviors.cam3d_three.materialcollection = Drupal.behaviors.cam3d_three.materialcollection || {};

    var genericControllerPrototype = {
        'create' : function(data, callback) {
            console.log('Generic Object controller create!');
        },
        'read' : function(data, callback) {
            console.log('Generic Object controller read!');
        },
        'update' : function(data, callback) {
            console.log('Generic Object controller update!');
        },
        'delete' : function(data, callback) {
            console.log('Generic Object controller delete!');
        }
    };

    var is_defined = function(parameter) {
        return typeof parameter !== 'undefined' && parameter !== null;
    };
    
    var getKeys = function(obj){
       var keys = [];
       for(var key in obj){
         if (obj.hasOwnProperty(key)) {
           keys.push(key);
         }
       }
       return keys;
    };
    
    var generic_update_callback = function(data, update_callback) {

        // This function could be called in one of three ways - to update a mesh object, to update a piece of geometry or to update a material
        var update_key = {
            object_type : data.object_type,
            object_id : data.object_id
        };
        var update_data = $.extend({}, data);

        Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers[data.object_type].create(data, update_callback);

        var object_update_callback = function(data, callback) {
            //console.log('Backend returned this after update :');
            //console.log(data);
        };

        Drupal.behaviors.cam3d_three_editor.update_remote(update_data, object_update_callback, JSON.stringify(update_key));
    };

    Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers.view = {
        create : function(data, view_create_callback){
            var children = data.children;
            
            var view = new THREE.Object3D();
            view.parameters = data;
            view.object_type = 'view';
            view.name = data.name;
            
            if (children.length > 0) {
            
                var objects_loaded = function(objects_loaded) {
                    $.each(objects_loaded, function(index, object_loaded) {
                        view.add(object_loaded);
                    });
                    view_create_callback(view);
                };

                var view_default_object = {};
                var view_objects_to_load = [];

                $.each(children, function(index, child_object_index) {
                    var view_object_to_load = $.extend({ object_id : child_object_index}, view_default_object);
                    view_objects_to_load.push(view_object_to_load);
                });

                Drupal.behaviors.cam3d_three.read_remote_collection(view_objects_to_load, objects_loaded);
            }
            else {
                view_create_callback(view);
            }
        },
        update : generic_update_callback
    };


    $.each(Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers, function(key, controller){
        controller.prototype = genericControllerPrototype;
    });

}(jQuery));