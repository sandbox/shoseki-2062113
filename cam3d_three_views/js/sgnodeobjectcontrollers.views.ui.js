
(function ($) {

    // Create default object if not defined
    Drupal.behaviors.cam3d_three_editor = Drupal.behaviors.cam3d_three_editor || {};

    Drupal.behaviors.cam3d_three_editor.general_object_update_timers = Drupal.behaviors.cam3d_three_editor.general_object_update_timers || {};

    var is_defined = function(parameter) {
        return typeof parameter !== 'undefined' && parameter !== null;
    };

    Sidebar.Object3D.view = function ( editor, sidebar_object_signals, sidebar_object, parent_update ) {

        var ui_elements = $.extend(Drupal.behaviors.cam3d_three_editor.base_object_ui_elements(), {
            rotation : {
                label : new UI.Text( 'Rotation' ).setWidth( '90px' ),
                element : new UI.TextArea().setWidth( '150px' ).setHeight( '80px' )
            },
            scale : {
                label : new UI.Text( 'Scale' ).setWidth( '90px' ),
                element : new UI.TextArea().setWidth( '150px' ).setHeight( '80px' )
            },
            view_name : {
                label : new UI.Text( 'View Name' ).setWidth( '90px' ),
                element : new UI.Input().setWidth( '150px' ).setColor( '#444' ).setFontSize( '12px' )
            },
            view_display : {
                label : new UI.Text( 'View Display' ).setWidth( '90px' ),
                element : new UI.Input().setWidth( '150px' ).setColor( '#444' ).setFontSize( '12px' )
            },
            view_parameters : {
                label : new UI.Text( 'View Parameters' ).setWidth( '90px' ),
                element : new UI.TextArea().setWidth( '150px' ).setHeight( '80px' )
            },
            view_data : {
                label : new UI.Text( 'View Data' ).setWidth( '90px' ),
                element : new UI.TextArea().setWidth( '150px' ).setHeight( '80px' )
            }
        });
        
        ui_elements = Drupal.behaviors.cam3d_three_editor.attach_typed_value_functions(ui_elements, ['rotation', 'scale'], 'vector3');
        ui_elements = Drupal.behaviors.cam3d_three_editor.attach_typed_value_functions(ui_elements, ['view_parameters', 'view_data'], 'json');

        var update = Drupal.behaviors.cam3d_three_editor.general_object_update(editor, sidebar_object_signals, sidebar_object, ui_elements, parent_update);

        return Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer(ui_elements, update, new UI.Panel(), sidebar_object);
    };

}(jQuery));
