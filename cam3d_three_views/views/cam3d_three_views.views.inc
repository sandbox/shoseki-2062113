<?php

/**
 * Implements hook_views_plugins().
 */
function cam3d_three_views_views_plugins() {

  $cam3d_three_views_path = drupal_get_path('module', 'cam3d_three_views') . '/views';
  
  $additional_themes['cam3d_three_views_views_json_style_simple'] = 'style';
  
  $cam3d_three_plugin_types = array('object', 'geometry', 'material');
  
  $plugins = array(
    'module' => 'cam3d_three_views',
  );

  foreach($cam3d_three_plugin_types as $plugin_type) {

      $plugins['display']['cam3d_three_' . $plugin_type . '_views_json_display'] = array(
        'title' => t('CAM3D Three (' . $plugin_type . ') View Display'),
        'path' => $cam3d_three_views_path . '/plugins',
        'help' => t('Expose this view to CAM3D Three'),
        'handler' => 'cam3d_three_' . $plugin_type . '_views_plugin_display',
        'theme' => 'cam3d_three_views_views_json_display',
        'theme file' => 'cam3d_three_views_views_json_display.theme.inc',
        'theme path' => $cam3d_three_views_path . '/theme',
        'use ajax' => FALSE,
        'use pager' => TRUE,
        'accept attachments' => TRUE,
      );
  }

  return $plugins;
}

/**
 * Implements hook_views_data_alter().
 * This is where custom global handlers will be added to expose objects of every time for consumption.
 */
function cam3d_three_views_views_data_alter(&$data) {
  $data['cam3d_three']['table']['group'] = t('CAM3D Three');
  $data['cam3d_three']['table']['join'] = array(
    // #global is a special flag which let's a table appear all the time.
    '#global' => array(),
  );
  
  if (module_invoke('ctools', 'api_version', '1.7.1')) {
    $data['cam3d_three']['vec3f_expression'] = array(
      'title' => t('3D Vector'),
      'help' => t('Evaluates a 3 mathematical expressions and displays as an object with x, y, z properties.'),
      'field' => array(
        'handler' => 'cam3d_three_views_handler_field_vec3f',
        'float' => TRUE,
      ),
    );
  }
  
  return $data;
}