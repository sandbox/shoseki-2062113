<?php


/**
 * @file
 * Contains the media browser tab display plugin.
 */

class cam3d_three_object_views_plugin_display extends views_plugin_display {

  function option_definition() {
    $options = parent::option_definition();
    $controllers = cam3d_three_retrieve_sgnodeobjectcontrollers();
    $controller_keys = array_keys($controllers);
    $options['object_type'] = array(
      'default' => $controller_keys[0],
    );
    return $options;
  }
  function options_summary(&$categories, &$options) {

    // It is very important to call the parent function here:
    parent::options_summary($categories, $options);
    
    $categories['cam3d_three_object'] = array(
      'title' => t('CAM3D Three Object settings'),
      'column' => 'second',
      'build' => array(
        '#weight' => -10,
      ),
    );
    $options['object_type'] = array(
      'category' => 'cam3d_three_object',
      'title' => t('Object Type'),
      'value' => $this->get_option('object_type'),
    );
  }
  function options_form(&$form, &$form_state) {
    // It is very important to call the parent function here:
    parent::options_form($form, $form_state);

    switch ($form_state['section']) {
      case 'object_type':
          
        $form['#title'] .= t('The Object Type for this view');
        $controllers = cam3d_three_retrieve_sgnodeobjectcontrollers();
        $controller_keys = array_keys($controllers);
        $type_options = drupal_map_assoc($controller_keys);

        $form['object_type'] = array(
          '#type' => 'radios',
          '#title' => t('CAM3D Three Object Type'),
          '#options' => $type_options,
          '#default_value' => $this->options['object_type'],
          '#description' => t('The CAM3D Three Object Type used to derive default data and variables to which fields will be mapped/converted.')
        );
        break;
    }
  }
  function options_submit(&$form, &$form_state) {
    // It is very important to call the parent function here:
    parent::options_submit($form, $form_state);
    switch ($form_state['section']) {
      case 'object_type':
        $this->set_option('object_type', $form_state['values']['object_type']);
        break;
    }
  }
}
class cam3d_three_geometry_views_plugin_display extends views_plugin_display {

  function option_definition() {
    $options = parent::option_definition();
    $controllers = cam3d_three_retrieve_sgnodegeometrycorecontrollers();
    $controller_keys = array_keys($controllers);
    $options['geometry_type'] = array(
      'default' => $controller_keys[0],
    );
    return $options;
  }
  function options_summary(&$categories, &$options) {

    // It is very important to call the parent function here:
    parent::options_summary($categories, $options);
    
    $categories['cam3d_three_geometry'] = array(
      'title' => t('CAM3D Three Geometry settings'),
      'column' => 'second',
      'build' => array(
        '#weight' => -10,
      ),
    );
    $options['geometry_type'] = array(
      'category' => 'cam3d_three_geometry',
      'title' => t('Geometry Type'),
      'value' => $this->get_option('geometry_type'),
    );
  }
  function options_form(&$form, &$form_state) {
    // It is very important to call the parent function here:
    parent::options_form($form, $form_state);

    switch ($form_state['section']) {
      case 'geometry_type':
          
        $form['#title'] .= t('The Geometry Type for this view');
        $controllers = cam3d_three_retrieve_sgnodegeometrycorecontrollers();
        $controller_keys = array_keys($controllers);
        $type_options = drupal_map_assoc($controller_keys);

        $form['geometry_type'] = array(
          '#type' => 'radios',
          '#title' => t('CAM3D Three Geometry Type'),
          '#options' => $type_options,
          '#default_value' => $this->options['geometry_type'],
          '#description' => t('The CAM3D Three Geometry Type used to derive default data and variables to which fields will be mapped/converted.')
        );
        break;
    }
  }
  function options_submit(&$form, &$form_state) {
    // It is very important to call the parent function here:
    parent::options_submit($form, $form_state);
    switch ($form_state['section']) {
      case 'geometry_type':
        $this->set_option('geometry_type', $form_state['values']['geometry_type']);
        break;
    }
  }
}
class cam3d_three_material_views_plugin_display extends views_plugin_display {

  function option_definition() {
    $options = parent::option_definition();
    $controllers = cam3d_three_retrieve_sgnodematerialcorecontrollers();
    $controller_keys = array_keys($controllers);
    $options['material_type'] = array(
      'default' => $controller_keys[0],
    );
    return $options;
  }
  function options_summary(&$categories, &$options) {

    // It is very important to call the parent function here:
    parent::options_summary($categories, $options);
    
    $categories['cam3d_three_material'] = array(
      'title' => t('CAM3D Three Material settings'),
      'column' => 'second',
      'build' => array(
        '#weight' => -10,
      ),
    );
    $options['material_type'] = array(
      'category' => 'cam3d_three_material',
      'title' => t('Material Type'),
      'value' => $this->get_option('material_type'),
    );
  }
  function options_form(&$form, &$form_state) {
    // It is very important to call the parent function here:
    parent::options_form($form, $form_state);

    switch ($form_state['section']) {
      case 'material_type':
          
        $form['#title'] .= t('The Material Type for this view');
        $controllers = cam3d_three_retrieve_sgnodematerialcorecontrollers();
        $controller_keys = array_keys($controllers);
        $type_options = drupal_map_assoc($controller_keys);

        $form['material_type'] = array(
          '#type' => 'radios',
          '#title' => t('CAM3D Three Material Type'),
          '#options' => $type_options,
          '#default_value' => $this->options['material_type'],
          '#description' => t('The CAM3D Three Material Type used to derive default data and variables to which fields will be mapped/converted.')
        );
        break;
    }
  }
  function options_submit(&$form, &$form_state) {
    // It is very important to call the parent function here:
    parent::options_submit($form, $form_state);
    switch ($form_state['section']) {
      case 'material_type':
        $this->set_option('material_type', $form_state['values']['material_type']);
        break;
    }
  }
}