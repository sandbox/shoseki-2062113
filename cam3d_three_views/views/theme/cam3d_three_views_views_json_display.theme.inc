<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Preprocess the primary theme implementation for a view.
 */
function template_preprocess_cam3d_three_views_views_json_display(&$vars) {

  global $base_path;

  $view = $vars['view'];

  $vars['rows']       = (!empty($view->result) || $view->style_plugin->even_empty()) ? $view->style_plugin->render($view->result) : '';

  $vars['css_name']   = drupal_clean_css_identifier($view->name);
  $vars['name']       = $view->name;
  $vars['display_id'] = $view->current_display;

  // Basic classes
  $vars['css_class'] = '';

  $vars['classes_array'] = array();
  $vars['classes_array'][] = 'view';
  $vars['classes_array'][] = 'view-' . drupal_clean_css_identifier($vars['name']);
  $vars['classes_array'][] = 'view-id-' . $vars['name'];
  $vars['classes_array'][] = 'view-display-id-' . $vars['display_id'];

  $css_class = $view->display_handler->get_option('css_class');
  if (!empty($css_class)) {
    $vars['css_class'] = preg_replace('/[^a-zA-Z0-9- ]/', '-', $css_class);
    $vars['classes_array'][] = $vars['css_class'];
  }

  $empty = empty($vars['rows']);

  $vars['header'] = $view->display_handler->render_area('header', $empty);
  $vars['footer'] = $view->display_handler->render_area('footer', $empty);
  if ($empty) {
    $vars['empty'] = $view->display_handler->render_area('empty', $empty);
  }

  $vars['exposed']    = !empty($view->exposed_widgets) ? $view->exposed_widgets : '';
  $vars['more']       = $view->display_handler->render_more_link();
  $vars['feed_icon']  = !empty($view->feed_icon) ? $view->feed_icon : '';

  $vars['pager']      = '';

  // @todo: Figure out whether this belongs into views_ui_preprocess_views_view.
  // Render title for the admin preview.
  $vars['title'] = !empty($view->views_ui_context) ? filter_xss_admin($view->get_title()) : '';

  if ($view->display_handler->render_pager()) {
    $exposed_input = isset($view->exposed_raw_input) ? $view->exposed_raw_input : NULL;
    $vars['pager']      = $view->query->render_pager($exposed_input);
  }

  $vars['attachment_before'] = !empty($view->attachment_before) ? $view->attachment_before : '';
  $vars['attachment_after'] = !empty($view->attachment_after) ? $view->attachment_after : '';

  // Add contextual links to the view. We need to attach them to the dummy
  // $view_array variable, since contextual_preprocess() requires that they be
  // attached to an array (not an object) in order to process them. For our
  // purposes, it doesn't matter what we attach them to, since once they are
  // processed by contextual_preprocess() they will appear in the $title_suffix
  // variable (which we will then render in views-view.tpl.php).
  views_add_contextual_links($vars['view_array'], 'view', $view, $view->current_display);

  // Attachments are always updated with the outer view, never by themselves,
  // so they do not have dom ids.
  if (empty($view->is_attachment)) {
    // Our JavaScript needs to have some means to find the HTML belonging to this
    // view.
    //
    // It is true that the DIV wrapper has classes denoting the name of the view
    // and its display ID, but this is not enough to unequivocally match a view
    // with its HTML, because one view may appear several times on the page. So
    // we set up a hash with the current time, $dom_id, to issue a "unique" identifier for
    // each view. This identifier is written to both Drupal.settings and the DIV
    // wrapper.
    $vars['dom_id'] = $view->dom_id;
    $vars['classes_array'][] = 'view-dom-id-' . $vars['dom_id'];
  }

  // If using AJAX, send identifying data about this view.
  if ($view->use_ajax && empty($view->is_attachment) && empty($view->live_preview)) {
    $settings = array(
      'views' => array(
        'ajax_path' => url('views/ajax'),
        'ajaxViews' => array(
          'views_dom_id:' . $vars['dom_id'] => array(
            'view_name' => $view->name,
            'view_display_id' => $view->current_display,
            'view_args' => check_plain(implode('/', $view->args)),
            'view_path' => check_plain($_GET['q']),
            // Pass through URL to ensure we get e.g. language prefixes.
//            'view_base_path' => isset($view->display['page']) ? substr(url($view->display['page']->display_options['path']), strlen($base_path)) : '',
            'view_base_path' => $view->get_path(),
            'view_dom_id' => $vars['dom_id'],
            // To fit multiple views on a page, the programmer may have
            // overridden the display's pager_element.
            'pager_element' => isset($view->query->pager) ? $view->query->pager->get_pager_id() : 0,
          ),
        ),
      ),
    );

    drupal_add_js($settings, 'setting');
    views_add_js('ajax_view');
  }

  // If form fields were found in the View, reformat the View output as a form.
  if (views_view_has_form_elements($view)) {
    $output = !empty($vars['rows']) ? $vars['rows'] : $vars['empty'];
    $form = drupal_get_form(views_form_id($view), $view, $output);
    // The form is requesting that all non-essential views elements be hidden,
    // usually because the rendered step is not a view result.
    if ($form['show_view_elements']['#value'] == FALSE) {
      $vars['header'] = '';
      $vars['exposed'] = '';
      $vars['pager'] = '';
      $vars['footer'] = '';
      $vars['more'] = '';
      $vars['feed_icon'] = '';
    }
    $vars['rows'] = $form;
  }
}
