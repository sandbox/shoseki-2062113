<?php
/**
 * @file
 * fragment_shader_content_type.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function fragment_shader_content_type_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function fragment_shader_content_type_node_info() {
  $items = array(
    'fragment_shader' => array(
      'name' => t('Fragment Shader'),
      'base' => 'node_content',
      'description' => t('A fragment shader script'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
