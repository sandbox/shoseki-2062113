(function ($) {

  Drupal.behaviors.cam3d_three_jstree = {
    attach: function (context, settings) {

      $.each (settings.cam3d_three_jstree.roots, function(i, sgnodeid) {
        var selector = '.' + settings.cam3d_three_jstree.prefix + sgnodeid;
        var rootsgnodeid = '' + sgnodeid;
        $cam3d_three_blocks = $(selector);

        var startsWith = function(data, str){
            return data.lastIndexOf(str, 0) === 0;            
        };
        
        var ROOT = '#', SGNODE = 'sgnode', SGNODECORE = 'sgnodecore';

        var core_settings = {
          "animation" : 0,
          "check_callback" : false,
          "themes" : { "stripes" : false },
          'data' : {
            'url' : function (node) {
              console.log(node);

              if (startsWith(node.id, ROOT)) {
                  return '/cam3d_three_tree/' + rootsgnodeid + '/root';
              }
              if (startsWith(node.id, SGNODE)) {
                  return '/cam3d_three_tree/' + rootsgnodeid;
              }
              if (startsWith(node.id, SGNODECORE)) {
                  return false;
              }
            }
          }
        };
        var types = {
          "#" : {
            "valid_children" : ["root"]
          },
          "root" : {
            "icon" : "/static/3.0.0/assets/images/tree_icon.png",
            "valid_children" : ["sgnode", "sgnodecore"]
          },
          "sgnode" : {
            "valid_children" : ["sgnode", "sgnodecore"]
          },
          "sgnodecore" : {
            "icon" : "glyphicon glyphicon-file",
            "valid_children" : []
          }
        };
        
        $cam3d_three_blocks.jstree({'core' : core_settings, "types" : types});
        
      });
    }
  };
}(jQuery));