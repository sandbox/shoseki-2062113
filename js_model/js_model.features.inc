<?php
/**
 * @file
 * js_model.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function js_model_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function js_model_node_info() {
  $items = array(
    'js_model' => array(
      'name' => t('JS Model'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
