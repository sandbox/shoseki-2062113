<?php
/**
 * @file
 * js_model.features.field.inc
 */

/**
 * Implements hook_field_default_fields().
 */
function js_model_field_default_fields() {
  $fields = array();

  // Exported field: 'node-js_model-field_js_eval'.
  $fields['node-js_model-field_js_eval'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_js_eval',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => '0',
      'module' => 'text',
      'settings' => array(
        'profile2_private' => FALSE,
      ),
      'translatable' => '0',
      'type' => 'text_long',
    ),
    'field_instance' => array(
      'bundle' => 'js_model',
      'default_value' => array(
        0 => array(
          'value' => 'new THREE.PlaneGeometry( 2, 2 )',
        ),
      ),
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'cam3d_three',
          'settings' => array(
            'height' => '240',
            'width' => '320',
          ),
          'type' => 'cam3d_three_model_formatter',
          'weight' => '0',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_js_eval',
      'label' => 'JS Eval',
      'required' => 1,
      'settings' => array(
        'text_processing' => '0',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'rows' => '5',
        ),
        'type' => 'text_textarea',
        'weight' => '-4',
      ),
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('JS Eval');

  return $fields;
}
