/**
 * @author mrdoob / http://mrdoob.com/
 */

Sidebar.Script = function ( editor ) {

	var signals = editor.signals;

	var container = new UI.CollapsiblePanel();
	container.setCollapsed( editor.config.getKey( 'ui/sidebar/script/collapsed' ) );
	container.onCollapsedChange( function ( boolean ) {
	  editor.config.setKey( 'ui/sidebar/script/collapsed', boolean );
	} );
	container.setDisplay( 'none' );

	container.addStatic( new UI.Text( 'Script' ).setTextTransform( 'uppercase' ) );
	container.add( new UI.Break() );

	//
	var scriptsContainer = new UI.Panel();
	container.add( scriptsContainer );

	var newScript = new UI.Button( 'New' );
	newScript.onClick( function () {

		var script = { name: '', source: 'function update( event ) {}' };
                var object = editor.selected;
                if (object.parameters.scripts === undefined) object.parameters.scripts = [];
                object.parameters.scripts.push(script);
                signals.scriptAdded.dispatch( script );
	} );
	container.add( newScript );

	/*
	var loadScript = new UI.Button( 'Load' );
	loadScript.setMarginLeft( '4px' );
	container.add( loadScript );
	*/

	//
        var triggerDatabaseUpdate = function (data) {

            var process_object_function = function(controller_generated_object) {
                // This function will be called with the updated object, but with Gizmos, we don't actually use it
                // Or else the Gizmo will lose reference to the object that has been updated
                // This is different from the UI where each time the values are changed, the object itself is run through the constructor
                // And then replaces the object
                //console.log(controller_generated_object);
            };

            Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers[data.object_type].update(data, process_object_function);
        };
        
        var object;

	function update() {

		scriptsContainer.clear();

                object = editor.selected;

		var scripts = object.parameters.scripts;

		if ( scripts !== undefined ) {

			for ( var i = 0; i < scripts.length; i ++ ) {

				( function ( object, script, script_key ) {

					var name = new UI.Input( script.name ).setWidth( '130px' ).setFontSize( '12px' );
					name.onChange( function () {

						script.name = this.getValue();

						signals.scriptChanged.dispatch();
					} );
					scriptsContainer.add( name );

					var edit = new UI.Button( 'Edit' );
					edit.setMarginLeft( '4px' );
					edit.onClick( function () {

						signals.editScript.dispatch( object, script );

					} );
					scriptsContainer.add( edit );

					var remove = new UI.Button( 'Remove' );
					remove.setMarginLeft( '4px' );
					remove.onClick( function () {

						if ( confirm( 'Are you sure?' ) ) {

							editor.removeScript( editor.selected, script );

						}

					} );
					scriptsContainer.add( remove );
					
					scriptsContainer.add( new UI.Break() );

				} )( object, scripts[ i ] );

			}

		}

	}

	// signals
        
        signals.scriptChanged.add( function ( script ) {
            
            triggerDatabaseUpdate(object.parameters);
            
            /*
            
            var index = items.indexOf(script);

            if (index !== -1) {
                items[index] = 1010;
            }*/
            
        });
        
        //triggerDatabaseUpdate(object.parameters);

	signals.objectSelected.add( function ( object ) {

		if ( object !== null ) {

			container.setDisplay( 'block' );

			update();

		} else {

			container.setDisplay( 'none' );

		}

	} );

	signals.scriptAdded.add( update );
	signals.scriptRemoved.add( update );

	return container;

};
