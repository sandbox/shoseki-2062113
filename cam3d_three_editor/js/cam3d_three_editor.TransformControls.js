/**
 * @author arodic / https://github.com/arodic
 */
 /*jshint sub:true*/

(function () {
    
	THREE.TransformControls = function ( camera, domElement ) {

		// TODO: Make non-uniform scale and rotate play nice in hierarchies
		// TODO: ADD RXYZ contol

		THREE.Object3D.call( this );

		domElement = ( domElement !== undefined ) ? domElement : document;

		this.gizmo = {};
		this.gizmo["translate"] = new THREE.TransformGizmoTranslate();
		this.gizmo["rotate"] = new THREE.TransformGizmoRotate();
		this.gizmo["scale"] = new THREE.TransformGizmoScale();

		this.add(this.gizmo["translate"]);
		this.add(this.gizmo["rotate"]);
		this.add(this.gizmo["scale"]);

		this.gizmo["translate"].hide();
		this.gizmo["rotate"].hide();
		this.gizmo["scale"].hide();

		this.object = undefined;
		this.snap = null;
		this.space = "world";
		this.size = 1;
		this.axis = null;

		var scope = this;

		var _dragging = false;
		var _mode = "translate";
		var _plane = "XY";

		var changeEvent = { type: "change" };
		var mouseDownEvent = { type: "mouseDown" };
		var mouseUpEvent = { type: "mouseUp", mode: _mode };
		var objectChangeEvent = { type: "objectChange" };

		var ray = new THREE.Raycaster();
		var pointerVector = new THREE.Vector3();

		var point = new THREE.Vector3();
		var offset = new THREE.Vector3();

		var rotation = new THREE.Vector3();
		var offsetRotation = new THREE.Vector3();
		var scale = 1;

		var lookAtMatrix = new THREE.Matrix4();
		var eye = new THREE.Vector3();

		var tempMatrix = new THREE.Matrix4();
		var tempVector = new THREE.Vector3();
		var tempQuaternion = new THREE.Quaternion();
		var unitX = new THREE.Vector3( 1, 0, 0 );
		var unitY = new THREE.Vector3( 0, 1, 0 );
		var unitZ = new THREE.Vector3( 0, 0, 1 );

		var quaternionXYZ = new THREE.Quaternion();
		var quaternionX = new THREE.Quaternion();
		var quaternionY = new THREE.Quaternion();
		var quaternionZ = new THREE.Quaternion();
		var quaternionE = new THREE.Quaternion();

		var oldPosition = new THREE.Vector3();
		var oldScale = new THREE.Vector3();
		var oldRotationMatrix = new THREE.Matrix4();

		var parentRotationMatrix  = new THREE.Matrix4();
		var parentScale = new THREE.Vector3();

		var worldPosition = new THREE.Vector3();
		var worldRotation = new THREE.Euler();
		var worldRotationMatrix  = new THREE.Matrix4();
		var camPosition = new THREE.Vector3();
		var camRotation = new THREE.Euler();

		domElement.addEventListener( "mousedown", onPointerDown, false );
		domElement.addEventListener( "touchstart", onPointerDown, false );

		domElement.addEventListener( "mousemove", onPointerHover, false );
		domElement.addEventListener( "touchmove", onPointerHover, false );

		domElement.addEventListener( "mousemove", onPointerMove, false );
		domElement.addEventListener( "touchmove", onPointerMove, false );

		domElement.addEventListener( "mouseup", onPointerUp, false );
		domElement.addEventListener( "mouseout", onPointerUp, false );
		domElement.addEventListener( "touchend", onPointerUp, false );
		domElement.addEventListener( "touchcancel", onPointerUp, false );
		domElement.addEventListener( "touchleave", onPointerUp, false );

		this.attach = function ( object ) {

			scope.object = object;

			this.gizmo["translate"].hide();
			this.gizmo["rotate"].hide();
			this.gizmo["scale"].hide();
			this.gizmo[_mode].show();

			scope.update();

		};

		this.detach = function ( object ) {

			scope.object = undefined;
			this.axis = null;

			this.gizmo["translate"].hide();
			this.gizmo["rotate"].hide();
			this.gizmo["scale"].hide();

		};

		this.setMode = function ( mode ) {

			_mode = mode ? mode : _mode;

			if ( _mode == "scale" ) scope.space = "local";

			this.gizmo["translate"].hide();
			this.gizmo["rotate"].hide();
			this.gizmo["scale"].hide();
			this.gizmo[_mode].show();

			this.update();
			scope.dispatchEvent( changeEvent );

		};

		this.setSnap = function ( snap ) {

			scope.snap = snap;

		};

		this.setSize = function ( size ) {

			scope.size = size;
			this.update();
			scope.dispatchEvent( changeEvent );

		};

		this.setSpace = function ( space ) {

			scope.space = space;
			this.update();
			scope.dispatchEvent( changeEvent );

		};

		this.update = function () {

			if ( scope.object === undefined ) return;

			scope.object.updateMatrixWorld();
			worldPosition.setFromMatrixPosition( scope.object.matrixWorld );
			worldRotation.setFromRotationMatrix( tempMatrix.extractRotation( scope.object.matrixWorld ) );

			camera.updateMatrixWorld();
			camPosition.setFromMatrixPosition( camera.matrixWorld );
			camRotation.setFromRotationMatrix( tempMatrix.extractRotation( camera.matrixWorld ) );

			scale = worldPosition.distanceTo( camPosition ) / 6 * scope.size;
			this.position.copy( worldPosition );
			this.scale.set( scale, scale, scale );

			eye.copy( camPosition ).sub( worldPosition ).normalize();

			if ( scope.space == "local" )
				this.gizmo[_mode].update( worldRotation, eye );

			else if ( scope.space == "world" )
				this.gizmo[_mode].update( new THREE.Euler(), eye );

			this.gizmo[_mode].highlight( scope.axis );

		};

		function onPointerHover( event ) {

			if ( scope.object === undefined || _dragging === true ) return;

			event.preventDefault();

			var pointer = event.changedTouches ? event.changedTouches[ 0 ] : event;

			var intersect = intersectObjects( pointer, scope.gizmo[_mode].pickers.children );

			var axis = null;

			if ( intersect ) {

				axis = intersect.object.name;

			}

			if ( scope.axis !== axis ) {

				scope.axis = axis;
				scope.update();
				scope.dispatchEvent( changeEvent );

			}

		}

		function onPointerDown( event ) {

			if ( scope.object === undefined || _dragging === true ) return;

			event.preventDefault();
			event.stopPropagation();

			var pointer = event.changedTouches ? event.changedTouches[ 0 ] : event;

			if ( pointer.button === 0 || pointer.button === undefined ) {

				var intersect = intersectObjects( pointer, scope.gizmo[_mode].pickers.children );

				if ( intersect ) {

					scope.dispatchEvent( mouseDownEvent );

					scope.axis = intersect.object.name;

					scope.update();

					eye.copy( camPosition ).sub( worldPosition ).normalize();

					scope.gizmo[_mode].setActivePlane( scope.axis, eye );

					var planeIntersect = intersectObjects( pointer, [scope.gizmo[_mode].activePlane] );

					oldPosition.copy( scope.object.position );
					oldScale.copy( scope.object.scale );

					oldRotationMatrix.extractRotation( scope.object.matrix );
					worldRotationMatrix.extractRotation( scope.object.matrixWorld );

					parentRotationMatrix.extractRotation( scope.object.parent.matrixWorld );
					parentScale.setFromMatrixScale( tempMatrix.getInverse( scope.object.parent.matrixWorld ) );

					offset.copy( planeIntersect.point );

				}

			}

			_dragging = true;

		}

                function triggerDatabaseUpdate (data) {

                    var process_object_function = function(controller_generated_object) {
                        // This function will be called with the updated object, but with Gizmos, we don't actually use it
                        // Or else the Gizmo will lose reference to the object that has been updated
                        // This is different from the UI where each time the values are changed, the object itself is run through the constructor
                        // And then replaces the object
                        //console.log(controller_generated_object);
                    };

                    Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers[data.object_type].update(data, process_object_function);
                };

                function onPointerMove ( event ) {

                if ( scope.object === undefined || scope.axis === null || _dragging === false ) return;

                event.preventDefault();
                event.stopPropagation();

                var pointer = event.changedTouches? event.changedTouches[0] : event;

                var planeIntersect = intersectObjects( pointer, [scope.gizmo[_mode].activePlane] );

                point.copy( planeIntersect.point );

                if ( _mode == "translate" ) {

                        point.sub( offset );
                        point.multiply(parentScale);

                        if ( scope.space == "local" ) {

                                point.applyMatrix4( tempMatrix.getInverse( worldRotationMatrix ) );

                                if ( scope.axis.search("X") == -1 ) point.x = 0;
                                if ( scope.axis.search("Y") == -1 ) point.y = 0;
                                if ( scope.axis.search("Z") == -1 ) point.z = 0;

                                point.applyMatrix4( oldRotationMatrix );

                                scope.object.position.copy( oldPosition );
                                scope.object.position.add( point );

                        } 

                        if ( scope.space == "world" || scope.axis.search("XYZ") != -1 ) {

                                if ( scope.axis.search("X") == -1 ) point.x = 0;
                                if ( scope.axis.search("Y") == -1 ) point.y = 0;
                                if ( scope.axis.search("Z") == -1 ) point.z = 0;

                                point.applyMatrix4( tempMatrix.getInverse( parentRotationMatrix ) );

                                scope.object.position.copy( oldPosition );
                                scope.object.position.add( point );

                        }

                        if ( scope.snap !== null ) {

                                if ( scope.axis.search("X") != -1 ) scope.object.position.x = Math.round( scope.object.position.x / scope.snap ) * scope.snap;
                                if ( scope.axis.search("Y") != -1 ) scope.object.position.y = Math.round( scope.object.position.y / scope.snap ) * scope.snap;
                                if ( scope.axis.search("Z") != -1 ) scope.object.position.z = Math.round( scope.object.position.z / scope.snap ) * scope.snap;

                        }

                        scope.object.parameters.position = {x : scope.object.position.x, y : scope.object.position.y, z : scope.object.position.z};
                        triggerDatabaseUpdate(scope.object.parameters);

                } else if ( _mode == "scale" ) {

                        point.sub( offset );
                        point.multiply(parentScale);

                        if ( scope.space == "local" ) {

                                if ( scope.axis == "XYZ") {

                                        scale = 1 + ( ( point.y ) / 50 );

                                        scope.object.scale.x = oldScale.x * scale;
                                        scope.object.scale.y = oldScale.y * scale;
                                        scope.object.scale.z = oldScale.z * scale;

                                } else {

                                        point.applyMatrix4( tempMatrix.getInverse( worldRotationMatrix ) );

                                        if ( scope.axis == "X" ) scope.object.scale.x = oldScale.x * ( 1 + point.x / 50 );
                                        if ( scope.axis == "Y" ) scope.object.scale.y = oldScale.y * ( 1 + point.y / 50 );
                                        if ( scope.axis == "Z" ) scope.object.scale.z = oldScale.z * ( 1 + point.z / 50 );

                                }

                        }

                        scope.object.parameters.scale = {x : scope.object.scale.x, y : scope.object.scale.y, z : scope.object.scale.z};
                        triggerDatabaseUpdate(scope.object.parameters);

                } else if ( _mode == "rotate" ) {

                        point.sub( worldPosition );
                        point.multiply(parentScale);
                        tempVector.copy(offset).sub( worldPosition );
                        tempVector.multiply(parentScale);

                        if ( scope.axis == "E" ) {

                                point.applyMatrix4( tempMatrix.getInverse( lookAtMatrix ) );
                                tempVector.applyMatrix4( tempMatrix.getInverse( lookAtMatrix ) );

                                rotation.set( Math.atan2( point.z, point.y ), Math.atan2( point.x, point.z ), Math.atan2( point.y, point.x ) );
                                offsetRotation.set( Math.atan2( tempVector.z, tempVector.y ), Math.atan2( tempVector.x, tempVector.z ), Math.atan2( tempVector.y, tempVector.x ) );

                                tempQuaternion.setFromRotationMatrix( tempMatrix.getInverse( parentRotationMatrix ) );

                                quaternionE.setFromAxisAngle( eye, rotation.z - offsetRotation.z );
                                quaternionXYZ.setFromRotationMatrix( worldRotationMatrix );

                                tempQuaternion.multiplyQuaternions( tempQuaternion, quaternionE );
                                tempQuaternion.multiplyQuaternions( tempQuaternion, quaternionXYZ );

                                scope.object.quaternion.copy( tempQuaternion );

                        } else if ( scope.axis == "XYZE" ) {

                                quaternionE.setFromEuler( point.clone().cross(tempVector).normalize() ); // rotation axis

                                tempQuaternion.setFromRotationMatrix( tempMatrix.getInverse( parentRotationMatrix ) );
                                quaternionX.setFromAxisAngle( quaternionE, - point.clone().angleTo(tempVector) );
                                quaternionXYZ.setFromRotationMatrix( worldRotationMatrix );

                                tempQuaternion.multiplyQuaternions( tempQuaternion, quaternionX );
                                tempQuaternion.multiplyQuaternions( tempQuaternion, quaternionXYZ );

                                scope.object.quaternion.copy( tempQuaternion );

                        } else if ( scope.space == "local" ) {

                                point.applyMatrix4( tempMatrix.getInverse( worldRotationMatrix ) );

                                tempVector.applyMatrix4( tempMatrix.getInverse( worldRotationMatrix ) );

                                rotation.set( Math.atan2( point.z, point.y ), Math.atan2( point.x, point.z ), Math.atan2( point.y, point.x ) );
                                offsetRotation.set( Math.atan2( tempVector.z, tempVector.y ), Math.atan2( tempVector.x, tempVector.z ), Math.atan2( tempVector.y, tempVector.x ) );

                                quaternionXYZ.setFromRotationMatrix( oldRotationMatrix );
                                quaternionX.setFromAxisAngle( unitX, rotation.x - offsetRotation.x );
                                quaternionY.setFromAxisAngle( unitY, rotation.y - offsetRotation.y );
                                quaternionZ.setFromAxisAngle( unitZ, rotation.z - offsetRotation.z );

                                if ( scope.axis == "X" ) quaternionXYZ.multiplyQuaternions( quaternionXYZ, quaternionX );
                                if ( scope.axis == "Y" ) quaternionXYZ.multiplyQuaternions( quaternionXYZ, quaternionY );
                                if ( scope.axis == "Z" ) quaternionXYZ.multiplyQuaternions( quaternionXYZ, quaternionZ );

                                scope.object.quaternion.copy( quaternionXYZ );

                        } else if ( scope.space == "world" ) {

                                rotation.set( Math.atan2( point.z, point.y ), Math.atan2( point.x, point.z ), Math.atan2( point.y, point.x ) );
                                offsetRotation.set( Math.atan2( tempVector.z, tempVector.y ), Math.atan2( tempVector.x, tempVector.z ), Math.atan2( tempVector.y, tempVector.x ) );

                                tempQuaternion.setFromRotationMatrix( tempMatrix.getInverse( parentRotationMatrix ) );

                                quaternionX.setFromAxisAngle( unitX, rotation.x - offsetRotation.x );
                                quaternionY.setFromAxisAngle( unitY, rotation.y - offsetRotation.y );
                                quaternionZ.setFromAxisAngle( unitZ, rotation.z - offsetRotation.z );
                                quaternionXYZ.setFromRotationMatrix( worldRotationMatrix );

                                if ( scope.axis == "X" ) tempQuaternion.multiplyQuaternions( tempQuaternion, quaternionX );
                                if ( scope.axis == "Y" ) tempQuaternion.multiplyQuaternions( tempQuaternion, quaternionY );
                                if ( scope.axis == "Z" ) tempQuaternion.multiplyQuaternions( tempQuaternion, quaternionZ );

                                tempQuaternion.multiplyQuaternions( tempQuaternion, quaternionXYZ );

                                scope.object.quaternion.copy( tempQuaternion );

                        }
                        scope.object.parameters.rotation = {x : scope.object.rotation.x, y : scope.object.rotation.y, z : scope.object.rotation.z};
                        triggerDatabaseUpdate(scope.object.parameters);
                }

                scope.update();
                scope.dispatchEvent( changeEvent );

        };

		function onPointerUp( event ) {

			if ( _dragging && ( scope.axis !== null ) ) {
				mouseUpEvent.mode = _mode;
				scope.dispatchEvent( mouseUpEvent )
			}
			_dragging = false;
			onPointerHover( event );

		}

		function intersectObjects( pointer, objects ) {

			var rect = domElement.getBoundingClientRect();
			var x = ( pointer.clientX - rect.left ) / rect.width;
			var y = ( pointer.clientY - rect.top ) / rect.height;

			pointerVector.set( ( x * 2 ) - 1, - ( y * 2 ) + 1, 0.5 );
			pointerVector.unproject( camera );

			ray.set( camPosition, pointerVector.sub( camPosition ).normalize() );

			var intersections = ray.intersectObjects( objects, true );
			return intersections[0] ? intersections[0] : false;

		}

	};
	THREE.TransformControls.prototype = Object.create( THREE.Object3D.prototype );
	THREE.TransformControls.prototype.constructor = THREE.TransformControls;
	
}());