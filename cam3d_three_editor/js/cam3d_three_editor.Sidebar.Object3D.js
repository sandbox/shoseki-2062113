Sidebar.Object3D = function ( editor ) {
    
    var signals = editor.signals;

    var container = new UI.CollapsiblePanel();
    container.setDisplay( 'none' );

    // Object type
    var objectType = new UI.Text().setTextTransform( 'uppercase' );
    container.addStatic( objectType );
    container.add( new UI.Break() );

    // Object name
    /*var objectNameRow = new UI.Panel();
    var objectName = new UI.Input().setWidth( '150px' ).setColor( '#444' ).setFontSize( '12px' ).onChange( function () {
        editor.setObjectName( editor.selected, objectName.getValue() );
    } );
    objectNameRow.add( new UI.Text( 'Name' ).setWidth( '90px' ) );
    objectNameRow.add( objectName );
    container.add( objectNameRow );*/
    
    var is_defined = function(parameter) {
      return typeof parameter !== 'undefined' && parameter !== null;
    };

    // Add object type specific stuff
    // parameters
    var parameters;
    var build = function () {
        
        // Remove parameters if already there
        if ( is_defined(parameters) ) {
            container.remove( parameters );
            objectType.setValue( '' );
            parameters = null;
            container.setDisplay( 'none' );
        }
        
        var object = editor.selected;
        
        if (object) {

            objectType.setValue( object.type );
            container.setDisplay( 'block' );
            
            var parent_update = function(object) {
                return function() {
                  // console.log(object);
                };
            }(object);

            var object_type = object.object_type;
            if ( typeof object_type !== 'undefined' && typeof Sidebar.Object3D[object_type] !== 'undefined') {
                parameters = new Sidebar.Object3D[object_type](editor, signals, object, parent_update );
                container.add( parameters );
            }
            else {
                console.log(object);
                console.log(object_type);
                console.log(typeof Sidebar.Object3D[object_type]);
                console.log();
            }
        }
    };
    
    signals.objectSelected.add( build );
    signals.objectChanged.add( build );

    return container;
};