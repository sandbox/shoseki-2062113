Sidebar.Material = function ( editor, object_update ) {

    var signals = editor.signals;

    var container = new UI.CollapsiblePanel();
    container.setDisplay( 'none' );
    
    // type
    var materialType = new UI.Text().setTextTransform( 'uppercase' );
    container.addStatic( materialType );
    container.add( new UI.Break() );

    // uuid
    var materialUUIDRow = new UI.Panel();
    var materialUUID = new UI.Input().setWidth( '115px' ).setColor( '#444' ).setFontSize( '12px' ).setDisabled( true );
    var materialUUIDRenew = new UI.Button( '⟳' ).setMarginLeft( '7px' ).onClick( function () {
        materialUUID.setValue( THREE.Math.generateUUID() );
        editor.selected.material.uuid = materialUUID.getValue();
    } );
    materialUUIDRow.add( new UI.Text( 'UUID' ).setWidth( '90px' ) );
    materialUUIDRow.add( materialUUID );
    materialUUIDRow.add( materialUUIDRenew );
    container.add( materialUUIDRow );
    
    // name
    var materialNameRow = new UI.Panel();
    var materialName = new UI.Input().setWidth( '150px' ).setColor( '#444' ).setFontSize( '12px' ).onChange( function () {
        editor.setMaterialName( editor.selected.material, materialName.getValue() );
    } );
    materialNameRow.add( new UI.Text( 'Name' ).setWidth( '90px' ) );
    materialNameRow.add( materialName );
    container.add( materialNameRow );

    // parameters
    var parameters;
    var build = function () {
        
        var object = editor.selected;
        
        if ( object && object.geometry ) {

            var geometry = object.geometry;
            
            var objectHasUvs = false;

            if ( object instanceof THREE.Sprite ) objectHasUvs = true;
            if ( geometry instanceof THREE.Geometry && geometry.faceVertexUvs[ 0 ].length > 0 ) objectHasUvs = true;
            if ( geometry instanceof THREE.BufferGeometry && geometry.attributes.uv !== undefined ) objectHasUvs = true;
            
            var material = object.material;
            
            var parent_update = function(geometry, material) {
                return function() {
                    material.needsUpdate = true;
                    geometry.buffersNeedUpdate = true;
                    geometry.uvsNeedUpdate = true;
                };
            }(geometry, material);

            container.setDisplay( 'block' );
            materialType.setValue( material.type );
            materialUUID.setValue( material.uuid );
            materialName.setValue( material.name );

            // Remove parameters if already there
            if ( parameters !== undefined ) {
                container.remove( parameters );
                parameters = undefined;
            }

            var material_type = material.material_type;

            if ( typeof material_type !== 'undefined' && typeof Sidebar.Material[material_type] !== 'undefined') {
                parameters = new Sidebar.Material[material_type](signals, object, parent_update );
                container.add( parameters );
            }
            else {
                //console.log(material_type + ' undefined');
            }
        }
        else {
            container.setDisplay( 'none' );
        }
    };
    
    signals.objectSelected.add( build );
    signals.objectChanged.add( build );
    
    return container;
};