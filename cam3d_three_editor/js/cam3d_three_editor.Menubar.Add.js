(function ($) {

    // Store our function as a property of Drupal.behaviors.
    Drupal.behaviors.cam3d_three_editor_menu_add = {

      loaded_controller_uris : [],

      attach: function (context, settings) {

        $('body').once(function(){

          Menubar.Add = function ( editor ) {

            var add_to_editor = function(object) {
              editor.addObject(object);
              editor.select(object);
            };

            var container = new UI.Panel();
            container.setClass( 'menu' );

            var title = new UI.Panel();
            title.setTextContent( 'Add' );
            title.setMargin( '0px' );
            title.setPadding( '8px' );
            container.add( title );

            //
            var options = new UI.Panel();
            options.setClass( 'options' );
            container.add( options );

            var meshCount = 0;
            var lightCount = 0;
            
            var capitaliseFirstLetter = function (string) {
              return string.charAt(0).toUpperCase() + string.slice(1);
            };
            
            /*var add_options = {
                Mesh : [
                    {object_type:'mesh', geometry_type : 'plane', title : 'Plane'},
                    {object_type:'mesh', geometry_type : 'box', title : 'Box'},
                    {object_type:'mesh', geometry_type : 'circle', title : 'Circle'},
                    {object_type:'mesh', geometry_type : 'cylinder', title : 'Cylinder'},
                    {object_type:'mesh', geometry_type : 'sphere', title : 'Sphere'},
                    {object_type:'mesh', geometry_type : 'icosahedron', title : 'Icosahedron'},
                    {object_type:'mesh', geometry_type : 'torus', title : 'Torus'},
                    {object_type:'mesh', geometry_type : 'torusknot', title : 'Torusknot'},
                    {object_type:'mesh', geometry_type : 'text', title : 'Text'}                 
                ],
                Lights : [
                    {object_type:'pointlight', title : 'Pointlight'},
                    {object_type:'spotlight', title : 'Spotlight'},
                    {object_type:'directionallight', title : 'Directionallight'},
                    {object_type:'hemispherelight', title : 'Hemispherelight'},
                    {object_type:'ambientlight', title : 'Ambientlight'}
                ],
                Other : [
                    {object_type:'sprite', title : 'Sprite'},
                    {object_type:'perspectivecamera', title : 'PerspectiveCamera'}
                ]
            };*/
            
            var add_options = Drupal.settings.cam3d_three_editor.add_menu_objects;
            
            var generateObjectGenerationOption = function(objectPlusTitle) {
                
                var objectTitle = objectPlusTitle.title;
                delete objectPlusTitle.title;
                
                var option = new UI.Panel();
                option.setClass( 'option' );
                option.setTextContent( objectTitle );
                option.onClick( function () {
                  Drupal.behaviors.cam3d_three_editor.create_remote(objectPlusTitle, add_to_editor);
                });
                return option;
            };
            
            $.each(add_options, function(category, items) {

                //var category_option = new UI.Panel();
                //category_option.setClass( 'option' );
                //category_option.setTextContent( category );
                //options.add(category_option);
                
                $.each(items, function (index, objectPlusTitle) {
                    var objectPlusTitleClone = $.extend({}, objectPlusTitle);
                    var option = generateObjectGenerationOption(objectPlusTitleClone);
                    options.add( option );
                });
                
                options.add(new UI.HorizontalRule());
            });

            return container;
          };
        });
      }
    };
}(jQuery));