UI.CAM3DTexture = function (value) {

	UI.Element.call( this );

	var scope = this;

	var dom = document.createElement( 'span' );

        this.selectedFile = undefined;

        this.processFiles = function (mediaFiles){

            if (mediaFiles.length < 1) return;

            scope.selectedFile = mediaFiles[0];
            scope.processFile(scope.selectedFile.url, true);
        };
        this.processFile = function (url, fromDialog) {

            if (typeof fromDialog === undefined) fromDialog = false;

            var image = document.createElement( 'img' );
            image.addEventListener( 'load', function( event ) {

                var texture = new THREE.Texture( this );
                //texture.sourceFile = scope.selectedFile.filename;
                texture.sourceFile = url;
                texture.needsUpdate = true;
                scope.setTextureValue(texture);

                if (fromDialog && scope.onChangeCallback) {
                    scope.onChangeCallback();
                }

            }, false );

            image.src = url;
        };
        this.getValue = function () {
            if (typeof scope.selectedFile !== 'undefined') return scope.selectedFile;
        };
        this.setValue = function (file) {

            if (typeof file === 'undefined' || file === null) return;

            // At the moment, setter does nothing!
            scope.selectedFile = file;
            scope.processFile(scope.selectedFile.url, false);
        };
        this.onChange = function ( callback ) {
            scope.onChangeCallback = callback;
            return scope;
        };
        this.setTextureValue = function ( texture ) {

            var canvas = this.canvas;
            var name = this.name;
            var context = canvas.getContext( '2d' );

            if ( typeof texture !== 'undefined' || texture !== null ) {

                var image = texture.image;

                if ( image.width > 0 ) {

                    name.value = texture.sourceFile;

                    var scale = canvas.width / image.width;
                    context.drawImage( image, 0, 0, image.width * scale, image.height * scale );
                }
                else {
                    name.value = texture.sourceFile + ' (error)';
                    context.clearRect( 0, 0, canvas.width, canvas.height );
                }
            }
            else {
                name.value = '';
                context.clearRect( 0, 0, canvas.width, canvas.height );
            }

            scope.texture = texture;
        };
        this.getTextureValue = function () {
            return scope.texture;
        };
        var triggerPopUp = function () {
            Drupal.media.popups.mediaBrowser(scope.processFiles);
        };

	var canvas = document.createElement( 'canvas' );
	canvas.width = 32;
	canvas.height = 16;
	canvas.style.cursor = 'pointer';
	canvas.style.marginRight = '5px';
	canvas.style.border = '1px solid #888';
	canvas.addEventListener('click', triggerPopUp, false);
	canvas.addEventListener('drop', function (event) {
            event.preventDefault();
            event.stopPropagation();
	}, false );
	dom.appendChild(canvas);
        this.canvas = canvas;

	var name = document.createElement('input');
	name.disabled = true;
	name.style.width = '64px';
	name.style.border = '1px solid #ccc';
	name.addEventListener('click', triggerPopUp, false);
	dom.appendChild(name);
        this.name = name;

	this.dom = dom;
	this.texture = null;
	this.onChangeCallback = null;

	return this;
};

UI.CAM3DTexture.prototype = Object.create( UI.Element.prototype );