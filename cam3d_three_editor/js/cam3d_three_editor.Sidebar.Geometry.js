Sidebar.Geometry = function ( editor ) {

    var signals = editor.signals;

    var container = new UI.CollapsiblePanel();
    container.setDisplay( 'none' );
    
    container.addStatic( new UI.Text().setValue( 'GEOMETRY' ) );
    container.add( new UI.Break() );

    // uuid
    var geometryUUIDRow = new UI.Panel();
    var geometryUUID = new UI.Input().setWidth( '115px' ).setColor( '#444' ).setFontSize( '12px' ).setDisabled( true );
    var geometryUUIDRenew = new UI.Button( '⟳' ).setMarginLeft( '7px' ).onClick( function () {
        geometryUUID.setValue( THREE.Math.generateUUID() );
        editor.selected.geometry.uuid = geometryUUID.getValue();
    } );
    geometryUUIDRow.add( new UI.Text( 'UUID' ).setWidth( '90px' ) );
    geometryUUIDRow.add( geometryUUID );
    geometryUUIDRow.add( geometryUUIDRenew );
    container.add( geometryUUIDRow );

    // name
    var geometryNameRow = new UI.Panel();
    var geometryName = new UI.Input().setWidth( '150px' ).setColor( '#444' ).setFontSize( '12px' ).onChange( function () {
            editor.setGeometryName( editor.selected.geometry, geometryName.getValue() );
    } );
    geometryNameRow.add( new UI.Text( 'Name' ).setWidth( '90px' ) );
    geometryNameRow.add( geometryName );
    container.add( geometryNameRow );

    // type
    var geometryType = new UI.Text().setTextTransform( 'uppercase' );
    container.addStatic( geometryType );
    container.add( new UI.Break() );

    // geometry
    //container.add( new Sidebar.Geometry.Geometry( signals ) );

    // buffergeometry
    //container.add( new Sidebar.Geometry.BufferGeometry( signals ) );

    // parameters
    var parameters;

    //
    var build = function () {

        var object = editor.selected;

        if ( object && object.geometry ) {

            var geometry = object.geometry;
            container.setDisplay( 'block' );
            geometryType.setValue( geometry.type );
            geometryUUID.setValue( geometry.uuid );
            geometryName.setValue( geometry.name );

            //
            if ( parameters !== undefined ) {
                container.remove( parameters );
                parameters = undefined;
            }

            var geometry_type = geometry.geometry_type;
            if ( geometry_type !== undefined && Sidebar.Geometry[geometry_type] !== undefined) {
                parameters = new Sidebar.Geometry[geometry_type](signals, object);
                container.add( parameters );
            }
        }
        else {
            container.setDisplay( 'none' );
        }
    };

    signals.objectSelected.add( build );
    signals.objectChanged.add( build );

    return container;
};