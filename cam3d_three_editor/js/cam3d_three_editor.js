(function ($) {

    Number.prototype.format = function (){
      return this.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
    };
    
    // We override request_controller in cam3d_three to set controller_data.ui = true, otherwise identical
    Drupal.behaviors.cam3d_three.request_controller = function(function_name, object_data, request_controller_callback, callback_immediately) {

      if (!Drupal.behaviors.cam3d_three.is_defined(callback_immediately)) var callback_immediately = false;

      var controller_data = $.extend({}, object_data);
      controller_data.ui = true;
      var data_json = JSON.stringify(controller_data);

      var request = $.ajax({
        type: 'POST',
        url: this.controllers_url,
        data : {data:data_json},
        success : (function( data ) {
          if (data.files && data.files.length > 0) {
            Drupal.behaviors.cam3d_three.load_controllers(data.files, function_name, object_data, request_controller_callback, callback_immediately);
          }
        }),
        fail : (function( jqXHR, textStatus ) {
          alert( "Request failed: " + textStatus );
        })
      });
    };

    // Store our function as a property of Drupal.behaviors.
    Drupal.behaviors.cam3d_three_editor = {

      request_objects : {},

      attach: function (context, settings) {

        this.base_root = Drupal.settings.cam3d_three_editor.base_root;
        this.api_root = this.base_root + '/' + Drupal.settings.cam3d_three_editor.api_root;
        
        this.create_url = this.api_root + 'sgnodeobject/create';
        this.update_url = this.api_root + 'sgnodeobject/update';
        this.delete_url = this.api_root + 'sgnodeobject/delete';
        
        window.URL = window.URL || window.webkitURL;

        var $cam3dThreeEditorDiv = $("#cam3d-three-editor");
        
        $cam3dThreeEditorDiv.once('cam3d-three-editor-once', function(){

            var editor = new Editor();
            editor.setTheme(settings.cam3d_three_editor.theme);
            
            var viewport = new Viewport( editor ).setId( 'viewport' );
            $cam3dThreeEditorDiv.append( $(viewport.dom) );
            
            var script = new Script( editor );
            $cam3dThreeEditorDiv.append( $(script.dom) );
            
            var menubar = new Menubar( editor ).setId( 'menubar' );
            $cam3dThreeEditorDiv.append( $(menubar.dom) );
            
            var toolbar = new Toolbar( editor ).setId( 'toolbar' );

            var $toolbarDom = $(toolbar.dom);

            $cam3dThreeEditorDiv.append( $toolbarDom );

            var sidebar = new Sidebar( editor ).setId( 'sidebar' );
            $cam3dThreeEditorDiv.append( $(sidebar.dom) );
            
            $cam3dThreeEditorDiv.bind( 'dragover', function ( event ) {
              event.stopImmediatePropagation();
              event.dataTransfer.dropEffect = 'copy';
            }, false );

            $cam3dThreeEditorDiv.bind( 'drop', function ( event ) {
              event.stopImmediatePropagation();
              editor.loader.loadFile( event.dataTransfer.files[ 0 ] );
            }, false );

            $cam3dThreeEditorDiv.bind( 'keydown', function ( event ) {
              switch ( event.keyCode ) {
                case 8: // prevent browser back 
                  event.preventDefault();
                  break;
                case 46: // delete
                  editor.removeObject( editor.selected );
                  editor.deselect();
                  break;
              }
            }, false );

            var onWindowResize = function ( event ) {

              editor.signals.windowResize.dispatch();

              var screenHeight = $(window).height();
              var offset = $cam3dThreeEditorDiv.offset();

              var toolbarHeight = $toolbarDom.height() + 8;

              var calculatedHeight = Math.round(screenHeight - offset.top - toolbarHeight);

              $cam3dThreeEditorDiv.css({
                'height' : calculatedHeight
              });
            };

            $(window).bind( 'resize', onWindowResize);

            $(window).trigger( 'resize');
            
            var scene_id = Drupal.settings.cam3d_three_editor.scene_to_be_loaded_id;

            var scene_object = {object_id:scene_id};
            var process_scene_callback = function(scene_loaded) {
              editor.setScene(scene_loaded);
              Drupal.settings.cam3d_three_editor.scene_to_be_loaded_id = scene_loaded.parameters.object_id;
              Drupal.behaviors.cam3d_three_editor.scene = scene_loaded;
            };
            Drupal.behaviors.cam3d_three.read_remote(scene_object, process_scene_callback);
        });
      }
    };
    
    Drupal.behaviors.cam3d_three_editor.create_remote = function(object, create_remote_callback) {
      // Need to intercept and add the parent of the object as the scene itself, this can later be updated
      if (Drupal.behaviors.cam3d_three.is_defined(Drupal.behaviors.cam3d_three_editor.scene)) object.parent = Drupal.behaviors.cam3d_three_editor.scene.parameters.object_id;
      var data_json = JSON.stringify(object);

      var request = $.ajax({
        type: 'POST',
        url: Drupal.behaviors.cam3d_three_editor.create_url,
        data: {data:data_json},
        success : (function( data ) {
          Drupal.behaviors.cam3d_three.process_create(data, create_remote_callback);
        }),
        fail : (function( jqXHR, textStatus ) {
          alert( "Request failed: " + textStatus );
        })
      });
    };
    Drupal.behaviors.cam3d_three_editor.delete_remote = function(object, delete_remote_callback) {
      var data_json = JSON.stringify(object);

      var request = $.ajax({
        type: 'POST',
        url: Drupal.behaviors.cam3d_three_editor.delete_url,
        data: {data:data_json},
        success : (function( data ) {
            delete_remote_callback(data);
        }),
        fail : (function( jqXHR, textStatus ) {
          alert( "Request failed: " + textStatus );
        })
      });
    };    
    Drupal.behaviors.cam3d_three_editor.update_remote = function(object, update_remote_callback, update_key) {

      // Need to intercept and add the parent of the object as the scene itself, this can later be updated
      var data_json = JSON.stringify(object);

      // Set a default object if not defined before
      if (!Drupal.behaviors.cam3d_three.is_defined(Drupal.behaviors.cam3d_three_editor.request_objects[update_key])) Drupal.behaviors.cam3d_three_editor.request_objects[update_key] = {};

      // If timeout already set, clear it, prevents callback
      if (Drupal.behaviors.cam3d_three.is_defined(Drupal.behaviors.cam3d_three_editor.request_objects[update_key].timeout)) clearTimeout(Drupal.behaviors.cam3d_three_editor.request_objects[update_key].timeout);

      // If xhr already started, abort it
      if (Drupal.behaviors.cam3d_three.is_defined(Drupal.behaviors.cam3d_three_editor.request_objects[update_key].xhr)) Drupal.behaviors.cam3d_three_editor.request_objects[update_key].xhr.abort();

      var update_unique = Math.random();
      Drupal.behaviors.cam3d_three_editor.request_objects[update_key].update_unique = update_unique;

      var timeoutCallback = function(temp_update_key, temp_update_unique) {
        return function() {
          if (Drupal.behaviors.cam3d_three_editor.request_objects[temp_update_key].update_unique === temp_update_unique) {
            Drupal.behaviors.cam3d_three_editor.request_objects[temp_update_key].xhr = $.ajax({
              type: 'POST',
              url: Drupal.behaviors.cam3d_three_editor.update_url,
              data: {data:data_json},
              success : (function( data ) {
                Drupal.behaviors.cam3d_three.process_create(data, update_remote_callback);
              }),
              fail : (function( jqXHR, textStatus ) {
                alert( "Request failed: " + textStatus );
              })
            });
          };
        };
      }(update_key, update_unique);

      Drupal.behaviors.cam3d_three_editor.request_objects[update_key].timeout = setTimeout(timeoutCallback, 500);
    };

    // Create get typed value functions (functions that return the value already processed to a type)
    Drupal.behaviors.cam3d_three_editor.typed_get_value_functions = Drupal.behaviors.cam3d_three_editor.type_get_value_functions || {};    
    
    Drupal.behaviors.cam3d_three_editor.typed_get_value_functions.get_int_value = function() {
      return parseInt(this.getValue());
    };
    Drupal.behaviors.cam3d_three_editor.typed_get_value_functions.get_float_value = function() {
      return parseFloat(this.getValue());
    };
    Drupal.behaviors.cam3d_three_editor.typed_get_value_functions.get_json_value = function() {
      return JSON.parse(this.getValue());
    };
    Drupal.behaviors.cam3d_three_editor.typed_get_value_functions.get_vector3_value = function() {
      return JSON.parse(this.getValue());
    };
    Drupal.behaviors.cam3d_three_editor.typed_get_value_functions.get_string_value = function() {
      return this.getValue();
    };
    Drupal.behaviors.cam3d_three_editor.typed_get_value_functions.get_hex_alias_value = function() {
      return this.getHexValue();
    };

    // Create set typed value functions (functions that call set type after processing from a type)
    Drupal.behaviors.cam3d_three_editor.typed_set_value_functions = Drupal.behaviors.cam3d_three_editor.type_set_value_functions || {};
    Drupal.behaviors.cam3d_three_editor.typed_set_value_functions.set_int_value = function(value) {
      this.setValue('' + value);
    };
    Drupal.behaviors.cam3d_three_editor.typed_set_value_functions.set_float_value = function(value) {
      this.setValue('' + value);
    };
    Drupal.behaviors.cam3d_three_editor.typed_set_value_functions.set_json_value = function(value) {
      this.setValue(JSON.stringify(value));
    };
    Drupal.behaviors.cam3d_three_editor.typed_set_value_functions.set_vector3_value = function(value) {
      var object = {x : value.x, y : value.y, z : value.z};
      this.setValue(JSON.stringify(object));
    };
    Drupal.behaviors.cam3d_three_editor.typed_set_value_functions.set_string_value = function(value) {
      this.setValue(value);
    };
    Drupal.behaviors.cam3d_three_editor.typed_set_value_functions.set_hex_alias_value = function(value) {
      this.setHexValue(value);
    };
    
    // Attach typed values to multiple ui elements
    Drupal.behaviors.cam3d_three_editor.attach_typed_value_functions = function(ui_elements, ui_elements_keys, value_type) {

      var get_function_name = 'get_' + value_type + '_value';
      var set_function_name = 'set_' + value_type + '_value';

      $.each(ui_elements_keys, function(i, v) {
        if (typeof ui_elements[v] !== 'undefined') {
          if (typeof Drupal.behaviors.cam3d_three_editor.typed_get_value_functions[get_function_name] == "function") {
            ui_elements[v].element.getTypedValue = Drupal.behaviors.cam3d_three_editor.typed_get_value_functions[get_function_name];
          }
          else console.log('Unable to bind ' + get_function_name + ' as getTypedValue function.');
          
          if (typeof Drupal.behaviors.cam3d_three_editor.typed_set_value_functions[set_function_name] == "function") {
            ui_elements[v].element.setTypedValue = Drupal.behaviors.cam3d_three_editor.typed_set_value_functions[set_function_name];
          }
          else console.log('Unable to bind ' + set_function_name + ' as setTypedValue function.');
        }
      });
      return ui_elements;
    };

    Drupal.behaviors.cam3d_three_editor.createSubPanel = function(label, element) {
      var subPanel = new UI.Panel();
      subPanel.add(label);
      subPanel.add(element);
      return subPanel;
    };

    Drupal.behaviors.cam3d_three_editor.bindSubjectUpdatesValuesToUIElementsAndAddToContainer = function(ui_elements, update, container, subject) {

      // Go through each element, attach the update function to the onChange event, add a generated subpanel to the container and attach typed functions where necessary
      $.each(ui_elements, function(index) {
        ui_elements[index].element.onChange( update );
        container.add( Drupal.behaviors.cam3d_three_editor.createSubPanel( ui_elements[index].label, ui_elements[index].element));

        // If no typed value functions are not available, set them to simply alias the default get/set value
        if (ui_elements[index].element.getTypedValue == undefined) ui_elements[index].element.getTypedValue = ui_elements[index].element.getValue;
        if (ui_elements[index].element.setTypedValue == undefined) ui_elements[index].element.setTypedValue = ui_elements[index].element.setValue;

        if (subject.parameters[index] != undefined) {
          ui_elements[index].element.setTypedValue(subject.parameters[index]);
        }
      });

      return container;
    };
}(jQuery));