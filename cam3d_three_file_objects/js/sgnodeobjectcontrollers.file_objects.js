
(function ($) {

    Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers = Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers || {};

    var genericControllerPrototype = {
        'create' : function(data, callback) {
            console.log('Generic Object controller create!');
        },
        'read' : function(data, callback) {
            console.log('Generic Object controller read!');
        },
        'update' : function(data, callback) {
            console.log('Generic Object controller update!');
        },
        'delete' : function(data, callback) {
            console.log('Generic Object controller delete!');
        }
    };
    
    var endsWith = function(string, suffix) {
        return string.indexOf(suffix, this.length - suffix.length) !== -1;
    };

    var is_defined = function(parameter) {
        return typeof parameter !== 'undefined' && parameter !== null;
    };
    
    var getKeys = function(obj){
       var keys = [];
       for(var key in obj){
         if (obj.hasOwnProperty(key)) {
           keys.push(key);
         }
       }
       return keys;
    };
    
    Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers.file_obj = {
        create : function(file_data, file_create_callback){
            
            //var file_contents = file_data.file.file_contents;
            var file_url = file_data.file.url;

            if (!is_defined(file_data.position)) file_data.position = {x : 0.0, y : 0.0, z : 0.0};
            if (!is_defined(file_data.rotation)) file_data.rotation = {x : 0.0, y : 0.0, z : 0.0};
            if (!is_defined(file_data.scale)) file_data.scale = {x : 1.0, y : 1.0, z : 1.0};
            if (!is_defined(file_data.name)) file_data.name = 'File OBJ ' + ('id' + file_data.object_id);
            
            var process_object_callback = function(object) {
                object.parameters = file_data;
                object.object_type = 'file_obj';
                object.position.set(file_data.position.x, file_data.position.y, file_data.position.z);
                object.rotation.set(file_data.rotation.x, file_data.rotation.y, file_data.rotation.z);
                object.scale.set(file_data.scale.x, file_data.scale.y, file_data.scale.z);
                object.name = file_data.name;
                file_create_callback(object);
            };

            var load_object_callback = function() {
                var loader = new THREE.OBJLoader();
                loader.load( file_url, process_object_callback);
            }; 

            if (is_defined(THREE.OBJLoader)) {
                load_object_callback();
            }
            else {
                // Such a weird situation
                // We need to trigger the load controllers for the piece of geometry
                // Except that usually this happens at the object level, not the sub object (geomtry) level
                // So we need to repackage the geometry inside parent object to retrieve the controllers
                var packaged_object = $.extend({}, file_data);
                Drupal.behaviors.cam3d_three.request_controller(null, packaged_object, load_object_callback, true);
            }
        },
        update : function(data, update_callback) {

            var update_key = {
                object_type : data.object_type,
                object_id : data.object_id
            };
            var update_data = $.extend({}, data);

            if (is_defined(update_data.file.fid)) update_data.file = update_data.file.fid;

            //Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers[data.object_type].create(data, update_callback);
            
            var object_update_callback = function(data, callback) {
                Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers[data.object_type].create(data, update_callback);
                //console.log('Backend returned this after update :');
                //console.log(data);
            };

            Drupal.behaviors.cam3d_three_editor.update_remote(update_data, Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers[data.object_type].create, JSON.stringify(update_key));
        }
    };


    $.each(Drupal.behaviors.cam3d_three.sgnodeobjectcontrollers, function(key, controller){
        controller.prototype = genericControllerPrototype;
    });

}(jQuery));