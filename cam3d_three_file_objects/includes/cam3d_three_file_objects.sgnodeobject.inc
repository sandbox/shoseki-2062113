<?php

include_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'cam3d_three_file_objects') . '/includes/cam3d_three.file_object_loaders.inc';

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function cam3d_three_file_objects_sgnodeobjectcontrollers_info() {

    // {$view->name}--{$display->id}

    // Default position for objects
    $default_position = new stdClass();
    $default_position->x = 0.0;
    $default_position->y = 0.0;
    $default_position->z = 0.0;

    // Default rotation for objects
    $default_rotation = new stdClass();
    $default_rotation->x = 0.0;
    $default_rotation->y = 0.0;
    $default_rotation->z = 0.0;

    // Default scale for objects
    $default_scale = new stdClass();
    $default_scale->x = 1.0;
    $default_scale->y = 1.0;
    $default_scale->z = 1.0;
    
    // Default scripts for objects
    $default_scripts = array();
    
    // Default paradata
    $default_paradata = new stdClass();
    
    // Default view data
    $default_view_data = new stdClass();
  
    $controllers['file_obj'] = array(
        'label' => 'Volumetric Object',
        'data_controller' => 'FileOBJObjectCAM3DController',
        'variables' => array(
            'file' => array('default_value' => -1, 'type' => 'file'),
            'rotation' => array('default_value' => $default_rotation, 'type' => 'object'),
            'scale' => array('default_value' => $default_scale, 'type' => 'object'),
            'object_type' => array('default_value' => 'file_obj', 'type' => 'string'),
        ),
    );

    foreach ($controllers as $controllers_key => $controller) {
        $controllers[$controllers_key]['variables']['children'] = array('default_value' => array(), 'type' => 'array');
        $controllers[$controllers_key]['variables']['object_id'] = array('default_value' => -1, 'type' => 'integer');
        $controllers[$controllers_key]['variables']['position'] = array('default_value' => $default_position, 'type' => 'object');
        $controllers[$controllers_key]['variables']['name'] = array('default_value' => '', 'type' => 'string');
        $controllers[$controllers_key]['variables']['scripts'] = array('default_value' => $default_scripts, 'type' => 'object');
    }
  
    return $controllers;
}

class FileOBJObjectCAM3DController extends GenericCAM3DController {
    public function create($default_variables, $variables) {
        return parent::create($default_variables, $variables, 'file_obj');
    }
    
    public function read($default_variables, $entity) {
        
        // $id by default is the id of our entity
        // Parameters includes name => default_value and type
        
        if (is_string($entity->variables)) $entity->variables = unserialize($entity->variables);

        $variables = !empty($entity->variables) ? $entity->variables : array();
        $variables['object_id'] = $entity->basic_id;
        if (is_string($variables['object_id'])) $variables['object_id'] = intval($variables['object_id']);
        $variables['name'] = $entity->title;
        
        return $this->generate_json($default_variables, $variables);
    }
    
    public function front_end_controllers($variables) {

        $files = array();

        global $base_root;
        $files[] = $base_root . '/' . drupal_get_path('module', 'cam3d_three_file_objects') . '/js/sgnodeobjectcontrollers.file_objects.js';

        $object_loaders = cam3d_three_file_objects_retrieve_object_loaders();
        if (!empty($object_loaders[$variables['object_type']])) {
            $files[] = $object_loaders[$variables['object_type']];
        }
        
        // Check to see if $variables contains the editor value, in which case add core.ui controllers
        if (!empty($variables['ui'])) {
          $files[] = $base_root . '/' . drupal_get_path('module', 'cam3d_three_file_objects') . '/js/sgnodeobjectcontrollers.file_objects.ui.js';
        }

        return $files;
    }
}

function cam3d_three_file_objects_cam3d_three_variable_postprocessing_info() {

  $variable_postprocessing_info = array();
  
  $variable_postprocessing_info['file'] = '_cam3d_three_file_objects_process_file';
  
  return $variable_postprocessing_info;
}

function _cam3d_three_file_objects_process_file($consolidated_variables, $consolidated_variable_key) {
    
    $file_id = $consolidated_variables[$consolidated_variable_key];

    // If this function gets called multiple times, it only needs to convert an id into an object once
    if (is_object($file_id) || is_null($file_id)) {
        return $file_id;
    }

    if ($file_id < 0) {
      //if ($file_id == -1) $file_id = variable_get('cam3d_three_default_tex0_id', -1);
        return null;
    }
    if ($file_id >= 0) {
        $file_entity = file_load($file_id);

        $file = new stdClass();

        // Embed the file directly into the object
        //$file_path = drupal_realpath($file_entity->uri);
        //$file->file_contents = file_exists($file_path) ? file_get_contents($file_path) : null;
        
        // Retrieve the uri and return it instead
        $file->url = file_create_url($file_entity->uri);
        
        return $file;
    }
}