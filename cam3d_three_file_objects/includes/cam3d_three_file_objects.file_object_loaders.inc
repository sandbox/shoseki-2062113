<?php

// Function to call when retrieving typefaces, in the form $typeface_fonts['font']['weight]['style] = 'font_file_name.js';
function cam3d_three_file_objects_retrieve_object_loaders() {
    $data = array();
    $hook_name = 'object_loaders_info';
    $data = module_invoke_all($hook_name);
    drupal_alter($hook_name, $data);
    return $data;
}

/**
 * Implements hook_typeface_font_info().
 */
function cam3d_three_file_objects_object_loaders_info() {
    $object_loaders = array();

    // Convert the font into a typeface javascript file using http://typeface.neocracy.org/
    // $typeface_fonts['font']['weight]['style] = 'font_file_name.js';

    global $base_root;
    $three_path = libraries_get_path('three.js') . '/examples/js/loaders/';

    $typeface_fonts['file_obj'] = $base_root . '/' . $three_path . 'OBJLoader.js';

    return $typeface_fonts;
}