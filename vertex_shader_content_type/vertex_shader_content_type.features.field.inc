<?php
/**
 * @file
 * vertex_shader_content_type.features.field.inc
 */

/**
 * Implements hook_field_default_fields().
 */
function vertex_shader_content_type_field_default_fields() {
  $fields = array();

  // Exported field: 'node-vertex_shader-field_vs_script'.
  $fields['node-vertex_shader-field_vs_script'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_vs_script',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => '0',
      'module' => 'text',
      'settings' => array(),
      'translatable' => '0',
      'type' => 'text_long',
    ),
    'field_instance' => array(
      'bundle' => 'vertex_shader',
      'default_value' => array(
        0 => array(
          'value' => 'void main()
{
	gl_Position = vec4( position, 1.0 );
}

',
        ),
      ),
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'cam3d_three',
          'settings' => array(
            'height' => '240',
            'width' => '320',
          ),
          'type' => 'cam3d_three_vertex_shader_formatter',
          'weight' => '0',
        ),
        'teaser' => array(
          'label' => 'above',
          'module' => 'text',
          'settings' => array(
            'height' => '240',
            'width' => '320',
          ),
          'type' => 'text_default',
          'weight' => '0',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_vs_script',
      'label' => 'Vertex Shader Script',
      'required' => 1,
      'settings' => array(
        'text_processing' => '0',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'rows' => '5',
        ),
        'type' => 'text_textarea',
        'weight' => '-4',
      ),
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Vertex Shader Script');

  return $fields;
}
